package com.abipb.linkaccount.rest.resource;

import com.abipb.linkaccount.common.payload.ADBServiceResponse;
import com.abipb.linkaccount.payload.CustomerAccountSummary.response.CustomerAccountSummaryRequestApp;
import com.abipb.linkaccount.payload.CustomerAccountSummary.response.CustomerAccountSummaryResponse;
import com.abipb.linkaccount.payload.accountDetails.AccountRetrievalRequest;
import com.abipb.linkaccount.payload.accountDetails.request.AccountDetailsRequest;
import com.abipb.linkaccount.payload.accountDetails.response.AccountDetailsResponse;
import com.abipb.linkaccount.payload.loginmpin.Accounts;
import com.abipb.linkaccount.repository.TrackingDetailsRepository;
import com.abipb.linkaccount.service.AccountDetailsService;
import com.abipb.linkaccount.service.impl.AccountDetailsServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by pardhasaradhi.k on 10/26/2018.
 */
@Component
@Path("/accountDetails")
public class AccountDetailsRetrievalResource {
    private static Logger logger= LoggerFactory.getLogger(AccountDetailsRetrievalResource.class);

    @Autowired
    AccountDetailsService accountDetailsService;

    @Path("/getAccountDetails")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAccountDetails(AccountRetrievalRequest accountDetailsRequest){
    logger.info("Entering getAccountDetails method");
        AccountDetailsResponse accountDetailsResponse=null;
        accountDetailsResponse= accountDetailsService.getAccountDetails(accountDetailsRequest);
        return Response.ok(accountDetailsResponse).build();

    }

    @Path("/getCustomerAccountSummary")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getCustomerAccountSummary(CustomerAccountSummaryRequestApp customerAccountSummaryRequestApp){

        CustomerAccountSummaryResponse customerAccountSummaryResponse=new CustomerAccountSummaryResponse();
        customerAccountSummaryResponse=accountDetailsService.getCustomerSummaryData(customerAccountSummaryRequestApp);

        return Response.ok(customerAccountSummaryResponse).build();
    }



    @Path("/savePreferAccId")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response savePeferAccId(String accountId){
        logger.info("Entering savePreferAccId "+accountId);
        Accounts account=  accountDetailsService.savePreferAccId(accountId);
        return Response.ok(account).build();

    }

}
