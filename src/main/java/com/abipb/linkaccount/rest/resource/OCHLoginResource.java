package com.abipb.linkaccount.rest.resource;

import com.abipb.linkaccount.common.payload.ADBServiceResponse;
import com.abipb.linkaccount.payload.loginmpin.LogInRequest;
import com.abipb.linkaccount.payload.loginmpin.LogInResponse;
import com.abipb.linkaccount.service.OCHLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by pardhasaradhi.k on 10/31/2018.
 */
@Component
@Path("/OCHLogin")
public class OCHLoginResource {

    @Autowired
    OCHLoginService ochLoginService;

    @Path("/authenticateUser")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response authenticateUser(LogInRequest ochLogInMpinRequest){
        LogInResponse ochLoginResponse= null;
        ochLoginResponse=ochLoginService.authenticateUser(ochLogInMpinRequest);
        return Response.ok(ochLoginResponse).build();
    }


}
