package com.abipb.linkaccount.rest.resource;

import com.abipb.linkaccount.common.payload.ADBMessageHeader;
import com.abipb.linkaccount.common.payload.ADBServiceResponse;
import com.abipb.linkaccount.common.payload.StatusInfo;
import com.abipb.linkaccount.exception.LinkAccountMiddlewareException;
import com.abipb.linkaccount.exception.LinkAccountServiceException;
import com.abipb.linkaccount.payload.ArrayListOfLocateAgentDetail;
import com.abipb.linkaccount.payload.LocateAgentDetil;
import com.abipb.linkaccount.service.LocateAgentService;
import com.abipb.linkaccount.utils.MPesaPropertyValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;

/**
 * This class contains various methods for doing functionality of Locate Agent.
 *
 * <p>The methods in this class all throw a {@code LinkAccountMiddlewareException},
 * if the specified array reference is null, except where noted.

 * @author Abhay kumar
 * @since  1.0
 */
@Component
@Path("/locateAgent")
public class LocateAgentResource {
    public static final Logger LOGGER= LoggerFactory.getLogger(LocateAgentResource.class);

    @Autowired
    private LocateAgentService locateAgentService;

//    @Path("/fetechAgents")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<LocateAgentDetil> fetchLocateAgent(@QueryParam("operationType") String operationType,
                                                   @DefaultValue("") @QueryParam("mobileNo") String mobileNo,
                                                   @DefaultValue("")@QueryParam("city") String city,
                                                   @DefaultValue("") @QueryParam("district") String district,
                                                   @DefaultValue("") @QueryParam("pinCode") String pinCode) throws LinkAccountMiddlewareException {
        List<LocateAgentDetil> locateAgentDetils=null;

        try {
            if(StringUtils.isEmpty(operationType)){
                throw new LinkAccountMiddlewareException("Please Enter Action Type.");
            }

            LocateAgentDetil locateAgentDetil=new LocateAgentDetil();
            locateAgentDetil.setMobileNo(mobileNo);
            locateAgentDetils= locateAgentService.fetchLocateAgent(locateAgentDetil);


        }catch (Exception exception){

        }

        return Arrays.asList();
    }


    @Path("/fetechAgents")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response fetchLocateAgents(@QueryParam("operationType") String operationType,
                                                @DefaultValue("") @QueryParam("mobileNo") String mobileNo,
                                                @DefaultValue("")@QueryParam("city") String city,
                                                @DefaultValue("") @QueryParam("district") String district,
                                                @DefaultValue("") @QueryParam("pinCode") String pinCode) throws LinkAccountMiddlewareException {
        ADBServiceResponse adbServiceResponse=new ADBServiceResponse();
        StatusInfo  statusInfo=new StatusInfo();

        adbServiceResponse.setAdbMessageHeader(new ADBMessageHeader());
        adbServiceResponse.setStatusInfo(statusInfo);

        try {
            if(StringUtils.isEmpty(operationType)){
                LinkAccountMiddlewareException linkAccountMiddlewareException =new LinkAccountMiddlewareException(MPesaPropertyValue.getValue("resource.locateAgent.MP0001"));
                LOGGER.debug(MPesaPropertyValue.getValue("resource.locateAgent.MP0001"));
                throw linkAccountMiddlewareException;
            }

            LocateAgentDetil locateAgentDetil=new LocateAgentDetil();
            locateAgentDetil.setMobileNo(mobileNo);
            List<LocateAgentDetil> locateAgentDetils= locateAgentService.fetchLocateAgent(locateAgentDetil);
            LOGGER.debug("Got List of Details for Locate Agent size : "+locateAgentDetils.size());
            adbServiceResponse.setResponseData(new ArrayListOfLocateAgentDetail(locateAgentDetils));
            statusInfo.setStatus("SUCCESS");
            statusInfo.setCode("000");
            statusInfo.setInfo(MPesaPropertyValue.getValue("resource.locateAgent.MP0002"));
        }catch (LinkAccountServiceException exception){
            LOGGER.error(" LinkAccountServiceException  "+exception.getLocalizedMessage());
            LinkAccountMiddlewareException linkAccountMiddlewareException =new LinkAccountMiddlewareException(exception);
            statusInfo.setStatus("FAILURE");
            statusInfo.setCode("MP0003");
            statusInfo.setInfo(MPesaPropertyValue.getValue("resource.locateAgent.MP0003"));
            linkAccountMiddlewareException.setAdbServiceResponseError(adbServiceResponse);
            throw linkAccountMiddlewareException;
        }

        return Response.ok(adbServiceResponse).build();
    }
}
