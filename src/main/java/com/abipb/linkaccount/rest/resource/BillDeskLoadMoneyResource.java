package com.abipb.linkaccount.rest.resource;

import com.abipb.linkaccount.payload.billdesk.request.BillDeskIIBLoadMoneyRequest;
import com.abipb.linkaccount.payload.billdesk.response.BillDeskLoadMoneyResponse;
import com.abipb.linkaccount.service.LoadMoneyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by Rajasekhar.E-V on 29-10-2018.
 */
@Component
@Path("/billDesk")
public class BillDeskLoadMoneyResource {

    private static final Logger log= LoggerFactory.getLogger(BillDeskLoadMoneyResource.class);

    @Autowired
    LoadMoneyService loadMoneyService;

    @Path("/loadMoneyRequest")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response billDeskLoadMoneyRequest(BillDeskIIBLoadMoneyRequest billDeskIIBLoadMoneyRequest) {
        URI targetURIForRedirection =null;
        String finalReq =  loadMoneyService.generateBilldeskUrl(billDeskIIBLoadMoneyRequest);

        try {
         targetURIForRedirection = new URI(finalReq);
        } catch (URISyntaxException e) {
           log.error(e.getMessage());
        }
        return Response.seeOther(targetURIForRedirection).build();

    }
    @Path("/loadMoneyRespnse")
    @POST
    public Response billDeskLoadMoneyRequest(BillDeskLoadMoneyResponse billDeskLoadMoneyResponse) {
        URI targetURIForRedirection =null;
        String merchantReturnUrl= loadMoneyService.saveBillDeskResponse(billDeskLoadMoneyResponse.getDATA());
        try {
            targetURIForRedirection = new URI(merchantReturnUrl);
        } catch (URISyntaxException e) {
            log.error(e.getMessage());
        }
        return Response.seeOther(targetURIForRedirection).build();
    }
}
