package com.abipb.linkaccount.rest.resource;

import com.abipb.linkaccount.common.payload.ADBServiceResponse;
import com.abipb.linkaccount.payload.BalanceEnqryResp;
import com.abipb.linkaccount.payload.balanceenquiry.BalanceEnquiryRequest;
import com.abipb.linkaccount.service.BalanceEnquiryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by pardhasaradhi.k on 10/12/2018.
 */
@Component
@Path("/balanceEnquiry")
public class BalanceEnquiryResource {
    private static final Logger log= LoggerFactory.getLogger(BalanceEnquiryResource.class);
    @Autowired
    BalanceEnquiryService balanceEnquiryService;



    @Path("/getBalance")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getBalance(BalanceEnquiryRequest balanceEnquiryRequest){

        BalanceEnqryResp balanceEnqryResp =balanceEnquiryService.getBalance(balanceEnquiryRequest);

        return Response.ok(balanceEnqryResp).build();
    }


}
