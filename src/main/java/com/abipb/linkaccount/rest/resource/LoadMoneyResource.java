package com.abipb.linkaccount.rest.resource;

import com.abipb.linkaccount.common.payload.ADBMessageHeader;
import com.abipb.linkaccount.common.payload.ADBServiceResponse;
import com.abipb.linkaccount.common.payload.StatusInfo;
import com.abipb.linkaccount.payload.LoadMnyResp;
import com.abipb.linkaccount.payload.loadmoney.request.LoadMoneyRequest;
import com.abipb.linkaccount.payload.loadmoney.response.LoadMoneyResponse;
import com.abipb.linkaccount.service.LoadMoneyService;
import com.abipb.linkaccount.utils.HTTPPostClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jboss.logging.annotations.Pos;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * Created by pardhasaradhi.k on 10/12/2018.
 */
@Component
@Path("/LoadMoney")
public class LoadMoneyResource {
    private static final Logger log= LoggerFactory.getLogger(LoadMoneyResource.class);
    @Autowired
    LoadMoneyService loadMoneyService;




    @Path("/depositMoney")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response depositMoney(LoadMoneyRequest loadMoneyRequest){
       ADBServiceResponse adbServiceResponse=new ADBServiceResponse();
       adbServiceResponse=loadMoneyService.depositMoney(loadMoneyRequest);



        return Response.ok(adbServiceResponse).build();
    }
}
