package com.abipb.linkaccount.rest.resource;

import com.abipb.linkaccount.common.payload.ADBMessageHeader;
import com.abipb.linkaccount.common.payload.ADBServiceResponse;
import com.abipb.linkaccount.common.payload.StatusInfo;
import com.abipb.linkaccount.payload.FundsTrnsResp;
import com.abipb.linkaccount.payload.fundtransfer.FundsTransferRequest;
import com.abipb.linkaccount.payload.fundtransfer.request.FundTransferRequest;
import com.abipb.linkaccount.payload.fundtransfer.response.FundTransferResponse;
import com.abipb.linkaccount.service.FundsTransferService;
import com.abipb.linkaccount.utils.HTTPPostClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * Created by pardhasaradhi.k on 10/12/2018.
 */
@Component
@Path("/fundTransfer")
public class FundsTransferResource {
    private static final Logger log= LoggerFactory.getLogger(FundsTransferResource.class);
    @Autowired
    FundsTransferService fundsTransferService;


    @Path("/imps")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response fundsTransfer(FundsTransferRequest fundTransferRequest){
      ADBServiceResponse adbServiceResponse=new ADBServiceResponse();
      adbServiceResponse=fundsTransferService.fundsTransfer(fundTransferRequest);

        return Response.ok(adbServiceResponse).build();
    }

}
