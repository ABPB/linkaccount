package com.abipb.linkaccount.rest.resource;

import com.abipb.linkaccount.domain.DATA;
import com.abipb.linkaccount.domain.TrackingDetails;
import com.abipb.linkaccount.payload.loginmpin.LogInRequest;
import com.abipb.linkaccount.payload.loginmpin.LogInResponse;
import com.abipb.linkaccount.service.TrackingDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Rajasekhar.E-V on 08-11-2018.
 */
@Component
@Path("/TrackingDetails")
public class TrackingDetailsResouce {
    @Autowired
    TrackingDetailsService trackingDetailsService;

    @Path("/getTrackingDetails")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getTrackingDetails(String trackingId){
      TrackingDetails trackingDetails= trackingDetailsService.getTrackingDetails(trackingId);
        return Response.ok(trackingDetails).build();
    }

    @Path("/getBillDeskTrackingDetails")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getBillDeskTrackingDetails(String trackingId){
        DATA trackingDetails= trackingDetailsService.getBilldeskTransactionDetails(trackingId);
        return Response.ok(trackingDetails).build();
    }

}
