package com.abipb.linkaccount.common.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder( {
    "ADBServiceRequest"
})
 
public class ADBPBRequest {
    @JsonProperty(value = "ADBServiceRequest",  required = true)
    private ADBServiceRequest adbServiceRequest;

    public ADBServiceRequest getAdbServiceRequest() {
        return adbServiceRequest;
    }

    public void setAdbServiceRequest(ADBServiceRequest adbServiceRequest) {
        this.adbServiceRequest = adbServiceRequest;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((adbServiceRequest == null) ? 0
		: adbServiceRequest.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	ADBPBRequest other = (ADBPBRequest) obj;
	if (adbServiceRequest == null) {
	    if (other.adbServiceRequest != null)
		return false;
	} else if (!adbServiceRequest.equals(other.adbServiceRequest))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "ADBPBRequest [adbServiceRequest=" + adbServiceRequest + "]";
    }
    
    
    
}
