
package com.abipb.linkaccount.common.payload;


import com.abipb.linkaccount.payload.balanceenquiry.request.MsgAttrGrp;
import com.abipb.linkaccount.payload.balanceenquiry.request.MsgIdentifyGrp;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "msgIdentifyGrp",
        "userAttrGrp",
        "msgAttrGrp"
})
public class ADBMsgHdr {

    @JsonProperty(value = "msgIdentifyGrp",required = false,defaultValue = "")
    private MsgIdentifyGrp msgIdentifyGrp;
    @JsonProperty(value = "userAttrGrp",required = false,defaultValue = "")
    private UserAttributeGroup userAttrGrp;
    @JsonProperty(value = "msgAttrGrp",required = false,defaultValue = "")
    private MsgAttrGrp msgAttrGrp;

    /**
     * No args constructor for use in serialization
     */
    public ADBMsgHdr() {
    }

    /**
     * @param msgIdentifyGrp
     * @param userAttrGrp
     * @param msgAttrGrp
     */
    public ADBMsgHdr(MsgIdentifyGrp msgIdentifyGrp, UserAttributeGroup userAttrGrp, MsgAttrGrp msgAttrGrp) {
        super();
        this.msgIdentifyGrp = msgIdentifyGrp;
        this.userAttrGrp = userAttrGrp;
        this.msgAttrGrp = msgAttrGrp;
    }

    @JsonProperty(value = "msgIdentifyGrp",required = false,defaultValue = "")
    public MsgIdentifyGrp getMsgIdentifyGrp() {
        return msgIdentifyGrp;
    }

    @JsonProperty(value = "msgIdentifyGrp",required = false,defaultValue = "")
    public void setMsgIdentifyGrp(MsgIdentifyGrp msgIdentifyGrp) {
        this.msgIdentifyGrp = msgIdentifyGrp;
    }

    @JsonProperty(value = "userAttrGrp",required = false,defaultValue = "")
    public UserAttributeGroup getUserAttrGrp() {
        return userAttrGrp;
    }

    @JsonProperty(value = "userAttrGrp",required = false,defaultValue = "")
    public void setUserAttrGrp(UserAttributeGroup userAttrGrp) {
        this.userAttrGrp = userAttrGrp;
    }

    @JsonProperty(value = "msgAttrGrp",required = false,defaultValue = "")
    public MsgAttrGrp getMsgAttrGrp() {
        return msgAttrGrp;
    }

    @JsonProperty(value = "msgAttrGrp",required = false,defaultValue = "")
    public void setMsgAttrGrp(MsgAttrGrp msgAttrGrp) {
        this.msgAttrGrp = msgAttrGrp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ADBMsgHdr adbMsgHdr = (ADBMsgHdr) o;

        if (msgIdentifyGrp != null ? !msgIdentifyGrp.equals(adbMsgHdr.msgIdentifyGrp) : adbMsgHdr.msgIdentifyGrp != null)
            return false;
        if (userAttrGrp != null ? !userAttrGrp.equals(adbMsgHdr.userAttrGrp) : adbMsgHdr.userAttrGrp != null)
            return false;
        return msgAttrGrp != null ? msgAttrGrp.equals(adbMsgHdr.msgAttrGrp) : adbMsgHdr.msgAttrGrp == null;
    }

    @Override
    public int hashCode() {
        int result = msgIdentifyGrp != null ? msgIdentifyGrp.hashCode() : 0;
        result = 31 * result + (userAttrGrp != null ? userAttrGrp.hashCode() : 0);
        result = 31 * result + (msgAttrGrp != null ? msgAttrGrp.hashCode() : 0);
        return result;
    }
}
