package com.abipb.linkaccount.common.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.lang.*;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder( {
    "ADBMsgHdr",
    "StatusInfo",
    "ResponseData",
    "Error"
})
public class ADBServiceResponse {
       
         @JsonProperty(value = "ADBMsgHdr", required = false)
        private ADBMessageHeader  adbMessageHeader;
        @JsonProperty(value = "ResponseData", required = false)
        private ResponseData responseData;
        
        @JsonProperty(value = "StatusInfo", required = false)
        private StatusInfo statusInfo;;
        
        
        @JsonProperty(value = "Error", required = false)
        private Error error;


	public ADBMessageHeader getAdbMessageHeader() {
	    return adbMessageHeader;
	}


	public void setAdbMessageHeader(ADBMessageHeader adbMessageHeader) {
	    this.adbMessageHeader = adbMessageHeader;
	}


	public ResponseData getResponseData() {
		return responseData;
	}

	public void setResponseData(ResponseData responseData) {
		this.responseData = responseData;
	}

	public StatusInfo getStatusInfo() {
	    return statusInfo;
	}


	public void setStatusInfo(StatusInfo statusInfo) {
	    this.statusInfo = statusInfo;
	}


	public Error getError() {
	    return error;
	}


	public void setError(Error error) {
	    this.error = error;
	}


	@Override
	public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + ((adbMessageHeader == null) ? 0
		    : adbMessageHeader.hashCode());
	    result = prime * result + ((error == null) ? 0 : error.hashCode());
	    result = prime * result
		    + ((responseData == null) ? 0 : responseData.hashCode());
	    result = prime * result
		    + ((statusInfo == null) ? 0 : statusInfo.hashCode());
	    return result;
	}


	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
		return true;
	    if (obj == null)
		return false;
	    if (getClass() != obj.getClass())
		return false;
	    ADBServiceResponse other = (ADBServiceResponse) obj;
	    if (adbMessageHeader == null) {
		if (other.adbMessageHeader != null)
		    return false;
	    } else if (!adbMessageHeader.equals(other.adbMessageHeader))
		return false;
	    if (error == null) {
		if (other.error != null)
		    return false;
	    } else if (!error.equals(other.error))
		return false;
	    if (responseData == null) {
		if (other.responseData != null)
		    return false;
	    } else if (!responseData.equals(other.responseData))
		return false;
	    if (statusInfo == null) {
		if (other.statusInfo != null)
		    return false;
	    } else if (!statusInfo.equals(other.statusInfo))
		return false;
	    return true;
	}


	@Override
	public String toString() {
	    return "ADBServiceResponse [adbMessageHeader=" + adbMessageHeader
		    + ", responseData=" + responseData + ", statusInfo="
		    + statusInfo + ", error=" + error + "]";
	}


	
        
}
