package com.abipb.linkaccount.common.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder( {
    "channelId",
    "staffId",
    "deviceId"
})
public class UserAttributeGroup {
    
    @JsonProperty(value = "channelId",defaultValue="", required = false)
    private String channelId;
    @JsonProperty(value = "staffId",defaultValue="", required = false)
    private String staffId;
    @JsonProperty(value = "deviceId",defaultValue="", required = false)
    private String deviceId;
    
    
    public String getChannelId() {
        return channelId;
    }
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
    public String getStaffId() {
        return staffId;
    }
    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }
    public String getDeviceId() {
        return deviceId;
    }
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result
		+ ((channelId == null) ? 0 : channelId.hashCode());
	result = prime * result
		+ ((deviceId == null) ? 0 : deviceId.hashCode());
	result = prime * result + ((staffId == null) ? 0 : staffId.hashCode());
	return result;
    }
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	UserAttributeGroup other = (UserAttributeGroup) obj;
	if (channelId == null) {
	    if (other.channelId != null)
		return false;
	} else if (!channelId.equals(other.channelId))
	    return false;
	if (deviceId == null) {
	    if (other.deviceId != null)
		return false;
	} else if (!deviceId.equals(other.deviceId))
	    return false;
	if (staffId == null) {
	    if (other.staffId != null)
		return false;
	} else if (!staffId.equals(other.staffId))
	    return false;
	return true;
    }
    @Override
    public String toString() {
	return "UserAttributeGroup [channelId=" + channelId + ", staffId="
		+ staffId + ", deviceId=" + deviceId + "]";
    }
   
    
    

}
