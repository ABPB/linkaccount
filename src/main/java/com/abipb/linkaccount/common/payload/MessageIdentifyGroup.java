package com.abipb.linkaccount.common.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder( {
    "sessionId",
    "rqstDateTime",
    "sourceChnlInfoGrp",
    "msgAttrGrp"
})
public class MessageIdentifyGroup {
    
    @JsonProperty(value = "sessionId",defaultValue="", required = false)
    private String  sessionId;
    @JsonProperty(value = "processId",defaultValue="", required = false)
    private String processId;
    @JsonProperty(value = "rqstDateTime",defaultValue="", required = false)
    private String requesttDateTime;
    @JsonProperty(value = "sourceChnlInfoGrp", required = false)
    private SourceChannelInfoGroup sourceChannelInfoGroup;
    @JsonProperty(value = "msgAttrGrp", required = false)
    private MessageAttributeGroup messageAttributeGroup;
    public String getSessionId() {
        return sessionId;
    }
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
    public String getProcessId() {
        return processId;
    }
    public void setProcessId(String processId) {
        this.processId = processId;
    }
    public String getRequesttDateTime() {
        return requesttDateTime;
    }
    public void setRequesttDateTime(String requesttDateTime) {
        this.requesttDateTime = requesttDateTime;
    }
    public SourceChannelInfoGroup getSourceChannelInfoGroup() {
        return sourceChannelInfoGroup;
    }
    public void setSourceChannelInfoGroup(
    	SourceChannelInfoGroup sourceChannelInfoGroup) {
        this.sourceChannelInfoGroup = sourceChannelInfoGroup;
    }
    public MessageAttributeGroup getMessageAttributeGroup() {
        return messageAttributeGroup;
    }
    public void setMessageAttributeGroup(
    	MessageAttributeGroup messageAttributeGroup) {
        this.messageAttributeGroup = messageAttributeGroup;
    }
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((messageAttributeGroup == null) ? 0
		: messageAttributeGroup.hashCode());
	result = prime * result
		+ ((processId == null) ? 0 : processId.hashCode());
	result = prime * result + ((requesttDateTime == null) ? 0
		: requesttDateTime.hashCode());
	result = prime * result
		+ ((sessionId == null) ? 0 : sessionId.hashCode());
	result = prime * result + ((sourceChannelInfoGroup == null) ? 0
		: sourceChannelInfoGroup.hashCode());
	return result;
    }
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	MessageIdentifyGroup other = (MessageIdentifyGroup) obj;
	if (messageAttributeGroup == null) {
	    if (other.messageAttributeGroup != null)
		return false;
	} else if (!messageAttributeGroup.equals(other.messageAttributeGroup))
	    return false;
	if (processId == null) {
	    if (other.processId != null)
		return false;
	} else if (!processId.equals(other.processId))
	    return false;
	if (requesttDateTime == null) {
	    if (other.requesttDateTime != null)
		return false;
	} else if (!requesttDateTime.equals(other.requesttDateTime))
	    return false;
	if (sessionId == null) {
	    if (other.sessionId != null)
		return false;
	} else if (!sessionId.equals(other.sessionId))
	    return false;
	if (sourceChannelInfoGroup == null) {
	    if (other.sourceChannelInfoGroup != null)
		return false;
	} else if (!sourceChannelInfoGroup.equals(other.sourceChannelInfoGroup))
	    return false;
	return true;
    }
    @Override
    public String toString() {
	return "MessageIdentifyGroup [sessionId=" + sessionId + ", processId="
		+ processId + ", requesttDateTime=" + requesttDateTime
		+ ", sourceChannelInfoGroup=" + sourceChannelInfoGroup
		+ ", messageAttributeGroup=" + messageAttributeGroup + "]";
    }
    

    
    
}
