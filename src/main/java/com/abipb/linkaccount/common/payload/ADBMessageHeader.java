package com.abipb.linkaccount.common.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder( {
     "sessionId",
     "processId",
     "rqstDateTime",
     "sourceChnl",
     "sourceAppl",
     "serviceName",
     "serviceVersionNbr",
     "channelId",
     "staffId",
    "deviceId"
})
public class ADBMessageHeader {

    @JsonProperty(value = "sessionId",defaultValue="", required = false)
    private String  sessionId;
    @JsonProperty(value = "processId",defaultValue="", required = false)
    private String processId;
    @JsonProperty(value = "rqstDateTime",defaultValue="", required = false)
    private String requesttDateTime;
    
    @JsonProperty(value = "sourceChnl",defaultValue="", required = false)
    private String sourceChannel;
    @JsonProperty(value = "sourceAppl",defaultValue="", required = false)
    private String sourchApplication;
    @JsonProperty(value = "serviceName",defaultValue="", required = false)
    private String serviceName;
    @JsonProperty(value = "serviceVersionNbr",defaultValue="", required = false)
    private String serviceVersionNumber;
    
    
    @JsonProperty(value = "channelId",defaultValue="", required = false)
    private String channelId;
    @JsonProperty(value = "staffId",defaultValue="", required = false)
    private String staffId;
    @JsonProperty(value = "deviceId",defaultValue="", required = false)
    private String deviceId;
    
    
   
}
