package com.abipb.linkaccount.common.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder( {
    "serviceName",
    "serviceVersionNbr"
})
public class MessageAttributeGroup {
    
    @JsonProperty(value = "serviceName",defaultValue="", required = false)
    private String serviceName;
    @JsonProperty(value = "serviceVersionNbr",defaultValue="", required = false)
    private String serviceVersionNumber;
    public String getServiceName() {
        return serviceName;
    }
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
    public String getServiceVersionNumber() {
        return serviceVersionNumber;
    }
    public void setServiceVersionNumber(String serviceVersionNumber) {
        this.serviceVersionNumber = serviceVersionNumber;
    }
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result
		+ ((serviceName == null) ? 0 : serviceName.hashCode());
	result = prime * result + ((serviceVersionNumber == null) ? 0
		: serviceVersionNumber.hashCode());
	return result;
    }
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	MessageAttributeGroup other = (MessageAttributeGroup) obj;
	if (serviceName == null) {
	    if (other.serviceName != null)
		return false;
	} else if (!serviceName.equals(other.serviceName))
	    return false;
	if (serviceVersionNumber == null) {
	    if (other.serviceVersionNumber != null)
		return false;
	} else if (!serviceVersionNumber.equals(other.serviceVersionNumber))
	    return false;
	return true;
    }
    @Override
    public String toString() {
	return "MessageAttributeGroup [serviceName=" + serviceName
		+ ", serviceVersionNumber=" + serviceVersionNumber + "]";
    }
    
    
    
    

}
