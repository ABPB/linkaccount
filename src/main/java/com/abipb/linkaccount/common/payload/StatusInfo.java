package com.abipb.linkaccount.common.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder( {
    "code",
    "status",
    "info"
})
public class StatusInfo {
    @JsonProperty(value = "code",defaultValue="", required = false)
    private String code;
    @JsonProperty(value = "status",defaultValue="", required = false)
    private String status;
    @JsonProperty(value = "info",defaultValue="", required = false)
    private String info;
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((code == null) ? 0 : code.hashCode());
	result = prime * result + ((info == null) ? 0 : info.hashCode());
	result = prime * result + ((status == null) ? 0 : status.hashCode());
	return result;
    }
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	StatusInfo other = (StatusInfo) obj;
	if (code == null) {
	    if (other.code != null)
		return false;
	} else if (!code.equals(other.code))
	    return false;
	if (info == null) {
	    if (other.info != null)
		return false;
	} else if (!info.equals(other.info))
	    return false;
	if (status == null) {
	    if (other.status != null)
		return false;
	} else if (!status.equals(other.status))
	    return false;
	return true;
    }
    @Override
    public String toString() {
	return "StatusInfo [code=" + code + ", status=" + status + ", info="
		+ info + "]";
    }
    
    
    
    
}
