package com.abipb.linkaccount.common.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder( {
    "resonCode",
    "resonDesc"
})
public class Error {
    @JsonProperty(value = "resonCode",defaultValue="", required = false)
    private String resonCode;
    @JsonProperty(value = "resonDesc",defaultValue="", required = false)
    private String resonDescription;
    
    
    public String getResonCode() {
        return resonCode;
    }
    public void setResonCode(String resonCode) {
        this.resonCode = resonCode;
    }
    public String getResonDescription() {
        return resonDescription;
    }
    public void setResonDescription(String resonDescription) {
        this.resonDescription = resonDescription;
    }
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result
		+ ((resonCode == null) ? 0 : resonCode.hashCode());
	result = prime * result + ((resonDescription == null) ? 0
		: resonDescription.hashCode());
	return result;
    }
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Error other = (Error) obj;
	if (resonCode == null) {
	    if (other.resonCode != null)
		return false;
	} else if (!resonCode.equals(other.resonCode))
	    return false;
	if (resonDescription == null) {
	    if (other.resonDescription != null)
		return false;
	} else if (!resonDescription.equals(other.resonDescription))
	    return false;
	return true;
    }
    @Override
    public String toString() {
	return "Error [resonCode=" + resonCode + ", resonDescription="
		+ resonDescription + "]";
    }
   
    
    
}
