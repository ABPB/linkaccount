package com.abipb.linkaccount.common.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder( {
    "sourceChnl",
    "sourceAppl"
})
public class SourceChannelInfoGroup {
    @JsonProperty(value = "sourceChnl",defaultValue="", required = false)
    private String sourceChannel;
    @JsonProperty(value = "sourceAppl",defaultValue="", required = false)
    private String sourchApplication;
    public String getSourceChannel() {
        return sourceChannel;
    }
    public void setSourceChannel(String sourceChannel) {
        this.sourceChannel = sourceChannel;
    }
    public String getSourchApplication() {
        return sourchApplication;
    }
    public void setSourchApplication(String sourchApplication) {
        this.sourchApplication = sourchApplication;
    }
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result
		+ ((sourceChannel == null) ? 0 : sourceChannel.hashCode());
	result = prime * result + ((sourchApplication == null) ? 0
		: sourchApplication.hashCode());
	return result;
    }
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	SourceChannelInfoGroup other = (SourceChannelInfoGroup) obj;
	if (sourceChannel == null) {
	    if (other.sourceChannel != null)
		return false;
	} else if (!sourceChannel.equals(other.sourceChannel))
	    return false;
	if (sourchApplication == null) {
	    if (other.sourchApplication != null)
		return false;
	} else if (!sourchApplication.equals(other.sourchApplication))
	    return false;
	return true;
    }
    @Override
    public String toString() {
	return "SourceChannelInfoGroup [sourceChannel=" + sourceChannel
		+ ", sourchApplication=" + sourchApplication + "]";
    }
    
    
    
}
