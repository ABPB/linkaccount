package com.abipb.linkaccount.common.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.abipb.linkaccount.common.payload.ADBMessageHeader;


@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder( {
    "ADBMsgHdr",
    "RequestData"
})
public class ADBServiceRequest {
    
     @JsonProperty(value = "ADBMsgHdr",defaultValue="", required = false)
    private ADBMessageHeader  adbMessageHeader;
    @JsonProperty(value = "RequestData",defaultValue="", required = false)
    private RequestData requestData;
   
    public ADBMessageHeader getAdbMessageHeader() {
        return adbMessageHeader;
    }
    public void setAdbMessageHeader(ADBMessageHeader adbMessageHeader) {
        this.adbMessageHeader = adbMessageHeader;
    }

	public RequestData getRequestData() {
		return requestData;
	}

	public void setRequestData(RequestData requestData) {
		this.requestData = requestData;
	}

	@Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((adbMessageHeader == null) ? 0
		: adbMessageHeader.hashCode());
	result = prime * result
		+ ((requestData == null) ? 0 : requestData.hashCode());
	return result;
    }
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	ADBServiceRequest other = (ADBServiceRequest) obj;
	if (adbMessageHeader == null) {
	    if (other.adbMessageHeader != null)
		return false;
	} else if (!adbMessageHeader.equals(other.adbMessageHeader))
	    return false;
	if (requestData == null) {
	    if (other.requestData != null)
		return false;
	} else if (!requestData.equals(other.requestData))
	    return false;
	return true;
    }
    @Override
    public String toString() {
	return "ADBServiceRequest [adbMessageHeader=" + adbMessageHeader
		+ ", requestData=" + requestData + "]";
    }
    
    
   }
