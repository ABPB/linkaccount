package com.abipb.linkaccount.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Rajasekhar.E-V on 26-10-2018.
 */
@Entity
@Table(name = "billedesk_Response")
public class DATA implements Serializable {
    @Id
    @Column
    private String TxnReferenceNo;
    @Column
    private String ItemCode;
    @Column
    private String AuthStatus;
    @Column
    private String SettlementType;
    @Column
    private String AdditionalInfo6;
    @Column
    private String SuccessFlag;
    @Column
    private String AdditionalInfo7;
    @Column
    private String BankReferenceNo;
    @Column
    private String TxnAmount;
    @Column
    private String AdditionalInfo5;
    @Column
    private String CurrencyName;
    @Column
    private String ErrorStatus;
    @Column
    private String BankID;
    @Column
    private String SecurityType;
    @Column
    private String SecurityID;
    @Column
    private String TxnDate;
    @Column
    private String SecurityPassword;
    @Column
    private String MerchantID;
    @Column
    private String ErrorDescription;
    @Column
    private String BankMerchantID;
    @Column
    private String CustomerID;
    @Column
    private String AdditionalInfo4;
    @Column
    private String AdditionalInfo3;
    @Column
    private String TxnType;
    @Column
    private String AdditionalInfo2;
    @Column
    private String CheckSum;
    @Column
    private String AdditionalInfo1;
    @Column
    private String CheckSumFlag;


    public String getTxnReferenceNo() {
        return TxnReferenceNo;
    }

    public void setTxnReferenceNo(String txnReferenceNo) {
        TxnReferenceNo = txnReferenceNo;
    }

    public String getItemCode() {
        return ItemCode;
    }

    public void setItemCode(String itemCode) {
        ItemCode = itemCode;
    }

    public String getAuthStatus() {
        return AuthStatus;
    }

    public void setAuthStatus(String authStatus) {
        AuthStatus = authStatus;
    }

    public String getSettlementType() {
        return SettlementType;
    }

    public void setSettlementType(String settlementType) {
        SettlementType = settlementType;
    }

    public String getAdditionalInfo6() {
        return AdditionalInfo6;
    }

    public void setAdditionalInfo6(String additionalInfo6) {
        AdditionalInfo6 = additionalInfo6;
    }

    public String getSuccessFlag() {
        return SuccessFlag;
    }

    public void setSuccessFlag(String successFlag) {
        SuccessFlag = successFlag;
    }

    public String getAdditionalInfo7() {
        return AdditionalInfo7;
    }

    public void setAdditionalInfo7(String additionalInfo7) {
        AdditionalInfo7 = additionalInfo7;
    }

    public String getBankReferenceNo() {
        return BankReferenceNo;
    }

    public void setBankReferenceNo(String bankReferenceNo) {
        BankReferenceNo = bankReferenceNo;
    }

    public String getTxnAmount() {
        return TxnAmount;
    }

    public void setTxnAmount(String txnAmount) {
        TxnAmount = txnAmount;
    }

    public String getAdditionalInfo5() {
        return AdditionalInfo5;
    }

    public void setAdditionalInfo5(String additionalInfo5) {
        AdditionalInfo5 = additionalInfo5;
    }

    public String getCurrencyName() {
        return CurrencyName;
    }

    public void setCurrencyName(String currencyName) {
        CurrencyName = currencyName;
    }

    public String getErrorStatus() {
        return ErrorStatus;
    }

    public void setErrorStatus(String errorStatus) {
        ErrorStatus = errorStatus;
    }

    public String getBankID() {
        return BankID;
    }

    public void setBankID(String bankID) {
        BankID = bankID;
    }

    public String getSecurityType() {
        return SecurityType;
    }

    public void setSecurityType(String securityType) {
        SecurityType = securityType;
    }

    public String getSecurityID() {
        return SecurityID;
    }

    public void setSecurityID(String securityID) {
        SecurityID = securityID;
    }

    public String getTxnDate() {
        return TxnDate;
    }

    public void setTxnDate(String txnDate) {
        TxnDate = txnDate;
    }

    public String getSecurityPassword() {
        return SecurityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        SecurityPassword = securityPassword;
    }

    public String getMerchantID() {
        return MerchantID;
    }

    public void setMerchantID(String merchantID) {
        MerchantID = merchantID;
    }

    public String getErrorDescription() {
        return ErrorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        ErrorDescription = errorDescription;
    }

    public String getBankMerchantID() {
        return BankMerchantID;
    }

    public void setBankMerchantID(String bankMerchantID) {
        BankMerchantID = bankMerchantID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    public String getAdditionalInfo4() {
        return AdditionalInfo4;
    }

    public void setAdditionalInfo4(String additionalInfo4) {
        AdditionalInfo4 = additionalInfo4;
    }

    public String getAdditionalInfo3() {
        return AdditionalInfo3;
    }

    public void setAdditionalInfo3(String additionalInfo3) {
        AdditionalInfo3 = additionalInfo3;
    }

    public String getTxnType() {
        return TxnType;
    }

    public void setTxnType(String txnType) {
        TxnType = txnType;
    }

    public String getAdditionalInfo2() {
        return AdditionalInfo2;
    }

    public void setAdditionalInfo2(String additionalInfo2) {
        AdditionalInfo2 = additionalInfo2;
    }

    public String getCheckSum() {
        return CheckSum;
    }

    public void setCheckSum(String checkSum) {
        CheckSum = checkSum;
    }

    public String getAdditionalInfo1() {
        return AdditionalInfo1;
    }

    public void setAdditionalInfo1(String additionalInfo1) {
        AdditionalInfo1 = additionalInfo1;
    }

    public String getCheckSumFlag() {
        return CheckSumFlag;
    }

    public void setCheckSumFlag(String checkSumFlag) {
        CheckSumFlag = checkSumFlag;
    }
}
