package com.abipb.linkaccount.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Rajasekhar.E-V on 25-10-2018.
 */
@Entity
@Table(name="funds_trnsfr_dtls")
public class FundsTransferDetails implements Serializable {

    @Id
    @Column(name="app_tran_id")
    private String applicationTransactionId;

    @Column(name="status_code",nullable = true)
    private String statusCode;

    @Column(name="status_desc",nullable = true)
    private String statusDesc;

    @Column(name="reason_code",nullable = true)
    private String reasonCode;

    @Column(name="reason_desc",nullable = true)
    private String reasonDesc;

    @Column(name="transaction_id",nullable = true)
    private String transactionId;

    @Column(name="debitor_Id",nullable = true)
    private String debitorId;

    @Column(name = "creditor_Id",nullable = true)
    private String creditorId;

    @Column(name="amount",nullable = true)
    private String amount;

    @Column(name="currency_type",nullable = true)
    private String currencyType;

    @Column(name="payment_ref_no",nullable = true)
    private String paymentRefNo;

    @Column(name="payment_order_id",nullable = true)
    private String paymentOrderId;

    @Column(name="beneficiary_account",nullable = true)
    private String beneficiaryAccount;

    @Column(name="beneficiary_name",nullable = true)
    private String beneficiaryName;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getDebitorId() {
        return debitorId;
    }

    public void setDebitorId(String debitorId) {
        this.debitorId = debitorId;
    }

    public String getCreditorId() {
        return creditorId;
    }

    public void setCreditorId(String creditorId) {
        this.creditorId = creditorId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(String currencyType) {
        this.currencyType = currencyType;
    }

    public String getPaymentRefNo() {
        return paymentRefNo;
    }

    public void setPaymentRefNo(String paymentRefNo) {
        this.paymentRefNo = paymentRefNo;
    }

    public String getPaymentOrderId() {
        return paymentOrderId;
    }

    public void setPaymentOrderId(String paymentOrderId) {
        this.paymentOrderId = paymentOrderId;
    }

    public String getBeneficiaryAccount() {
        return beneficiaryAccount;
    }

    public void setBeneficiaryAccount(String beneficiaryAccount) {
        this.beneficiaryAccount = beneficiaryAccount;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }
    public String getApplicationTransactionId() {
        return applicationTransactionId;
    }

    public void setApplicationTransactionId(String applicationTransactionId) {
        this.applicationTransactionId = applicationTransactionId;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String status) {
        this.statusCode = status;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonDesc() {
        return reasonDesc;
    }

    public void setReasonDesc(String reasonDesc) {
        this.reasonDesc = reasonDesc;
    }


}
