package com.abipb.linkaccount.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;


/**
 * The persistent class for the NACH_ROLE_TABLE database table.
 * 
 */
@Entity
@Table(name="ROLE_TABLE")
@NamedQuery(name="Role.findAll", query="SELECT r FROM Role r")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ROLE_ID")
	private long RoleId;

	@Column(name="CREATED_BY")
	private String CreatedBy;

	@Column(name="CREATED_DATE")
	private Date CreatedDate;

	@Column(name="ROLE_NAME")
	private String RoleName;

	@Column(name="UPDATED_BY")
	private String UpdatedBy;

	@Column(name="UPDATED_DATE")
	private Date UpdatedDate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="customer_id")
	private UserDetails userDetails;
	public Role() {
	}

	public long getRoleId() {
		return RoleId;
	}

	public void setRoleId(long roleId) {
		RoleId = roleId;
	}

	public String getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}

	public Date getCreatedDate() {
		return CreatedDate;
	}

	public void setCreatedDate(Date createdDate) {
		CreatedDate = createdDate;
	}

	public String getRoleName() {
		return RoleName;
	}

	public void setRoleName(String roleName) {
		RoleName = roleName;
	}

	public String getUpdatedBy() {
		return UpdatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		UpdatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return UpdatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		UpdatedDate = updatedDate;
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}
}