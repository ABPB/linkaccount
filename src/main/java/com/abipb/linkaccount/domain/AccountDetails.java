package com.abipb.linkaccount.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Rajasekhar.E-V on 26-10-2018.
 */
@Entity
@Table(name="account_details")
public class AccountDetails implements Serializable {

    @Id
    @Column(name="acc_no")
    private String accountNo;

    @Column(name="ifsc_code")
    private String ifscCode;

    @Column(name="card_no")
    private String cardNo;

    @Column(name="acc_status")
    private String accountStatus;

    @Column(name="acc_type")
    private String accountType;

    @Column(name="branch_name")
    private String branchName;

    @Column(name="branch_id")
    private String branchId;

    /*@Column(name="customer_id")
    private String customerId;
*/
    /*@ManyToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn(name = "customer_id")*/
    /*@ManyToOne
    @JoinTable(name="user_details",
            joinColumns={@JoinColumn(name="acc_no", insertable=false,updatable=false)},
            inverseJoinColumns={@JoinColumn(name="customer_id", insertable=false,updatable=false)})*/
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="customer_id")
    private UserDetails userDetails;


    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

   /* public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }*/

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }
}
