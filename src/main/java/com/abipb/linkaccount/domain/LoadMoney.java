package com.abipb.linkaccount.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Rajasekhar.E-V on 25-10-2018.
 */
@Entity
@Table(name="load_money_dtls")
public class LoadMoney implements Serializable {

    @Id
    @Column(name="customer_msisdn")
    private String customerMSISDN;

    @Column(name="amount")
    private String amount;

    @Column(name="reason_code")
    private String reasonCode;

    @Column(name="idea_mny_trn_id")
    private String ideamoneyTransactionID;

    @Column(name="reason_desc")
    private String reasonDesc;

    public String getReasonDesc() {
        return reasonDesc;
    }

    public void setReasonDesc(String reasonDesc) {
        this.reasonDesc = reasonDesc;
    }

    public String getCustomerMSISDN() {
        return customerMSISDN;
    }

    public void setCustomerMSISDN(String customerMSISDN) {
        this.customerMSISDN = customerMSISDN;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getIdeamoneyTransactionID() {
        return ideamoneyTransactionID;
    }

    public void setIdeamoneyTransactionID(String ideamoneyTransactionID) {
        this.ideamoneyTransactionID = ideamoneyTransactionID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LoadMoney loadMoney = (LoadMoney) o;

        if (!customerMSISDN.equals(loadMoney.customerMSISDN)) return false;
        if (!amount.equals(loadMoney.amount)) return false;
        if (!reasonCode.equals(loadMoney.reasonCode)) return false;
        if (!ideamoneyTransactionID.equals(loadMoney.ideamoneyTransactionID)) return false;
        return reasonDesc.equals(loadMoney.reasonDesc);
    }

    @Override
    public int hashCode() {
        int result = customerMSISDN.hashCode();
        result = 31 * result + amount.hashCode();
        result = 31 * result + reasonCode.hashCode();
        result = 31 * result + ideamoneyTransactionID.hashCode();
        result = 31 * result + reasonDesc.hashCode();
        return result;
    }
}
