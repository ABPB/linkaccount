package com.abipb.linkaccount.domain;

import org.springframework.web.bind.annotation.CrossOrigin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Rajasekhar.E-V on 25-10-2018.
 */
@Entity
@Table(name="merchant_details")
public class MerchantDetails implements Serializable {

    @Id
    @Column(name="merchant_id")
    private String merchantId;

    @Column(name="merchant_name")
    private String merchantName;

    @Column(name="bank_id")
    private String  bankId;

    @Column(name="branch_id")
    private String branchId;

    @Column(name="account_id")
    private String accountId;

    @Column(name="ifsc_code")
    private String ifscCode;

    @Column(name="mobile")
    private String mobileNumber;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MerchantDetails that = (MerchantDetails) o;

        if (!merchantId.equals(that.merchantId)) return false;
        if (!merchantName.equals(that.merchantName)) return false;
        if (!bankId.equals(that.bankId)) return false;
        if (!branchId.equals(that.branchId)) return false;
        if (!accountId.equals(that.accountId)) return false;
        if (!ifscCode.equals(that.ifscCode)) return false;
        return mobileNumber.equals(that.mobileNumber);
    }

    @Override
    public int hashCode() {
        int result = merchantId.hashCode();
        result = 31 * result + merchantName.hashCode();
        result = 31 * result + bankId.hashCode();
        result = 31 * result + branchId.hashCode();
        result = 31 * result + accountId.hashCode();
        result = 31 * result + ifscCode.hashCode();
        result = 31 * result + mobileNumber.hashCode();
        return result;
    }

}
