package com.abipb.linkaccount.domain;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 * Created by Rajasekhar.E-V on 25-10-2018.
 */
@Entity
@Table(name="user_details")
public class UserDetails implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="customer_id")
    private String customerId;

    @Column(name="password")
    private String password;

    @Column(name="name")
    private String name;

    @Column(name="mobile")
    private String mobileNumber;

    @Column(name="pan_no")
    private String panNo;

    @Column(name="dob")
    private String dob;

    @Column(name="pefer_acc_id")
    private String peferAaccId;

    @Column(name="login_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date loginDate;

    @Column(name="token")
    private String token;

    /*@OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name="ROLE_ID")*/
    @OneToMany(fetch = FetchType.EAGER, mappedBy="userDetails", cascade = CascadeType.ALL)
    private Collection<Role> roles;

   // @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY, orphanRemoval = true,mappedBy = "userDetails")
   // @OneToMany(mappedBy = "userDetails", cascade = CascadeType.ALL/*,fetch = FetchType.LAZY, orphanRemoval = true*/)
  /* @OneToMany
   @JoinTable(name="account_details",joinColumns = {@JoinColumn(name="customer_id")},
           inverseJoinColumns={@JoinColumn(name="acc_no")}
   )
   @Cascade(org.hibernate.annotations.CascadeType.ALL)*/
    @OneToMany(fetch = FetchType.LAZY, mappedBy="userDetails", cascade = CascadeType.ALL)
    private Collection<AccountDetails> accountDetails;



    public String getToken() {        return token;    }

    public void setToken(String token) {        this.token = token;    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }


    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Collection<AccountDetails> getAccountDetails() {
        return accountDetails;
    }

    public void setAccountDetails(Collection<AccountDetails> accountDetails) {
        this.accountDetails = accountDetails;
    }

    public String getPeferAaccId() {
        return peferAaccId;
    }

    public void setPeferAaccId(String peferAaccId) {
        this.peferAaccId = peferAaccId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }
}
