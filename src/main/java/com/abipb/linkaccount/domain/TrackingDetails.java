package com.abipb.linkaccount.domain;


import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by pardhasaradhi.k on 11/1/2018.
 */
@Entity
@Table(name="tracking_dtls")
public class TrackingDetails implements Serializable {
    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="req_tracking_id")
    private String requestTrackingId;

    @Column(name="res_tracking_id")
    private String responseTrackingId;

    @Column(name="status_code")
    private String statusCode;

    @Column(name="status_desc")
    private String statusDesc;

    @Column(name="service_name")
    private String serviceName;

    public String getRequestTrackingId() {
        return requestTrackingId;
    }

    public void setRequestTrackingId(String requestTrackingId) {
        this.requestTrackingId = requestTrackingId;
    }

    public String getResponseTrackingId() {
        return responseTrackingId;
    }

    public void setResponseTrackingId(String responseTrackingId) {
        this.responseTrackingId = responseTrackingId;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TrackingDetails that = (TrackingDetails) o;

        if (!requestTrackingId.equals(that.requestTrackingId)) return false;
        if (!responseTrackingId.equals(that.responseTrackingId)) return false;
        if (!statusCode.equals(that.statusCode)) return false;
        if (!statusDesc.equals(that.statusDesc)) return false;
        return serviceName.equals(that.serviceName);
    }

    @Override
    public int hashCode() {
        int result = requestTrackingId.hashCode();
        result = 31 * result + responseTrackingId.hashCode();
        result = 31 * result + statusCode.hashCode();
        result = 31 * result + statusDesc.hashCode();
        result = 31 * result + serviceName.hashCode();
        return result;
    }
}
