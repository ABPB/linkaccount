package com.abipb.linkaccount.service;

import com.abipb.linkaccount.payload.loginmpin.LogInRequest;
import com.abipb.linkaccount.payload.loginmpin.LogInResponse;

/**
 * Created by pardhasaradhi.k on 10/31/2018.
 */
public interface OCHLoginService {
    public LogInResponse authenticateUser(LogInRequest ochLogInMpinRequest);
}
