package com.abipb.linkaccount.service;

import com.abipb.linkaccount.common.payload.ADBServiceResponse;
import com.abipb.linkaccount.payload.fundtransfer.FundsTransferRequest;
import com.abipb.linkaccount.payload.fundtransfer.request.FundTransferRequest;

import javax.ws.rs.core.Response;

/**
 * Created by pardhasaradhi.k on 10/12/2018.
 */
public interface FundsTransferService {

    public ADBServiceResponse fundsTransfer(FundsTransferRequest fundTransferRequest);
}
