package com.abipb.linkaccount.service;

import com.abipb.linkaccount.exception.LinkAccountServiceException;
import com.abipb.linkaccount.payload.LocateAgentDetil;

import java.util.List;

/**
 * Created by Abhay.Kumar on 9/24/2018.
 */
public interface LocateAgentService {
    public List<LocateAgentDetil> fetchLocateAgent(LocateAgentDetil locateAgentDetil) throws LinkAccountServiceException;
}
