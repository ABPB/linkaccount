package com.abipb.linkaccount.service;

import com.abipb.linkaccount.payload.CustomerAccountSummary.response.CustomerAccountSummaryRequestApp;
import com.abipb.linkaccount.payload.CustomerAccountSummary.response.CustomerAccountSummaryResponse;
import com.abipb.linkaccount.payload.accountDetails.AccountRetrievalRequest;
import com.abipb.linkaccount.payload.accountDetails.response.AccountDetailsResponse;
import com.abipb.linkaccount.payload.loginmpin.Accounts;

/**
 * Created by pardhasaradhi.k on 10/26/2018.
 */
public interface AccountDetailsService {
    public AccountDetailsResponse getAccountDetails(AccountRetrievalRequest accountRetrievalRequest);
    public CustomerAccountSummaryResponse getCustomerSummaryData(CustomerAccountSummaryRequestApp customerAccountSummaryRequest);

    Accounts savePreferAccId(String accountId);
}
