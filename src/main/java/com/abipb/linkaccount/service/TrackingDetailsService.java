package com.abipb.linkaccount.service;

import com.abipb.linkaccount.domain.DATA;
import com.abipb.linkaccount.domain.TrackingDetails;

/**
 * Created by Rajasekhar.E-V on 08-11-2018.
 */
public interface TrackingDetailsService {
    public TrackingDetails getTrackingDetails(String trackingId);
    DATA getBilldeskTransactionDetails(String trackingId);
}
