package com.abipb.linkaccount.service;

import com.abipb.linkaccount.common.payload.ADBServiceResponse;
import com.abipb.linkaccount.domain.DATA;
import com.abipb.linkaccount.payload.billdesk.request.BillDeskIIBLoadMoneyRequest;
import com.abipb.linkaccount.payload.loadmoney.request.LoadMoneyRequest;

/**
 * Created by pardhasaradhi.k on 10/12/2018.
 */
public interface LoadMoneyService {
     ADBServiceResponse depositMoney(LoadMoneyRequest loadMoneyRequest);

    String generateBilldeskUrl(BillDeskIIBLoadMoneyRequest billDeskIIBLoadMoneyRequest);
    String  saveBillDeskResponse(DATA data);
}
