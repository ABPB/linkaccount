package com.abipb.linkaccount.service.impl;

import com.abipb.linkaccount.common.payload.ADBMessageHeader;
import com.abipb.linkaccount.common.payload.ADBServiceResponse;
import com.abipb.linkaccount.common.payload.StatusInfo;
import com.abipb.linkaccount.domain.DATA;
import com.abipb.linkaccount.domain.LoadMoney;
import com.abipb.linkaccount.payload.LoadMnyResp;
import com.abipb.linkaccount.payload.billdesk.request.BillDeskIIBLoadMoneyRequest;
import com.abipb.linkaccount.payload.loadmoney.request.LoadMoneyRequest;
import com.abipb.linkaccount.payload.loadmoney.response.LoadMoneyResponse;
import com.abipb.linkaccount.repository.AccountDetailsRepository;
import com.abipb.linkaccount.repository.BillDeskLoadMoneyResponseRepository;
import com.abipb.linkaccount.repository.LoadMoneyRepository;
import com.abipb.linkaccount.repository.UserDetailsRepository;
import com.abipb.linkaccount.service.LoadMoneyService;
import com.abipb.linkaccount.utils.HMAC;
import com.abipb.linkaccount.utils.HTTPPostClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by pardhasaradhi.k on 10/12/2018.
 */
@Service
public class LoadMoneyServiceImpl implements LoadMoneyService{

    private static final org.slf4j.Logger log= LoggerFactory.getLogger(LoadMoneyServiceImpl.class);
    @Autowired
    LoadMoneyRepository loadMoneyRepository;

    @Value("${iib.uat.url.loadMoney}")
    private String loadMoneyUrl;

    @Value("${billdesk.return.uat.url.loadMoney}")
    private  String returnURL;

    @Value("${wallet.merchant.id}")
    private  String walletMerchantId;

    @Value("${saving.merchant.id}")
    private  String savingMerchantId;

    @Value("${wallet.checksumKey}")
    private  String walletCheckSumKey;

    @Value("${saving.checksumKey}")
    private  String savingChecksumKey;

    @Value("${billdesk.url}")
    private  String billdeskUrl;

    @Autowired
    UserDetailsRepository userDetailsRepository;

    @Autowired
    AccountDetailsRepository accountDetailsRepository;

    @Autowired
    BillDeskLoadMoneyResponseRepository billDeskLoadMoneyResponseRepository;

    private String merchantReturnUrl;


@Transactional
    public ADBServiceResponse depositMoney(LoadMoneyRequest loadMoneyRequest){

        log.info("Entering depositMoney(LoadMoneyRequest loadMoneyRequest) method");
        ObjectMapper objectMapper=new ObjectMapper();
        String requestString=null;
        String responseString =null;
        LoadMoneyResponse loadMoneyResponse=null;
        LoadMnyResp loadMnyResp=null;
        ADBServiceResponse adbServiceResponse=new ADBServiceResponse();

        try {
            requestString =objectMapper.writeValueAsString(loadMoneyRequest);
            responseString= HTTPPostClient.callHttpPost(loadMoneyUrl,null,null,requestString);
            loadMoneyResponse=objectMapper.readValue(responseString,LoadMoneyResponse.class);

            log.info("Response Code from IIB for"+loadMoneyRequest.getLoadOrWithdrawMoney().getFundTransferDetails().getCustomerMSISDN()+" is "+loadMoneyResponse.getADBServiceResponse().getStatus().getReasonCode());
            log.info("Response Desc from IIB for"+loadMoneyRequest.getLoadOrWithdrawMoney().getFundTransferDetails().getCustomerMSISDN()+" is "+loadMoneyResponse.getADBServiceResponse().getStatus().getReasonDesc());

            if("000".equalsIgnoreCase(loadMoneyResponse.getADBServiceResponse().getStatus().getReasonCode())){
                LoadMoney loadMoneyDomain=new LoadMoney();
                loadMoneyDomain.setAmount(loadMoneyRequest.getLoadOrWithdrawMoney().getFundTransferDetails().getAmount());
                loadMoneyDomain.setCustomerMSISDN(loadMoneyRequest.getLoadOrWithdrawMoney().getFundTransferDetails().getCustomerMSISDN());
                loadMoneyDomain.setIdeamoneyTransactionID(loadMoneyRequest.getLoadOrWithdrawMoney().getFundTransferDetails().getIdeamoneyTransactionID());
                loadMoneyDomain.setReasonCode(loadMoneyResponse.getADBServiceResponse().getStatus().getReasonCode());
                loadMoneyDomain.setReasonDesc(loadMoneyResponse.getADBServiceResponse().getStatus().getReasonDesc());

                loadMoneyRepository.save(loadMoneyDomain);

                loadMnyResp=new LoadMnyResp();
                loadMnyResp.setReasonCode(loadMoneyResponse.getADBServiceResponse().getStatus().getReasonCode());
                loadMnyResp.setReasonDescription(loadMoneyResponse.getADBServiceResponse().getStatus().getReasonDesc());
            }

            adbServiceResponse  =new ADBServiceResponse();
            adbServiceResponse.setAdbMessageHeader(new ADBMessageHeader());
            adbServiceResponse.setResponseData(loadMnyResp);
            adbServiceResponse.setStatusInfo(new StatusInfo());

        } catch (JsonProcessingException e) {
            log.error("exceptino in depositMoney(LoadMoneyRequest loadMoneyRequest) method"+e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            log.error("exception in depositMoney(LoadMoneyRequest loadMoneyRequest) method"+e.getMessage());
            e.printStackTrace();
        }

        log.info("Exiting depositMoney(LoadMoneyRequest loadMoneyRequest) method");
        return adbServiceResponse;
    }

    @Override
    @Transactional
    public String generateBilldeskUrl(BillDeskIIBLoadMoneyRequest billDeskIIBLoadMoneyRequest) {
       merchantReturnUrl=billDeskIIBLoadMoneyRequest.getReturnUrl();
        // Parameters at server end
        String uniqueCustID = new SimpleDateFormat("yyMMddHHmmss").format(
                new Date()).toString();
        String checkSumKey = "";
        String merchantId="";
        String reqDate = new SimpleDateFormat("yyyyMMddHHmmss").format(
                new Date()).toString();
        if (billDeskIIBLoadMoneyRequest.getAccountType().toUpperCase().contains("SAVING")) {
            merchantId = savingMerchantId;
            checkSumKey = savingChecksumKey;
        } else if (billDeskIIBLoadMoneyRequest.getAccountType().toUpperCase().contains("WALLET")) {
            merchantId = walletMerchantId;
            checkSumKey = savingChecksumKey;
        }
       // AccountDetails accountDetails=accountDetailsRepository.findOne(billDeskIIBLoadMoneyRequest.getAccountNo());
        String tempReq = uniqueCustID + "|" + billDeskIIBLoadMoneyRequest.getAccountNo() + "|" + billDeskIIBLoadMoneyRequest.getAmount() + "|"
                + /*billDeskIIBLoadMoneyRequest.getChannel()*/ "WEB"+ "|" + returnURL + "|" + merchantId + "|" + billDeskIIBLoadMoneyRequest.getMobileNo() + "|NA|"
                + reqDate;

        String tempChckSum = HMAC.hmacSHA256(tempReq, checkSumKey);
        // Final request
        String finalReq = billdeskUrl+tempReq + "|" + tempChckSum;
        return finalReq;
    }

    @Override
    @Transactional
    public String saveBillDeskResponse(DATA data) {

        billDeskLoadMoneyResponseRepository.save(data);
        String status=data.getErrorDescription();
        String txnReferenceNo=data.getTxnReferenceNo();
        return merchantReturnUrl+"?status="+status+"&txnReferenceNo="+txnReferenceNo;
    }
}
