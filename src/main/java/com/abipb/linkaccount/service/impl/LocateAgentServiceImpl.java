package com.abipb.linkaccount.service.impl;

import com.abipb.linkaccount.exception.LinkAccountServiceException;
import com.abipb.linkaccount.payload.LocateAgentDetil;
import com.abipb.linkaccount.repository.LocateAgentRepository;
import com.abipb.linkaccount.service.LocateAgentService;
import com.abipb.linkaccount.utils.MPesaPropertyValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Abhay.Kumar on 9/24/2018.
 */
@Service
public class LocateAgentServiceImpl implements LocateAgentService {
    private static final Logger LOGGER= LoggerFactory.getLogger(LocateAgentServiceImpl.class);
    @Autowired
    private LocateAgentRepository locateAgentRepository;

    @Override
    public List<LocateAgentDetil> fetchLocateAgent(LocateAgentDetil locateAgentDetil) throws LinkAccountServiceException {

        try{
            if(StringUtils.isEmpty(locateAgentDetil)){
                LOGGER.debug(MPesaPropertyValue.getValue("resource.locateAgent.MP0001"));
                return Arrays.asList();
            }
            return locateAgentRepository.fetchLocateAgent(locateAgentDetil);
        }catch (Exception exception){
        LOGGER.error("Exception");
        }
        return null;
    }
}
