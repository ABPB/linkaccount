package com.abipb.linkaccount.service.impl;

import com.abipb.linkaccount.common.payload.ADBMessageHeader;
import com.abipb.linkaccount.common.payload.ADBServiceResponse;
import com.abipb.linkaccount.common.payload.StatusInfo;
import com.abipb.linkaccount.domain.TrackingDetails;
import com.abipb.linkaccount.payload.BalanceEnqryResp;
import com.abipb.linkaccount.payload.balanceenquiry.BalanceEnquiryRequest;
import com.abipb.linkaccount.payload.balanceenquiry.request.ADBServiceRequest;
import com.abipb.linkaccount.payload.balanceenquiry.response.BalanceEnquiryResponse;
import com.abipb.linkaccount.repository.TrackingDetailsRepository;
import com.abipb.linkaccount.service.BalanceEnquiryService;
import com.abipb.linkaccount.utils.Constants;
import com.abipb.linkaccount.utils.HTTPPostClient;
import com.abipb.linkaccount.utils.RSAEncDec;
import com.abipb.linkaccount.utils.RequestsGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

/**
 * Created by pardhasaradhi.k on 10/12/2018.
 */
@Service
public class BalanceEnquiryServiceImpl implements BalanceEnquiryService {
    private static final org.slf4j.Logger log= LoggerFactory.getLogger(BalanceEnquiryServiceImpl.class);


    @Autowired
    TrackingDetailsRepository trackingDetailsRepository;

    @Value("${iib.uat.url}")
    private String iibUrl;

    @Override
    @Transactional
    public BalanceEnqryResp getBalance(BalanceEnquiryRequest balanceEnquiryRequest) {
        System.out.println(balanceEnquiryRequest);
        ObjectMapper objectMapper=new ObjectMapper();
        String requestString=null;
        BalanceEnqryResp balanceEnqryResp=null;
        com.abipb.linkaccount.payload.balanceenquiry.request.BalanceEnquiryRequest iibBalanceEnquiryRequest=new com.abipb.linkaccount.payload.balanceenquiry.request.BalanceEnquiryRequest();

        try {
//            requestString=  objectMapper.writeValueAsString(balanceEnquiryRequest);

            String transactionId= Constants.TRANSACTION_ID_PREFIX.getConstant();
            transactionId=transactionId+trackingDetailsRepository.count();

            com.abipb.linkaccount.payload.balanceenquiry.request.BalanceEnquiryRequest balanceEnquiryRequest1=RequestsGenerator.getBalanceEnquiryRequestGenerator(balanceEnquiryRequest,transactionId);
            String requestString1=objectMapper.writeValueAsString(balanceEnquiryRequest1);
            String responseString=  HTTPPostClient.callHttpPost(iibUrl,null,null,requestString1);
            BalanceEnquiryResponse balanceEnquiryResponse=null;
            balanceEnquiryResponse=    objectMapper.readValue(responseString, BalanceEnquiryResponse.class);

            String statusCode=  balanceEnquiryResponse.getADBServiceResponse().getStatus().getReasonCode();
            String balanceValue=balanceEnquiryResponse.getADBServiceResponse().getResponseData().getAccountDlts().get(0).getBalanceInfo().get(0).getBalanceVal();
            String currency=balanceEnquiryResponse.getADBServiceResponse().getResponseData().getAccountDlts().get(0).getBalanceInfo().get(0).getCurrency();

            if("000".equalsIgnoreCase(statusCode)&& balanceValue!=null&&!"".equalsIgnoreCase(balanceValue)) {
                balanceEnqryResp=new BalanceEnqryResp();
                balanceEnqryResp.setStatus(statusCode);
                balanceEnqryResp.setCurrencyType(currency);
                balanceEnqryResp.setAmount(balanceEnquiryResponse.getADBServiceResponse().getResponseData().getAccountDlts().get(0).getBalanceInfo().get(0).getBalanceVal());
            }


            TrackingDetails trackingDetails=new TrackingDetails();


            trackingDetails.setRequestTrackingId(transactionId);
            trackingDetails.setResponseTrackingId(balanceEnquiryResponse.getADBServiceResponse().getADBMsgHdr().getMsgIdentifyGrp().getApplicationTranId());
            trackingDetails.setStatusCode(balanceEnquiryResponse.getADBServiceResponse().getStatus().getReasonCode());
            trackingDetails.setStatusDesc(balanceEnquiryResponse.getADBServiceResponse().getStatus().getReasonDesc());
            trackingDetails.setServiceName("Balance Enquiry Details");
            trackingDetailsRepository.save(trackingDetails);


        }catch (JsonProcessingException e){
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

       /* ADBServiceResponse adbServiceResponse=new ADBServiceResponse();
        adbServiceResponse.setAdbMessageHeader(new ADBMessageHeader());
        adbServiceResponse.setResponseData(balanceEnqryResp);
        adbServiceResponse.setStatusInfo(new StatusInfo());*/
        return balanceEnqryResp;
    }
}
