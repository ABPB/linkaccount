package com.abipb.linkaccount.service.impl;

import com.abipb.linkaccount.common.payload.ADBMessageHeader;
import com.abipb.linkaccount.common.payload.ADBServiceResponse;
import com.abipb.linkaccount.common.payload.StatusInfo;
import com.abipb.linkaccount.domain.FundsTransferDetails;
import com.abipb.linkaccount.domain.TrackingDetails;
import com.abipb.linkaccount.payload.FundsTrnsResp;
import com.abipb.linkaccount.payload.fundtransfer.FundsTransferRequest;
import com.abipb.linkaccount.payload.fundtransfer.request.FundTransferRequest;
import com.abipb.linkaccount.payload.fundtransfer.response.FundTransferResponse;
import com.abipb.linkaccount.repository.FundsTransferDetailsRepository;
import com.abipb.linkaccount.repository.TrackingDetailsRepository;
import com.abipb.linkaccount.service.FundsTransferService;
import com.abipb.linkaccount.utils.Constants;
import com.abipb.linkaccount.utils.HTTPPostClient;
import com.abipb.linkaccount.utils.RequestsGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.io.IOException;

/**
 * Created by pardhasaradhi.k on 10/12/2018.
 */
@Service
public class FundsTransferServiceImpl implements FundsTransferService{

    private static final org.slf4j.Logger log= LoggerFactory.getLogger(FundsTransferServiceImpl.class);
    @Autowired
    FundsTransferDetailsRepository fundsTransferDetailsRepository;

    @Autowired
    TrackingDetailsRepository trackingDetailsRepository;

    @Value("${iib.uat.url.fundsTransfer}")
    private String iibFundsTransfer;


    @Transactional
    public ADBServiceResponse fundsTransfer(FundsTransferRequest fundTransferRequests){
        ObjectMapper objectMapper=new ObjectMapper();
        String requestString=null;
        String responseString=null;
        FundTransferResponse fundTransferResponse=null;
        FundsTrnsResp fundsTrnsResp=null;
        ADBServiceResponse adbServiceResponse=null;
        try {
//            requestString=   objectMapper.writeValueAsString(fundTransferRequest);
            String transactionId= Constants.TRANSACTION_ID_PREFIX.getConstant();
            transactionId=transactionId+trackingDetailsRepository.count();

            FundTransferRequest fundTransferRequest=  RequestsGenerator.getFundTransferRequest(fundTransferRequests,transactionId);
            requestString=   objectMapper.writeValueAsString(fundTransferRequest);
            responseString= HTTPPostClient.callHttpPost(iibFundsTransfer,null,null,requestString);
            fundTransferResponse=objectMapper.readValue(responseString,FundTransferResponse.class);
            FundsTransferDetails fundsTransferDetailsDomain=new FundsTransferDetails();

            fundsTransferDetailsDomain.setApplicationTransactionId(StringUtils.isEmpty(fundTransferRequest.getADBServiceRequest().getADBMsgHdr().getMsgIdentifyGrp().getApplicationTranId())?null:fundTransferRequest.getADBServiceRequest().getADBMsgHdr().getMsgIdentifyGrp().getApplicationTranId());
            fundsTransferDetailsDomain.setAmount(fundTransferRequest.getADBServiceRequest().getRequestData().getRemitInfo().getRemitAmount());
            fundsTransferDetailsDomain.setCreditorId(fundTransferRequest.getADBServiceRequest().getRequestData().getCreditorInfo().getBankDetails().getAccountId());
            fundsTransferDetailsDomain.setCurrencyType(fundTransferRequest.getADBServiceRequest().getRequestData().getRemitInfo().getRemitCurrency());
            fundsTransferDetailsDomain.setDebitorId(fundTransferRequest.getADBServiceRequest().getRequestData().getDebitorInfo().getAccountId());
            fundsTransferDetailsDomain.setBeneficiaryAccount(fundTransferResponse.getADBServiceResponse().getResponseData().getBenificiaryAccount());
            fundsTransferDetailsDomain.setBeneficiaryName(fundTransferResponse.getADBServiceResponse().getResponseData().getBeneficiaryName());
            fundsTransferDetailsDomain.setPaymentOrderId(fundTransferResponse.getADBServiceResponse().getResponseData().getPaymentOrderId());
            fundsTransferDetailsDomain.setPaymentRefNo(fundTransferResponse.getADBServiceResponse().getResponseData().getPaymentRefNum());
            fundsTransferDetailsDomain.setTransactionId(fundTransferResponse.getADBServiceResponse().getResponseData().getTranId());
            fundsTransferDetailsDomain.setReasonCode(fundTransferResponse.getADBServiceResponse().getStatus().getReasonCode());
            fundsTransferDetailsDomain.setReasonDesc(StringUtils.isEmpty(fundTransferResponse.getADBServiceResponse().getStatus().getReasonDesc())?null:fundTransferResponse.getADBServiceResponse().getStatus().getReasonDesc());

            if(!"000".equalsIgnoreCase(fundTransferResponse.getADBServiceResponse().getStatus().getReasonCode())) {
                fundsTransferDetailsDomain.setStatusCode(fundTransferResponse.getADBServiceResponse().getStatus().getProviderStatus().getStatusCode());
                fundsTransferDetailsDomain.setStatusDesc(fundTransferResponse.getADBServiceResponse().getStatus().getProviderStatus().getStatusDesc());
            }

            fundsTransferDetailsRepository.save(fundsTransferDetailsDomain);

            fundsTrnsResp=new FundsTrnsResp();
            fundsTrnsResp.setReason(fundTransferResponse.getADBServiceResponse().getStatus().getReasonCode());
            fundsTrnsResp.setReasonDesc(fundTransferResponse.getADBServiceResponse().getStatus().getReasonDesc());

            TrackingDetails trackingDetails=new TrackingDetails();
            trackingDetails.setRequestTrackingId(transactionId);
            trackingDetails.setResponseTrackingId(fundTransferRequest.getADBServiceRequest().getADBMsgHdr().getMsgIdentifyGrp().getApplicationTranId());
            trackingDetails.setServiceName("Fund Transfer");
            trackingDetails.setStatusCode(fundTransferResponse.getADBServiceResponse().getStatus().getProviderStatus().getStatusCode());
            trackingDetails.setStatusDesc(fundTransferResponse.getADBServiceResponse().getStatus().getProviderStatus().getStatusDesc());
            trackingDetailsRepository.save(trackingDetails);

             adbServiceResponse  =new ADBServiceResponse();
            adbServiceResponse.setAdbMessageHeader(new ADBMessageHeader());
            adbServiceResponse.setResponseData(fundsTrnsResp);
            adbServiceResponse.setStatusInfo(new StatusInfo());

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }catch(IOException io){
            io.printStackTrace();
        }

        return adbServiceResponse;
    }
}
