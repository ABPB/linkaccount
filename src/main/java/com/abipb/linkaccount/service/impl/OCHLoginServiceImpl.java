package com.abipb.linkaccount.service.impl;

import com.abipb.linkaccount.binding.och.response.LoginMPINResponse;
import com.abipb.linkaccount.domain.TrackingDetails;
import com.abipb.linkaccount.domain.UserDetails;
import com.abipb.linkaccount.payload.BalanceEnqryResp;
import com.abipb.linkaccount.payload.CustomerAccountSummary.response.CustomerAccountSummaryRequestApp;
import com.abipb.linkaccount.payload.CustomerAccountSummary.response.CustomerAccountSummaryResponse;
import com.abipb.linkaccount.payload.CustomerAccountSummary.response.Custsummary;
import com.abipb.linkaccount.payload.accountDetails.response.AccountDetailsResponse;
import com.abipb.linkaccount.payload.balanceenquiry.BalanceEnquiryRequest;
import com.abipb.linkaccount.payload.loginmpin.Accounts;
import com.abipb.linkaccount.payload.loginmpin.LogInRequest;
import com.abipb.linkaccount.payload.loginmpin.LogInResponse;
import com.abipb.linkaccount.repository.AccountDetailsRepository;
import com.abipb.linkaccount.repository.TrackingDetailsRepository;
import com.abipb.linkaccount.repository.UserDetailsRepository;
import com.abipb.linkaccount.service.AccountDetailsService;
import com.abipb.linkaccount.service.BalanceEnquiryService;
import com.abipb.linkaccount.service.OCHLoginService;
import com.abipb.linkaccount.utils.Constants;
import com.abipb.linkaccount.utils.HTTPPostClient;
import com.abipb.linkaccount.utils.RequestsGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by pardhasaradhi.k on 10/31/2018.
 */
@Service
public class OCHLoginServiceImpl implements OCHLoginService {
    private static Logger logger= LoggerFactory.getLogger(OCHLoginServiceImpl.class);
    @Autowired
    TrackingDetailsRepository trackingDetailsRepository;

    @Autowired
    AccountDetailsService accountDetailsService;

    @Autowired
    UserDetailsRepository userDetailsRepository;

    @Autowired
    BalanceEnquiryService balanceEnquiryService;



    @Transactional
    public LogInResponse authenticateUser(LogInRequest ochLogInMpinRequest) {

        LogInResponse logInResponse = null;
        LoginMPINResponse loginMPINResponse = null;
        String cifId = null;
        String responseString = null;
        String transactionId = Constants.TRANSACTION_ID_PREFIX.getConstant();
        transactionId = transactionId + trackingDetailsRepository.count();
        AccountDetailsResponse accountDetailsResponse = null;
        String token = ochLogInMpinRequest.getToken();
        UserDetails userDetails = null;
        String requesttXML = RequestsGenerator.getOchLogInMpinRequest(ochLogInMpinRequest);
        CustomerAccountSummaryResponse customerAccountSummaryResponse = null;
        List<BalanceEnqryResp> balanceEnqryRespList = null;

        responseString = HTTPPostClient.callOchXmlHttpPost(requesttXML);

        if (!"".equalsIgnoreCase(responseString) && responseString != null) {
            Pattern pattern = Pattern.compile("<USER_ID>(.+?)</USER_ID>");
            Matcher matcher = pattern.matcher(responseString);
            if (matcher.find()) {
                cifId = matcher.group(1);
            }
        }
        if (!StringUtils.isEmpty(cifId)) {
            CustomerAccountSummaryRequestApp customerAccountSummaryRequestApp = new CustomerAccountSummaryRequestApp();
            customerAccountSummaryRequestApp.setAccountrole("CUST");
            customerAccountSummaryRequestApp.setCustomerNumber(cifId);
            customerAccountSummaryResponse = accountDetailsService.getCustomerSummaryData(customerAccountSummaryRequestApp);

            userDetails = userDetailsRepository.findOne(cifId);
        }
        String userTokenId = userDetails.getToken();
        Date currentDate = new Date();
        Date loginDate = userDetails.getLoginDate();

        long duration = currentDate.getTime() - loginDate.getTime();

        long diffInSeconds = TimeUnit.MILLISECONDS.toSeconds(duration);
        long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
        long diffInHours = TimeUnit.MILLISECONDS.toHours(duration);
        long diffInDays = TimeUnit.MILLISECONDS.toDays(duration);

        if (!"".equalsIgnoreCase(token) && token != null && token.equalsIgnoreCase(userTokenId)) {
            BalanceEnquiryRequest balanceEnquiryRequest = new BalanceEnquiryRequest();
            balanceEnquiryRequest.setAccountId(userDetails.getPeferAaccId());
            balanceEnquiryRequest.setCustomerId(cifId);
            balanceEnquiryRequest.setToken(ochLogInMpinRequest.getToken());
            BalanceEnqryResp balanceEnqryResp = balanceEnquiryService.getBalance(balanceEnquiryRequest);
            logInResponse = new LogInResponse();
            logInResponse.setToken(userTokenId);
            logInResponse.setBalnceAmount(balanceEnqryResp.getAmount());
            logInResponse.setCustomerId(cifId);
            logInResponse.setStatus(balanceEnqryResp.getStatus());
            logInResponse.setStatusCode(balanceEnqryResp.getStatus());
            return logInResponse;
        } else {
            try {
           /* String requesttXML= RequestsGenerator.getOchLogInMpinRequest(ochLogInMpinRequest);
            responseString=HTTPPostClient.callOchXmlHttpPost(requesttXML);
            if(!"".equalsIgnoreCase(responseString)&&responseString!=null){
            Pattern pattern=Pattern.compile("<USER_ID>(.+?)</USER_ID>");
            Matcher matcher=pattern.matcher(responseString);
            if (matcher.find()) {
                cifId= matcher.group(1);
            }
           }*/
          /*  JAXBContext jaxbContext = JAXBContext.newInstance(LoginMPINResponse.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            loginMPINResponse = (LoginMPINResponse) unmarshaller.unmarshal(new StringReader(responseString));
            if(loginMPINResponse != null) {
                String sessionId = loginMPINResponse.getHeader().getSESSION().getSESSIONID();
                if (!"".equalsIgnoreCase(sessionId) && sessionId != null) {
                    if (!"".equalsIgnoreCase(cifId) && cifId != null) {
                        AccountRetrievalRequest accountRetrievalRequest=new AccountRetrievalRequest();
                        accountRetrievalRequest.setCustomerNumber(cifId);
                        accountRetrievalRequest.setDeviceId(ochLogInMpinRequest.getIpAddress());
                        accountRetrievalRequest.setToken(transactionId);
                        //AccountDetailsRequest accountDetailsRequest = RequestsGenerator.getAccountDetailsRequest(transactionId, cifId, ochLogInMpinRequest.getIpAddress());
                        if (accountRetrievalRequest != null) {
                            accountDetailsResponse=  accountDetailsService.getAccountDetails(accountRetrievalRequest);
                        }
                    }
                }
            }
        }catch (Exception e){
            logger.error(e.getMessage());
        }*/

                if (!StringUtils.isEmpty(customerAccountSummaryResponse)) {
                    BalanceEnquiryRequest balanceEnquiryRequest = new BalanceEnquiryRequest();
                    BalanceEnqryResp balanceEnqryResp = new BalanceEnqryResp();
                    List<Accounts> accountsList = new ArrayList<Accounts>();

                    balanceEnqryRespList = new ArrayList<>();
                    balanceEnquiryRequest.setCustomerId(customerAccountSummaryResponse.getAdbServiceResponse().getResponseData().getCustomerId());
                    ArrayList<Custsummary> custsummaryList = (ArrayList<Custsummary>) customerAccountSummaryResponse.getAdbServiceResponse().getResponseData().getCustsummary();
                    for (Custsummary custsummary : custsummaryList) {
                        balanceEnquiryRequest.setAccountId(custsummary.getAccountNumber());
                        balanceEnqryResp = balanceEnquiryService.getBalance(balanceEnquiryRequest);
                        Accounts accounts = new Accounts();
                        accounts.setAccountNumber(custsummary.getAccountNumber());
                        accounts.setAccountType(custsummary.getAccountType());
                        accounts.setBalanceAmount(balanceEnqryResp.getAmount());
                        accountsList.add(accounts);

                    }

                    logInResponse = new LogInResponse();
                    logInResponse.setStatus(customerAccountSummaryResponse.getAdbServiceResponse().getStatus().getReasonDesc());
                    logInResponse.setStatusCode(customerAccountSummaryResponse.getAdbServiceResponse().getStatus().getReasonCode());
                    logInResponse.setCustomerId(customerAccountSummaryResponse.getAdbServiceResponse().getResponseData().getCustomerId());
                    logInResponse.setToken(userDetailsRepository.findOne(cifId).getToken());
                    logInResponse.setAccounts(accountsList);

                }
                JAXBContext jaxbContext = JAXBContext.newInstance(LoginMPINResponse.class);
                Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                loginMPINResponse = (LoginMPINResponse) unmarshaller.unmarshal(new StringReader(responseString));
                TrackingDetails trackingDetails = new TrackingDetails();
                trackingDetails.setRequestTrackingId(transactionId);
                trackingDetails.setResponseTrackingId("");
                trackingDetails.setStatusCode(loginMPINResponse.getHeader().getSTATUS().getMESSAGE().get(0).getMESSAGECODE());
                trackingDetails.setStatusDesc(loginMPINResponse.getHeader().getSTATUS().getMESSAGE().get(0).getMESSAGECODE());
                trackingDetails.setServiceName("OCHLogInMpin");
                trackingDetailsRepository.save(trackingDetails);
             /*logInResponse=new LogInResponse();
             logInResponse.setStatus(accountDetailsResponse.getAdbServiceResponse().getStatus().getReasonDesc());
            logInResponse.setStatusCode(accountDetailsResponse.getAdbServiceResponse().getStatus().getReasonCode());
            logInResponse.setCustomerId(accountDetailsResponse.getAdbServiceResponse().getResponseData().getCustomerId());
            logInResponse.setToken(userDetailsRepository.findOne(cifId).getToken());
            List<com.abipb.linkaccount.payload.accountDetails.response.AccountDetails> accountDetails=(List<com.abipb.linkaccount.payload.accountDetails.response.AccountDetails>) accountDetailsResponse.getAdbServiceResponse().getResponseData().getAccountDetails();
            List<Accounts> accountsList=new ArrayList<Accounts>();
            for (com.abipb.linkaccount.payload.accountDetails.response.AccountDetails accountDetails1:
                    accountDetails) {
                Accounts accounts=new Accounts();
                accounts.setAccountNumber(accountDetails1.getAccountNumber());
                accounts.setAccountType(accountDetails1.getAccountType());
                accounts.setBalanceAmount(accountDetails1.getAvalBalance());
                accountsList.add(accounts);
            }
            logInResponse.setAccounts(accountsList);
            return  logInResponse;*/
        /*else {
            return  logInResponse;
        }*/
            } catch (Exception e) {
                logger.error(e.getLocalizedMessage());
            }
            return logInResponse;

        }
    }
}
