package com.abipb.linkaccount.service.impl;

import com.abipb.linkaccount.common.payload.ADBServiceResponse;
import com.abipb.linkaccount.domain.Role;
import com.abipb.linkaccount.domain.TrackingDetails;
import com.abipb.linkaccount.domain.UserDetails;
import com.abipb.linkaccount.payload.AccountDtlsResp;
import com.abipb.linkaccount.payload.CustomerAccountSummary.request.CustomerAccountSummaryRequest;
import com.abipb.linkaccount.payload.CustomerAccountSummary.response.CustomerAccountSummaryRequestApp;
import com.abipb.linkaccount.payload.CustomerAccountSummary.response.CustomerAccountSummaryResponse;
import com.abipb.linkaccount.payload.CustomerAccountSummary.response.Custsummary;
import com.abipb.linkaccount.payload.accountDetails.AccountRetrievalRequest;
import com.abipb.linkaccount.payload.accountDetails.request.AccountDetailsRequest;
import com.abipb.linkaccount.payload.accountDetails.response.AccountDetails;
import com.abipb.linkaccount.payload.accountDetails.response.AccountDetailsResponse;
import com.abipb.linkaccount.payload.loginmpin.Accounts;
import com.abipb.linkaccount.repository.AccountDetailsRepository;
import com.abipb.linkaccount.repository.TrackingDetailsRepository;
import com.abipb.linkaccount.repository.UserDetailsRepository;
import com.abipb.linkaccount.security.entity.UserDetail;
import com.abipb.linkaccount.service.AccountDetailsService;
import com.abipb.linkaccount.utils.Constants;
import com.abipb.linkaccount.utils.HMAC;
import com.abipb.linkaccount.utils.HTTPPostClient;
import com.abipb.linkaccount.utils.RequestsGenerator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by pardhasaradhi.k on 10/26/2018.
 */
@Service
public class AccountDetailsServiceImpl implements AccountDetailsService {

        private static Logger logger= LoggerFactory.getLogger(AccountDetailsServiceImpl.class);

        @Autowired
        UserDetailsRepository userDetailsRepository;

        @Autowired
        TrackingDetailsRepository trackingDetailsRepository;

        @Autowired
        AccountDetailsRepository accountDetailsRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

        @Value("${iib.uat.url.accountDetails}")
        private String iibURL;

        @Value("${iib.uat.url.customerAccountSummary}")
        private String customerAccountDetailsURL;

        @Value("${user.token.checksumKey}")
        private String tokenKey;

        @Transactional
        public AccountDetailsResponse getAccountDetails(AccountRetrievalRequest accountRetrievalRequest){
            UserDetails  userDetailsRes =null;
            ObjectMapper objectMapper=new ObjectMapper();
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            String requestString=null;
            String responseString=null;
            ADBServiceResponse adbServiceResponse=null;
            AccountDtlsResp accountDtlsResp=null;
            UserDetails userDetails =null;
            AccountDetailsResponse accountDetailsResponse=null;
            AccountDetailsRequest accountDetailsRequest=null;
            Collection<com.abipb.linkaccount.domain.AccountDetails> userAccountDetailsList=new ArrayList<>();
            try {

                String transactionId=Constants.TRANSACTION_ID_PREFIX.getConstant();
                transactionId=transactionId+trackingDetailsRepository.count();
                String cifId=accountRetrievalRequest.getCustomerNumber();
                String devId=accountRetrievalRequest.getDeviceId();
                accountDetailsRequest= RequestsGenerator.getAccountDetailsRequest(transactionId,cifId,devId);
//                requestString = objectMapper.writeValueAsString(accountDetailsRequest);
                //com.abipb.linkaccount.payload.accountDetails.request.ADBServiceRequest adbServiceRequest= RequestsGenerator.getAccountDetailsRequestGenerator(accountDetailsRequest,transactionId);
                String  accountDetailsStringObj = objectMapper.writeValueAsString(accountDetailsRequest);
                responseString=HTTPPostClient.callHttpPost(iibURL,null,null,accountDetailsStringObj);

                 accountDetailsResponse=new AccountDetailsResponse();
                accountDetailsResponse=objectMapper.readValue(responseString,AccountDetailsResponse.class);
                ArrayList<AccountDetails> accountDetailsList=(ArrayList<AccountDetails>)accountDetailsResponse.getAdbServiceResponse().getResponseData().getAccountDetails();

                accountDtlsResp=new AccountDtlsResp();
                accountDtlsResp.setAccountNo(accountDetailsList.get(0).getAccountNumber());
                accountDtlsResp.setAccountType(accountDetailsList.get(0).getAccountType());
                accountDtlsResp.setBranchId(accountDetailsList.get(0).getBranchId());
                accountDtlsResp.setCustomerId(accountDetailsResponse.getAdbServiceResponse().getResponseData().getCustomerId());
                accountDtlsResp.setIfscCode(accountDetailsList.get(0).getIfscCode());

                userDetails =new UserDetails();
                userDetails.setCustomerId(accountDetailsResponse.getAdbServiceResponse().getResponseData().getCustomerId());
                userDetails.setDob(accountDetailsResponse.getAdbServiceResponse().getResponseData().getDateOfBirth());
                userDetails.setName(accountDetailsResponse.getAdbServiceResponse().getResponseData().getCustomerName());
                userDetails.setPanNo(accountDetailsResponse.getAdbServiceResponse().getResponseData().getPanNumber());

                for (AccountDetails accountDetails:accountDetailsList) {
                    com.abipb.linkaccount.domain.AccountDetails accountDetailsSavings=new com.abipb.linkaccount.domain.AccountDetails();
                    accountDetailsSavings.setAccountNo(accountDetails.getAccountNumber());
                    accountDetailsSavings.setAccountType(accountDetails.getAccountType());
                    accountDetailsSavings.setAccountStatus(accountDetails.getAccountStatus());
                    accountDetailsSavings.setBranchId(accountDetails.getBranchId());
                    accountDetailsSavings.setBranchName(accountDetails.getBranchName());
                    accountDetailsSavings.setCardNo(accountDetails.getCardNumber());
                    accountDetailsSavings.setIfscCode(accountDetails.getIfscCode());
                    userAccountDetailsList.add(accountDetailsSavings);
                }
                String tokenValue=HMAC.hmacSHA256(userDetails.toString(),transactionId);

                userDetails.setAccountDetails(userAccountDetailsList);
                userDetails.setToken(tokenValue);


               userDetailsRes =userDetailsRepository.save(userDetails);

                TrackingDetails trackingDetails=new TrackingDetails();
                trackingDetails.setRequestTrackingId(transactionId);
                trackingDetails.setResponseTrackingId(accountDetailsResponse.getAdbServiceResponse().getaDBMsgHdr().getMsgIdentifyGrp().getApplicationTranId());
                trackingDetails.setStatusCode(accountDetailsResponse.getAdbServiceResponse().getStatus().getReasonCode());
                trackingDetails.setStatusDesc(accountDetailsResponse.getAdbServiceResponse().getStatus().getReasonDesc());
                trackingDetails.setServiceName("Retreive Dashboard Details");
                trackingDetailsRepository.save(trackingDetails);

                /*adbServiceResponse=new ADBServiceResponse();
                adbServiceResponse.setResponseData(accountDtlsResp);
                adbServiceResponse.setAdbMessageHeader(new ADBMessageHeader());
                adbServiceResponse.setStatusInfo(new StatusInfo());
                adbServiceResponse.setError(new Error());*/

            }catch (Exception e){
                System.err.print(e.getMessage());
                logger.error(e.getLocalizedMessage());
            }
            if(accountDetailsResponse!=null){
                return accountDetailsResponse;
            }
           return accountDetailsResponse;

        }

    @Override
    @Transactional
    public CustomerAccountSummaryResponse getCustomerSummaryData(CustomerAccountSummaryRequestApp customerAccountSummaryRequest) {
        ObjectMapper objectMapper=new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        String requestString=null;
        String responseString=null;
        CustomerAccountSummaryRequest customerAccountSummaryRequestJson=null;
        CustomerAccountSummaryResponse customerAccountSummaryResponse=null;
        UserDetails userDetails=null;
        ArrayList<com.abipb.linkaccount.domain.AccountDetails> custsummaryArrayList=new ArrayList<>();

        try{
            String transactionId=Constants.TRANSACTION_ID_PREFIX.getConstant();
            transactionId=transactionId+trackingDetailsRepository.count();
            String customerNumber=customerAccountSummaryRequest.getCustomerNumber();
            String accountrole=customerAccountSummaryRequest.getAccountrole();
            customerAccountSummaryRequestJson= RequestsGenerator.getCustomerAccountSummaryRequest(transactionId,customerNumber,accountrole);
            String  requestStringObj= objectMapper.writeValueAsString(customerAccountSummaryRequestJson);
            responseString=HTTPPostClient.callHttpPost(customerAccountDetailsURL,null,null,requestStringObj);

            customerAccountSummaryResponse=objectMapper.readValue(responseString,CustomerAccountSummaryResponse.class);
            ArrayList<Custsummary> customerDetailsList=(ArrayList)customerAccountSummaryResponse.getAdbServiceResponse().getResponseData().getCustsummary();
//            TypeFactory typeFactory=objectMapper.getTypeFactory();
//            List<Custsummary> someClassList = objectMapper.readValue(customerDetailsList.toString(), typeFactory.constructCollectionType(List.class, Custsummary.class));
//            List CustSummary =objectMapper.readValue(customerDetailsList.toString(), new TypeReference<List<Custsummary>>() { });
            userDetails=new UserDetails();
            userDetails.setDob(customerAccountSummaryResponse.getAdbServiceResponse().getResponseData().getDateOfBirth());
            userDetails.setPanNo(customerAccountSummaryResponse.getAdbServiceResponse().getResponseData().getPanNumber());
            userDetails.setName(customerAccountSummaryResponse.getAdbServiceResponse().getResponseData().getCustomerName());
            userDetails.setMobileNumber(customerAccountSummaryResponse.getAdbServiceResponse().getResponseData().getMobileNumber());
            userDetails.setCustomerId(customerAccountSummaryResponse.getAdbServiceResponse().getResponseData().getCustomerId());
            String tokenValue=HMAC.hmacSHA256(userDetails.toString(),transactionId);
            userDetails.setToken(tokenValue);
            userDetails.setLoginDate(new Date());
            for (Custsummary custDetails:customerDetailsList) {
                com.abipb.linkaccount.domain.AccountDetails accountDetails=new com.abipb.linkaccount.domain.AccountDetails();
                accountDetails.setAccountNo(custDetails.getAccountNumber());
                accountDetails.setAccountType(custDetails.getAccountType());
                accountDetails.setBranchId(custDetails.getBranchId());
                accountDetails.setBranchName(custDetails.getBranchName());
                accountDetails.setIfscCode(custDetails.getIfscCode());
                //accountDetails.setCustomerId(customerAccountSummaryResponse.getAdbServiceResponse().getResponseData().getCustomerId());
                //accountDetails.setUserDetails(userDetails);
                custsummaryArrayList.add(accountDetails);
            }



            userDetails.setAccountDetails(custsummaryArrayList);


            userDetailsRepository.save(userDetails);


            TrackingDetails trackingDetails=new TrackingDetails();
            trackingDetails.setRequestTrackingId(transactionId);
            trackingDetails.setResponseTrackingId(customerAccountSummaryResponse.getAdbServiceResponse().getaDBMsgHdr().getMsgIdentifyGrp().getApplicationTranId());
            trackingDetails.setStatusCode(customerAccountSummaryResponse.getAdbServiceResponse().getStatus().getReasonCode());
            trackingDetails.setStatusDesc(customerAccountSummaryResponse.getAdbServiceResponse().getStatus().getReasonDesc());
            trackingDetails.setServiceName("Retreive Customer Account Summary Details");
            trackingDetailsRepository.save(trackingDetails);

        }catch (Exception e){
            logger.error(e.getLocalizedMessage());
            System.out.print(e.getStackTrace().length);
            StackTraceElement stackTraceElement=e.getStackTrace()[1];
            logger.error(String.valueOf(stackTraceElement));
        }

        return customerAccountSummaryResponse;
    }

    @Transactional
    public Accounts savePreferAccId(String accountId) {
        com.abipb.linkaccount.domain.AccountDetails accountDetails= accountDetailsRepository.findByAccountNo(accountId);
        UserDetails userDetails=userDetailsRepository.findByAccountDetails(accountDetails);
        userDetails.setPeferAaccId(accountId);
        userDetails.setLoginDate(new Date());
        userDetailsRepository.save(userDetails);
            String accountType= accountDetails.getAccountType();
        Accounts accounts=new Accounts();
        accounts.setAccountNumber(userDetails.getPeferAaccId());
        accounts.setAccountType(accountDetails.getAccountType());
        return accounts;
    }


}
