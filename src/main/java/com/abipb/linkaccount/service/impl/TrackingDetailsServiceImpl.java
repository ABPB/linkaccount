package com.abipb.linkaccount.service.impl;

import com.abipb.linkaccount.domain.DATA;
import com.abipb.linkaccount.domain.TrackingDetails;
import com.abipb.linkaccount.payload.billdesk.response.BillDeskLoadMoneyResponse;
import com.abipb.linkaccount.repository.BillDeskLoadMoneyResponseRepository;
import com.abipb.linkaccount.repository.TrackingDetailsRepository;
import com.abipb.linkaccount.service.TrackingDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * Created by Rajasekhar.E-V on 08-11-2018.
 */
@Service
public class TrackingDetailsServiceImpl implements TrackingDetailsService {
    @Autowired
    TrackingDetailsRepository trackingDetailsRepository;

    @Autowired
    BillDeskLoadMoneyResponseRepository billDeskLoadMoneyResponseRepository;

    @Override
    public TrackingDetails getTrackingDetails(String trackingId) {

        TrackingDetails trackingDetails = trackingDetailsRepository.findOne(trackingId);
        return trackingDetails;
    }

    @Override
    public DATA getBilldeskTransactionDetails(String trackingId) {
      DATA billDeskLoadMoneyResponse= billDeskLoadMoneyResponseRepository.findOne(trackingId);
        return billDeskLoadMoneyResponse;
    }
}
