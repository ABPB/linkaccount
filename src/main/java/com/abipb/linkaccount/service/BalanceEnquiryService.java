package com.abipb.linkaccount.service;

import com.abipb.linkaccount.common.payload.ADBServiceResponse;
import com.abipb.linkaccount.payload.BalanceEnqryResp;
import com.abipb.linkaccount.payload.balanceenquiry.BalanceEnquiryRequest;

/**
 * Created by pardhasaradhi.k on 10/12/2018.
 */
public interface BalanceEnquiryService {
    public BalanceEnqryResp getBalance(BalanceEnquiryRequest balanceEnquiryRequest);
}
