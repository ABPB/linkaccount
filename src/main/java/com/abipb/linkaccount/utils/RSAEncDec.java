package com.abipb.linkaccount.utils;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.Security;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.regex.Pattern;

import javax.crypto.Cipher;

import org.apache.log4j.Logger;

import com.infosys.feba.framework.security.Base64EncoderDecoder;
import org.slf4j.LoggerFactory;

/**
 * 
 */

/**
 * @author Anandakannan_S
 *
 */
public class RSAEncDec {
	private static final org.slf4j.Logger log= LoggerFactory.getLogger(RSAEncDec.class);
	private static final Pattern SPLIT_BY_WHITESPACE = Pattern.compile("\\s");
	/**
	 * @param args
	 */
	

	static
	{
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
	}

	
	/**
	 * @param bytes
	 * @return
	 */
	public static String byteArrayToHexString(byte[] bytes) {
		StringBuilder result = new StringBuilder();
		for (byte aByte : bytes) {
			result.append(Integer.toString((aByte & 0xff) + 0x100, 16)
					.substring(1));
		}
		return result.toString();
	}
	
	/**
	 * @param data
	 * @return
	 */
	public static byte[] hexStringToByteArray(String data) {
		int k = 0;
		byte[] results = new byte[data.length() / 2];
		for (int i = 0; i < data.length();) {
			results[k] = (byte) (Character.digit(data.charAt(i++), 16) << 4);
			results[k] += (byte) (Character.digit(data.charAt(i++), 16));
			k++;
		}
		
		return results;
	}

	public String getRSAEncrtpytion(String str) {
	    String B64EncryptedStr="";
		try {
		   if(str!=null&& !str.equalsIgnoreCase("")) {
			byte input[] = str.getBytes();
			Cipher cipher = Cipher.getInstance("RSA/ECB/NoPadding", "SunJCE");
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			
			RSAPublicKeySpec pubKeySpec = new RSAPublicKeySpec(new BigInteger("6881460287332454865023149546899256679107032015672448367270145189896448112417279517405189063011206242237047387812121407111058828252849912994668048817123507"), new BigInteger("65537"));
			RSAPublicKey pubKey = (RSAPublicKey) keyFactory.generatePublic(pubKeySpec);

			cipher.init(Cipher.ENCRYPT_MODE, pubKey);
			byte[] cipherText = cipher.doFinal(input);
			 B64EncryptedStr = Base64EncoderDecoder.encode(cipherText);
			//String cipherHexString = byteArrayToHexString(cipherText);
			
			//System.out.println(cipherHexString);
			System.out.println("*******************************");
			System.out.println("cipher: " + B64EncryptedStr);
			System.out.println("*******************************");	
		   }else {
		       log.info("Empty String");
		   }
		
		}catch(GeneralSecurityException generalSecurityException){
			generalSecurityException.printStackTrace();
		}
		return B64EncryptedStr; 

	}

}
