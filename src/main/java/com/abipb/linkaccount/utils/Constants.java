package com.abipb.linkaccount.utils;

/**
 * Created by pardhasaradhi.k on 11/1/2018.
 */
public enum Constants {
    TRANSACTION_ID_PREFIX("ABIPBL");

    Constants(String constant) {
        this.constant = constant;
    }
    private final String constant;

    public String getConstant() {
        return constant;
    }
}
