package com.abipb.linkaccount.utils;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

/**
 * Created by penchalaiah.g-v on 13-12-2017.
 */

@Component
public class JAXBUtil {
    private static Logger log= LoggerFactory.getLogger(JAXBUtil.class);
    public static Object convertXmlToObject(String xsdFilePath,String xmlFilePath,String category){
        return  null;
    }
    public static void convertObjectToXml(String xsdFilePath,String xmlFilePath,String category){

    }

    public static File convertMultipartFiletoStandardFile(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }


    public  void  marshal(String packageName,String fileName,Object object){
        try {
            JAXBContext jaxbContext=JAXBContext.newInstance(packageName);
            Marshaller marshaller=jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(object,new File(fileName));
        }catch (JAXBException e){
            log.error(e.getMessage(),e);
        }
    }


    public  Object  unmarshal(Class cls, URL xsdFile,String fileName)throws Exception{
        Object obj=null;
        try {
            JAXBContext jaxbContext=JAXBContext.newInstance(cls);
            Schema schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(xsdFile);
            Unmarshaller  unmarshaller=jaxbContext.createUnmarshaller();
            unmarshaller.setSchema(schema);
            obj=unmarshaller.unmarshal(new File(fileName));
        }catch (JAXBException e){
            log.error(e.getMessage(),e);
            throw new Exception(e);
        }catch (SAXException e){
            log.error(e.getMessage(),e);
            throw new Exception(e);
        }
        return obj;
    }


    public static Object  unmarshal1(Class cls, String fileName)throws Exception{
        Object obj=null;
        try {
            JAXBContext jaxbContext=JAXBContext.newInstance(cls);

            Unmarshaller  unmarshaller=jaxbContext.createUnmarshaller();

            obj=unmarshaller.unmarshal(new File(fileName));
        }catch (JAXBException e){
            log.error(e.getMessage(),e);
            throw new Exception(e);
        }
        return obj;
    }
    public static Object  unmarshal(Class cls, String fileName)throws Exception{
        Object obj=null;
        try {
            JAXBContext jaxbContext=JAXBContext.newInstance(cls);

            Unmarshaller  unmarshaller=jaxbContext.createUnmarshaller();

            obj=unmarshaller.unmarshal(new File(fileName));
        }catch (JAXBException e){
            log.error(e.getMessage(),e);
            throw new Exception(e);
        }
        return obj;
    }
    public static Object  unmarshal(Class cls, File file)throws Exception{
        Object obj=null;
        try {
            JAXBContext jaxbContext=JAXBContext.newInstance(cls);
            Unmarshaller  unmarshaller=jaxbContext.createUnmarshaller();
            obj=unmarshaller.unmarshal(file);
        }catch (JAXBException e){
            log.error(e.getMessage(),e);
            throw new Exception(e);
        }
        return obj;
    }



}




