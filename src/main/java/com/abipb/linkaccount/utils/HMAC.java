package com.abipb.linkaccount.utils;

import java.io.File;
import java.security.MessageDigest;
import java.util.Scanner;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;
public class HMAC {

    private static Logger log = Logger.getLogger("HMAC");

    // For generating the CheckSum
    public static String hmacSHA256(String message, String secret) {
	log.debug("Entering");
	MessageDigest md = null;
	try {

	    Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
	    SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(),
		    "HmacSHA256");
	    sha256_HMAC.init(secret_key);

	    byte raw[] = sha256_HMAC.doFinal(message.getBytes());
	    
	    return hex(raw);
	    
	} catch (Exception e) {
	    log.debug("Exception!!!!! " + e);
	    return null;
	}
    }

    
    public static String hex(byte[] input) throws Exception {
	final char[] HEX_TABLE = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
		'9', 'A', 'B', 'C', 'D', 'E', 'F' };
	StringBuffer sb = new StringBuffer(input.length * 2);
	for (int i = 0; i < input.length; i++) {
	    sb.append(HEX_TABLE[(input[i] >> 4 & 0xF)]);
	    sb.append(HEX_TABLE[(input[i] & 0xF)]);
	}
	return sb.toString();
    }

    
   

    public static void main(String[] args) {

	try {
	    
	    String ChecksumKey = "IDEASMALLKEY";

	    System.out
		    .println("CA853F5C77AFE5B18A2B1B1DD9469D2BF546CF08E3A12513DBC48CA551C044C9"
			    .equals("CA853F5C77AFE5B18A2B1B1DD9469D2BF546CF08E3A12513DBC48CA551C044C9"));
	    
	    System.out
		    .println("HmacSHA256="
			    + hmacSHA256(
				    "IS_IDEA~Y|PRN~589537|TXN_AMT~1|TRAN_CRN~INR|PID~IDEA_MER",
				    ChecksumKey));
	    System.out
		    .println("2.HmacSHA256="
			    + hmacSHA256(
				    "IS_IDEA~Y|PRN~589537|TXN_AMT~1|TRAN_CRN~INR|PID~IDEA_MER",
				    ChecksumKey));
	    
	    
	    String key = "7ee0f180-8a1b-11e6-90ca-e5eaebac9f97";
	    Scanner s = new Scanner(new File("C:\\Users\\Neeraj\\Desktop\\pwReq.txt")).useDelimiter("//A");
	    String req = s.hasNext() ? s.next() : "";
	    System.out.println(req);
	    System.out.println("HmacSHA256=" + hmacSHA256(req, key));
   
	    System.out
	    .println("A9E32C4B5C62560BF1EF385EB2FA32FECC1FB56F59BAF351A9AE23ADBB09A9E9"
		    .equals( hmacSHA256(req, key)));
	    
	    
	    String shopReq = "TRSTATUS~true|TRANSACTIONID~29376|PRN~42342456|TXN_AMT~3.79|Filler1~filler1|Filler2~filler2|Filler3~filler3|Filler4~filler4|Filler5~filler5";
	    String shopKey = "IDEASMALLKEY";
	    String shopHash = hmacSHA256(shopReq,shopKey);
	    System.out
		    .println("7f29c7bebf1463fca87a8c22d5f7c017c095ad2f03ac84da206527b7379f17e0"
			    .equals(shopHash));
	    
	    System.out.println(shopHash);

	} catch (Exception e) {
	    // e.printStackTrace();
	}

    }

}