package com.abipb.linkaccount.utils;

import com.abipb.linkaccount.payload.CustomerAccountSummary.request.*;
import com.abipb.linkaccount.payload.accountDetails.AccountRetrievalRequest;
import com.abipb.linkaccount.payload.accountDetails.request.*;

import com.abipb.linkaccount.payload.balanceenquiry.request.*;


import com.abipb.linkaccount.payload.fundtransfer.FundsTransferRequest;
import com.abipb.linkaccount.payload.fundtransfer.request.*;

import com.abipb.linkaccount.payload.fundtransfer.request.StaffId;
import com.abipb.linkaccount.payload.loadmoney.request.*;
import com.abipb.linkaccount.payload.loginmpin.LogInRequest;
import org.joda.time.DateTime;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by pardhasaradhi.k on 10/31/2018.ó
 */
public class RequestsGenerator {


    public static String getLoadMoneyRequestGenerator(String args[]) throws Exception {

        AgentmPin agentmPin=new AgentmPin();
        AgentMSISDN agentMSISDN=new AgentMSISDN();
        CommonServiceData commonServiceData=new CommonServiceData();
        CustomermPin customermPin=new CustomermPin();
        EntityId entityId=new EntityId();
        FundTransferDetails fundTransferDetails=new FundTransferDetails();
        LoadMoneyRequest loadMoneyRequest=new LoadMoneyRequest();
        LoadOrWithdrawMoney loadOrWithdrawMoney=new LoadOrWithdrawMoney();

        commonServiceData.setChannelID("17");
        commonServiceData.setEntityId(entityId);
        commonServiceData.setEntityTypeId("80");
        commonServiceData.setMSISDN("");
        commonServiceData.setRequestId("");

        fundTransferDetails.setAgentmPin(agentmPin);
        fundTransferDetails.setAgentMSISDN(agentMSISDN);
        fundTransferDetails.setAmount("");
        fundTransferDetails.setCustomermPin(customermPin);
        fundTransferDetails.setCustomerMSISDN("");
        fundTransferDetails.setEnterpriseId("");
        fundTransferDetails.setIdeamoneyTransactionID("");
        fundTransferDetails.setInstance("020");
        fundTransferDetails.setPaymentInstrument("02");
        fundTransferDetails.setTransactionType("519");

        loadOrWithdrawMoney.setCommonServiceData(commonServiceData);
        loadOrWithdrawMoney.setFundTransferDetails(fundTransferDetails);

        loadMoneyRequest.setLoadOrWithdrawMoney(loadOrWithdrawMoney);

        GregorianCalendar gcal = new GregorianCalendar();
        XMLGregorianCalendar xgcal = DatatypeFactory.newInstance()
                .newXMLGregorianCalendar(gcal);
        XMLGregorianCalendar xgc = DatatypeFactory.newInstance().newXMLGregorianCalendar(new DateTime().toGregorianCalendar());
        String timeStamp=String.valueOf(xgc).split("\\.")[0];
        System.out.println(timeStamp);
        return timeStamp;
    }
    public static String getOchLogInMpinRequest(LogInRequest ochLogInMpinRequest){
        String inputXML=null;
        String mPin=ochLogInMpinRequest.getAccessCode();
        String userPrinciple=ochLogInMpinRequest.getUserPrinciple();
        String  ipAddress=ochLogInMpinRequest.getIpAddress();
        if(!"".equalsIgnoreCase(mPin)&&mPin!=null&&!"".equalsIgnoreCase(userPrinciple)&&userPrinciple!=null) {
           // String rsaEncryptedOptionCode = new RSAEncDec().getRSAEncrtpytion(mPin);

            if (/*!"".equalsIgnoreCase(rsaEncryptedOptionCode)&&rsaEncryptedOptionCode!=null&&*/!"".equalsIgnoreCase(ipAddress)&&ipAddress!=null) {
                inputXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                        "<LoginMPINRequest xmlns=\"http://www.infosys.com/request/LoginMPIN\" xmlns:hd=\"http://www.infosys.com/request/header\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
                        "   <hd:header>\n" +
                        "      <hd:BANK_ID>ABP</hd:BANK_ID>\n" +
                        "      <hd:LANGUAGE_ID>001</hd:LANGUAGE_ID>\n" +
                        "      <hd:CHANNEL_ID>G</hd:CHANNEL_ID>\n" +
                        "      <hd:LOGIN_FLAG>1</hd:LOGIN_FLAG>\n" +
                        "      <hd:__SRVCID__>LGNMPN</hd:__SRVCID__>\n" +
                        "      <hd:OPFMT>XML</hd:OPFMT>\n" +
                        "      <hd:IPFMT>XML</hd:IPFMT>\n" +
                        "      <hd:USER_PRINCIPAL>"+userPrinciple+"</hd:USER_PRINCIPAL>\n" +
                        "      <hd:CORP_PRINCIPAL>"+userPrinciple+"</hd:CORP_PRINCIPAL>\n" +
                        "      <hd:ACCESS_CODE>\n" +
                        "         <EncryptedData xmlns=\"http://www.w3.org/2001/04/xmlenc#\" Type=\"http://www.w3.org/2001/04/xmlenc#Content\">\n" +
                        "            <CipherData>\n" +
                        "               <CipherValue>"+mPin+"</CipherValue>\n" +
                        "            </CipherData>\n" +
                        "         </EncryptedData>\n" +
                        "      </hd:ACCESS_CODE>\n" +
                        "      <hd:STATEMODE>Y</hd:STATEMODE>\n" +
                        "      <hd:FORM_ID />\n" +
                        "      <hd:IP_ADDRESS>"+ipAddress+"</hd:IP_ADDRESS>\n" +
                        "      <hd:ISMULTIREC />\n" +
                        "      <hd:LOGIN_TYPE />\n" +
                        "      <hd:RELATIONSHIP_ID />\n" +
                        "      <hd:PORTAL_REQUEST />\n" +
                        "      <hd:PORTAL_RANDOM_ID />\n" +
                        "      <hd:USER_ID />\n" +
                        "      <hd:ISATTACHMENT />\n" +
                        "      <hd:DEVICE_ID />\n" +
                        "      <hd:DEVICE_TYPE />\n" +
                        "      <hd:MACHINE_FINGER_PRINT />\n" +
                        "      <hd:BROWSER_TYPE />\n" +
                        "      <hd:REQUESTED_FRAME_START_INDEX />\n" +
                        "      <hd:CATEGORY_ID>IDEACAT</hd:CATEGORY_ID>\n" +
                        "   </hd:header>\n" +
                        "</LoginMPINRequest>";
            }
        }
        return inputXML;
    }
     public static com.abipb.linkaccount.payload.accountDetails.request.ADBServiceRequest getAccountDetailsRequestGenerator(AccountRetrievalRequest accountRetrievalRequest, String transactionId) {
         com.abipb.linkaccount.payload.accountDetails.request.ADBServiceRequest adbServiceRequest = new com.abipb.linkaccount.payload.accountDetails.request.ADBServiceRequest();
         AccountDetailsRequest accountDetailsRequest = new AccountDetailsRequest();
         com.abipb.linkaccount.payload.accountDetails.request.ADBMsgHdr adbMsgHdr = new com.abipb.linkaccount.payload.accountDetails.request.ADBMsgHdr();
         com.abipb.linkaccount.payload.accountDetails.request.MsgAttrGrp msgAttrGrp = new com.abipb.linkaccount.payload.accountDetails.request.MsgAttrGrp();
         com.abipb.linkaccount.payload.accountDetails.request.MsgIdentifyGrp msgIdentifyGrp = new com.abipb.linkaccount.payload.accountDetails.request.MsgIdentifyGrp();
         com.abipb.linkaccount.payload.accountDetails.request.RequestData requestData = new com.abipb.linkaccount.payload.accountDetails.request.RequestData();
         com.abipb.linkaccount.payload.accountDetails.request.SourceChnlInfoGrp sourceChnlInfoGrp = new com.abipb.linkaccount.payload.accountDetails.request.SourceChnlInfoGrp();
         com.abipb.linkaccount.payload.accountDetails.request.UserAttrGrp userAttrGrp = new com.abipb.linkaccount.payload.accountDetails.request.UserAttrGrp();

         msgAttrGrp.setServiceName("reteriveDashboard");
         msgAttrGrp.setServiceVersion("1.0");

         msgIdentifyGrp.setApplicationTranId(transactionId);
         msgIdentifyGrp.setRqstDateTime("");

         requestData.setCustomerNumber(accountRetrievalRequest.getCustomerNumber());

         sourceChnlInfoGrp.setSourceAppl("");
         sourceChnlInfoGrp.setSourceChnl("OLB");

         userAttrGrp.setChannelId("");
         userAttrGrp.setDeviceId(accountRetrievalRequest.getDeviceId());

         adbMsgHdr.setMsgAttrGrp(msgAttrGrp);
         adbMsgHdr.setMsgIdentifyGrp(msgIdentifyGrp);
         adbMsgHdr.setSourceChnlInfoGrp(sourceChnlInfoGrp);
         adbMsgHdr.setUserAttrGrp(userAttrGrp);

         adbServiceRequest.setADBMsgHdr(adbMsgHdr);
         adbServiceRequest.setRequestData(requestData);

         accountDetailsRequest.setAdbServiceRequest(adbServiceRequest);

         return adbServiceRequest;
   }


   public static BalanceEnquiryRequest getBalanceEnquiryRequestGenerator(com.abipb.linkaccount.payload.balanceenquiry.BalanceEnquiryRequest balanceEnquiryRequestApp,String appTranId){
       com.abipb.linkaccount.payload.balanceenquiry.request.ADBServiceRequest adbServiceRequest=new com.abipb.linkaccount.payload.balanceenquiry.request.ADBServiceRequest();
       AccountInfo accountInfo=new AccountInfo();
       com.abipb.linkaccount.payload.balanceenquiry.request.ADBMsgHdr adbMsgHdr=new com.abipb.linkaccount.payload.balanceenquiry.request.ADBMsgHdr();
       BalanceEnquiryRequest balanceEnquiryRequest=new BalanceEnquiryRequest();
       com.abipb.linkaccount.payload.balanceenquiry.request.MsgAttrGrp msgAttrGrp=new com.abipb.linkaccount.payload.balanceenquiry.request.MsgAttrGrp();
       com.abipb.linkaccount.payload.balanceenquiry.request.MsgIdentifyGrp msgIdentifyGrp=new com.abipb.linkaccount.payload.balanceenquiry.request.MsgIdentifyGrp();
       com.abipb.linkaccount.payload.balanceenquiry.request.RequestData requestData=new com.abipb.linkaccount.payload.balanceenquiry.request.RequestData();
       com.abipb.linkaccount.payload.balanceenquiry.request.SourceChnlInfoGrp sourceChnlInfoGrp=new com.abipb.linkaccount.payload.balanceenquiry.request.SourceChnlInfoGrp();
       com.abipb.linkaccount.payload.balanceenquiry.request.UserAttrGrp userAttrGrp=new com.abipb.linkaccount.payload.balanceenquiry.request.UserAttrGrp();

       List<String> accountIds=new ArrayList<>();
//       accountIds.add(balanceEnquiryRequestApp.getSavingsAccountId());
//       accountIds.add(balanceEnquiryRequestApp.getWalletAccountId());
       accountIds.add(balanceEnquiryRequestApp.getAccountId());
       accountInfo.setAccountId(accountIds);
       accountInfo.setBalanceType("EFFAVL");

       requestData.setAccountInfo(accountInfo);

       sourceChnlInfoGrp.setSourceAppl("");
       sourceChnlInfoGrp.setSourceChnl("OLB ");

       msgIdentifyGrp.setApplicationTranId(appTranId);
       msgIdentifyGrp.setRqstDateTime("");
       msgIdentifyGrp.setSourceChnlInfoGrp(sourceChnlInfoGrp);

       msgAttrGrp.setServiceName("BalanceEnquiry");
       msgAttrGrp.setServiceVersion("1.0");

       userAttrGrp.setChannelId("");
       userAttrGrp.setDeviceId("");
       userAttrGrp.setStaffId("");

       adbMsgHdr.setMsgAttrGrp(msgAttrGrp);
       adbMsgHdr.setMsgIdentifyGrp(msgIdentifyGrp);
       adbMsgHdr.setUserAttrGrp(userAttrGrp);

       adbServiceRequest.setADBMsgHdr(adbMsgHdr);
       adbServiceRequest.setRequestData(requestData);

       balanceEnquiryRequest.setADBServiceRequest(adbServiceRequest);


       return balanceEnquiryRequest;
   }

    public static FundTransferRequest getFundTransferRequest(FundsTransferRequest fundsTransferRequest,String transactionId){
        AadharNumber aadharNumber=new AadharNumber();
        com.abipb.linkaccount.payload.fundtransfer.request.ADBMsgHdr adbMsgHdr=new com.abipb.linkaccount.payload.fundtransfer.request.ADBMsgHdr();
        com.abipb.linkaccount.payload.fundtransfer.request.ADBServiceRequest adbServiceRequest=new com.abipb.linkaccount.payload.fundtransfer.request.ADBServiceRequest();
        AddressInfo addressInfo=new AddressInfo();
        AddressType addressType=new AddressType();
        AddrLine1 addrLine1 =new AddrLine1();
        AddrLine2 addrLine2=new AddrLine2();
        AddrLine3 addrLine3=new AddrLine3();
        BankDetails bankDetails=new BankDetails();
        ChargeAccountId chargeAccountId=new ChargeAccountId();
        City city=new City();
        Country country=new Country();
        CreditorInfo creditorInfo=new CreditorInfo();
        DebitorInfo debitorInfo=new DebitorInfo();
        FundTransferRequest fundTransferRequest=new FundTransferRequest();
        com.abipb.linkaccount.payload.fundtransfer.request.MsgAttrGrp msgAttrGrp=new com.abipb.linkaccount.payload.fundtransfer.request.MsgAttrGrp();
        com.abipb.linkaccount.payload.fundtransfer.request.MsgIdentifyGrp msgIdentifyGrp=new com.abipb.linkaccount.payload.fundtransfer.request.MsgIdentifyGrp();
        PostCode postCode =new PostCode();
        ProcessingCode processingCode=new ProcessingCode();
        RemitInfo remitInfo=new RemitInfo();
        RemitReasonCode remitReasonCode=new RemitReasonCode();
        RemitReferenceNo remitReferenceNo=new RemitReferenceNo();
        com.abipb.linkaccount.payload.fundtransfer.request.RequestData requestData=new com.abipb.linkaccount.payload.fundtransfer.request.RequestData();
        RetailerInfo retailerInfo=new RetailerInfo();
        com.abipb.linkaccount.payload.fundtransfer.request.SourceChnlInfoGrp sourceChnlInfoGrp=new com.abipb.linkaccount.payload.fundtransfer.request.SourceChnlInfoGrp();
        StaffId staffId=new StaffId();
        State state=new State();
        com.abipb.linkaccount.payload.fundtransfer.request.UserAttrGrp userAttrGrp=new com.abipb.linkaccount.payload.fundtransfer.request.UserAttrGrp();


        msgIdentifyGrp.setApplicationTranId(transactionId);
        msgAttrGrp.setServiceName("FundTransfer");
        msgAttrGrp.setServiceVersionNbr("1.0");

        msgIdentifyGrp.setMsgAttrGrp(msgAttrGrp);
        msgIdentifyGrp.setRqstDateTime(getTimeStamp());
        sourceChnlInfoGrp.setSourceAppl("MBY");
        sourceChnlInfoGrp.setSourceChnl("MBY");
        msgIdentifyGrp.setSourceChnlInfoGrp(sourceChnlInfoGrp);

        adbMsgHdr.setMsgIdentifyGrp(msgIdentifyGrp);
        userAttrGrp.setChannelId("MBY");
        userAttrGrp.setDeviceId("");
        userAttrGrp.setStaffId(staffId);
        adbMsgHdr.setUserAttrGrp(userAttrGrp);
        adbMsgHdr.setXmlns("");

        requestData.setChargeAccountId(chargeAccountId);

        creditorInfo.setAadharNumber(aadharNumber);
        addressInfo.setAddrLine1(addrLine1);
        addressInfo.setAddrLine2(addrLine2);
        addressInfo.setAddrLine3(addrLine3);
        addressInfo.setCity(city);
        addressInfo.setCountry(country);
        addressInfo.setPostCode(postCode);
        addressInfo.setState(state);

        creditorInfo.setAddressInfo(addressInfo);
        creditorInfo.setAddrTypeIndicator("B");
        creditorInfo.setCreditRemark("");
        creditorInfo.setMMID("");
        creditorInfo.setMobileNumber("");
        creditorInfo.setName("");
        creditorInfo.setTxnRemarks("");

        bankDetails.setAccountId("");

       /* addressInfo.setAddrLine1(addrLine1);
        addressInfo.setAddrLine2(addrLine2);
        addressInfo.setAddrLine3(addrLine3);
        addressInfo.setCity(city);
        addressInfo.setCountry(country);
        addressInfo.setPostCode(postCode);
        addressInfo.setState(state);*/

        bankDetails.setAddressInfo(addressInfo);

        bankDetails.setAddressType(addressType);
        bankDetails.setBankId("");
        bankDetails.setBankName("");
        bankDetails.setBranchId("");
        bankDetails.setIFSCCode("");
        creditorInfo.setBankDetails(bankDetails);
        requestData.setCreditorInfo(creditorInfo);

        debitorInfo.setAccountId("");
        requestData.setDebitorInfo(debitorInfo);

        requestData.setPaymentMode("");
        requestData.setPaymentProd("");
        requestData.setProcessingCode(processingCode);

        retailerInfo.setCSPIdentifier("");
        retailerInfo.setRetailerAccountNumber("");
        retailerInfo.setSrvcCtgIdentifier("");

        remitInfo.setRemitAmount("");
        remitInfo.setRemitCurrency("");
        remitInfo.setRemitReasonCode(remitReasonCode);
        remitInfo.setRemitReferenceNo(remitReferenceNo);
        remitInfo.setRemitRemark("");
        remitInfo.setRemitReversal("");

        requestData.setRemitInfo(remitInfo);
        requestData.setRetailerInfo(retailerInfo);
        requestData.setXmlns("");


        adbServiceRequest.setADBMsgHdr(adbMsgHdr);
        adbServiceRequest.setRequestData(requestData);
        adbServiceRequest.setXmlns("http://adb.co.ind/esb/common/Header");
        fundTransferRequest.setADBServiceRequest(adbServiceRequest);
        return fundTransferRequest;
   }
    public static String getTimeStamp(){
        GregorianCalendar gcal = new GregorianCalendar();
        XMLGregorianCalendar xgc=null;
        try {
            xgc = DatatypeFactory.newInstance().newXMLGregorianCalendar(new DateTime().toGregorianCalendar());
         } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
            }
        String timeStamp=String.valueOf(xgc).split("\\.")[0];
        return timeStamp;
    }

    public static AccountDetailsRequest getAccountDetailsRequest(String appTranId,String cifId,String devId){
        AccountDetailsRequest accountDetailsRequest=new AccountDetailsRequest();
        com.abipb.linkaccount.payload.accountDetails.request.ADBMsgHdr adbMsgHdr=new com.abipb.linkaccount.payload.accountDetails.request.ADBMsgHdr();
        com.abipb.linkaccount.payload.accountDetails.request.ADBServiceRequest adbServiceRequest=new com.abipb.linkaccount.payload.accountDetails.request.ADBServiceRequest();
        com.abipb.linkaccount.payload.accountDetails.request.MsgIdentifyGrp msgIdentifyGrp=new com.abipb.linkaccount.payload.accountDetails.request.MsgIdentifyGrp();
        com.abipb.linkaccount.payload.accountDetails.request.MsgAttrGrp msgAttrGrp=new com.abipb.linkaccount.payload.accountDetails.request.MsgAttrGrp();
        com.abipb.linkaccount.payload.accountDetails.request.RequestData requestData=new com.abipb.linkaccount.payload.accountDetails.request.RequestData();
        com.abipb.linkaccount.payload.accountDetails.request.SourceChnlInfoGrp sourceChnlInfoGrp=new com.abipb.linkaccount.payload.accountDetails.request.SourceChnlInfoGrp();
        com.abipb.linkaccount.payload.accountDetails.request.UserAttrGrp userAttrGrp=new com.abipb.linkaccount.payload.accountDetails.request.UserAttrGrp();

        accountDetailsRequest.setAdbServiceRequest(adbServiceRequest);
        msgAttrGrp.setServiceName("reteriveDashboard");
        msgAttrGrp.setServiceVersion("1.0");
        adbMsgHdr.setMsgAttrGrp(msgAttrGrp);
        msgIdentifyGrp.setApplicationTranId(appTranId);
        msgIdentifyGrp.setRqstDateTime(getTimeStamp());
        adbMsgHdr.setMsgIdentifyGrp(msgIdentifyGrp);
        sourceChnlInfoGrp.setSourceAppl("OLB");
        sourceChnlInfoGrp.setSourceChnl("FAD");
        adbMsgHdr.setSourceChnlInfoGrp(sourceChnlInfoGrp);
        userAttrGrp.setChannelId("OLB");
        userAttrGrp.setDeviceId(devId);
        adbMsgHdr.setUserAttrGrp(userAttrGrp);

        adbServiceRequest.setADBMsgHdr(adbMsgHdr);
        requestData.setCustomerNumber(cifId);
        adbServiceRequest.setRequestData(requestData);
        return accountDetailsRequest;
    }

    public static CustomerAccountSummaryRequest getCustomerAccountSummaryRequest(String transactionId, String customerNumber, String accountrole){
        CustomerAccountSummaryRequest customerAccountSummaryRequest=new CustomerAccountSummaryRequest();
        com.abipb.linkaccount.payload.CustomerAccountSummary.request.ADBServiceRequest adbServiceRequest=new com.abipb.linkaccount.payload.CustomerAccountSummary.request.ADBServiceRequest();
        com.abipb.linkaccount.payload.CustomerAccountSummary.request.ADBMsgHdr adbMsgHdr=new com.abipb.linkaccount.payload.CustomerAccountSummary.request.ADBMsgHdr();
        com.abipb.linkaccount.payload.CustomerAccountSummary.request.RequestData requestData=new com.abipb.linkaccount.payload.CustomerAccountSummary.request.RequestData();
        com.abipb.linkaccount.payload.CustomerAccountSummary.request.MsgIdentifyGrp msgIdentifyGrp=new com.abipb.linkaccount.payload.CustomerAccountSummary.request.MsgIdentifyGrp();
        com.abipb.linkaccount.payload.CustomerAccountSummary.request.SourceChnlInfoGrp sourceChnlInfoGrp=new com.abipb.linkaccount.payload.CustomerAccountSummary.request.SourceChnlInfoGrp();
        com.abipb.linkaccount.payload.CustomerAccountSummary.request.MsgAttrGrp msgAttrGrp=new com.abipb.linkaccount.payload.CustomerAccountSummary.request.MsgAttrGrp();
        com.abipb.linkaccount.payload.CustomerAccountSummary.request.UserAttrGrp userAttrGrp=new com.abipb.linkaccount.payload.CustomerAccountSummary.request.UserAttrGrp();

        sourceChnlInfoGrp.setSourceAppl("");
        sourceChnlInfoGrp.setSourceChnl("");

        msgAttrGrp.setServiceName("getCustomerSummary");
        msgAttrGrp.setServiceVersionNbr("1.0");

        com.abipb.linkaccount.payload.CustomerAccountSummary.request.StaffId staffId=new com.abipb.linkaccount.payload.CustomerAccountSummary.request.StaffId();
        userAttrGrp.setChannelId("");
        userAttrGrp.setDeviceId("");
        userAttrGrp.setStaffId(staffId);

        msgIdentifyGrp.setApplicationTranId(transactionId);
        msgIdentifyGrp.setMsgAttrGrp(msgAttrGrp);
        msgIdentifyGrp.setRqstDateTime("");
        msgIdentifyGrp.setSourceChnlInfoGrp(sourceChnlInfoGrp);

        requestData.setAccountrole(accountrole);
        requestData.setCustomerNumber(customerNumber);

        adbMsgHdr.setMsgIdentifyGrp(msgIdentifyGrp);
        adbMsgHdr.setUserAttrGrp(userAttrGrp);
        adbMsgHdr.setXmlns("http://adb.co.ind/esb/common/Header");


        adbServiceRequest.setXmlns("http://adb.co.ind/esb/service");
        adbServiceRequest.setADBMsgHdr(adbMsgHdr);
        adbServiceRequest.setRequestData(requestData);

        customerAccountSummaryRequest.setADBServiceRequest(adbServiceRequest);

        return customerAccountSummaryRequest;
    }
}
