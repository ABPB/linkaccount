package com.abipb.linkaccount.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by abhay.kumar on 9/24/2018.
 */
@Component
public class MPesaPropertyValue {
    private final static Logger LOGGER = LoggerFactory.getLogger(MPesaPropertyValue.class);
    private static Properties properties=new Properties();
    public static void loadProperties(String propPath) {
        try {
            InputStream inputStream=ClassLoader.class.getResourceAsStream(propPath);
            properties.load(inputStream);
        }catch(IOException fnfe) {
            LOGGER.error("IOExcepton :: "+fnfe);
        }
    }
    public static String getValue(String key) {
        return  properties.getProperty(key);
    }
}
