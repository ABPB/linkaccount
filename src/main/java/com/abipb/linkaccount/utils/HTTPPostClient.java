package com.abipb.linkaccount.utils;

import com.abipb.linkaccount.binding.och.response.LoginMPINResponse;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.bind.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by abhay.kumar-v on 10/23/2017.
 */


public class HTTPPostClient {

    private static final Logger log= LoggerFactory.getLogger(HTTPPostClient.class);
    public static String callHttpPost(String url, ConcurrentMap<String,String> headersMap,ConcurrentMap<String,String> params ,String jsonRequest){
        String strResponse="";
        HttpClient client = null;
        String protocol = null;


        try{
            if (url !=null && !"".equals(url))
            {
                protocol = url.split(":")[0];
            }
            else {
                throw new Exception("URL is null");
            }
            if ("HTTPS".equalsIgnoreCase(protocol)) {
                TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                }};

                HttpClientBuilder builder = HttpClientBuilder.create();
                SSLContext ctx = SSLContext.getInstance("TLS");
                ctx.init(null, trustAllCerts, null);
                SSLConnectionSocketFactory scsf = new SSLConnectionSocketFactory(ctx, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
                builder.setSSLSocketFactory(scsf);
                Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                        .register("https", scsf)
                        .build();

                HttpClientConnectionManager ccm = new BasicHttpClientConnectionManager(registry);

                builder.setConnectionManager(ccm);

                client = builder.build();

            } else {

                client =HttpClientBuilder.create().build();
            }

            HttpPost post=new HttpPost(url);

            // logic for param
            if(params!= null && !"".equals(params)) {
                List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
                Set<String> set = params.keySet();
                Iterator<String> iterator = set.iterator();
                while (iterator.hasNext()) {
                    String key = iterator.next();
                    urlParameters.add(new BasicNameValuePair(key, params.get(key)));
                }
                post.setEntity(new UrlEncodedFormEntity(urlParameters));
            }
            // logic for headers
            if(headersMap!= null && !"".equals(headersMap)) {
                Set<String> headerKeySet = headersMap.keySet();
                Iterator<String> headerKeyIterator = headerKeySet.iterator();
                while (headerKeyIterator.hasNext()){
                    String headerKey=headerKeyIterator.next();
                    post.setHeader(headerKey,headersMap.get(headerKey));
                }
            }

            // logic for binary data in json format
            if(jsonRequest != null && !"".equals(jsonRequest)){
                HttpEntity entity = new ByteArrayEntity(jsonRequest.getBytes(StandardCharsets.UTF_8));
//                post.setEntity(new StringEntity(jsonRequest, headersMap.get("Content-Type")));
                post.setEntity(new StringEntity(jsonRequest, ContentType.create("application/json")));
            }
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
                @Override
                public String handleResponse(
                        final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }
            };
            strResponse=client.execute(post,responseHandler);
        }catch (ClientProtocolException cpe){
            System.out.println(" ClientProtocolException  "+cpe);
        }catch (Exception e){
            System.out.println(" IOException  "+e);
        }
        return strResponse;

    }


    public static String callOchXmlHttpPost(String inputXML) {
        LoginMPINResponse loginMPINResponse = null;
        String body =null;
        InputStream in;
        StringEntity entity = new StringEntity(inputXML, ContentType.create(
                "text/xml", Consts.UTF_8));
        entity.setChunked(true);
        HttpPost httppost = new HttpPost(
                "http://uatfinchnl.abipbldc.com:9080/corp/XService;jsessionid=");
        httppost.addHeader("IPTYPE", "XML");

        httppost.setEntity(entity);

        HttpClient client = HttpClients.createDefault();

        try {
            HttpResponse response = client.execute(httppost);
            System.out.println(response.toString());
            in = response.getEntity().getContent();
            body = IOUtils.toString(in);

            //System.out.println(body);
        } catch (ClientProtocolException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }
        return body;
    }

}
