package com.abipb.linkaccount;
import com.abipb.linkaccount.utils.MPesaPropertyValue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@SpringBootApplication
@Component
public class LinkAccountMiddlewareApplication {
	public static void main(String[] args) {
		SpringApplication.run(LinkAccountMiddlewareApplication.class, args);
		MPesaPropertyValue.loadProperties("/properties/mpesaMessage.properties");
		MPesaPropertyValue.loadProperties("/application.properties");
	}
}        