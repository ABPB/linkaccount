package com.abipb.linkaccount.config;

import javax.ws.rs.ApplicationPath;

import com.abipb.linkaccount.exception.LinkAccountExceptionMapper;
import com.abipb.linkaccount.rest.resource.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("/linkaccount")
public class JerseyConfig extends ResourceConfig {
	public JerseyConfig() {
		register(LocateAgentResource.class);
		register(LinkAccountExceptionMapper.class);
		register(BalanceEnquiryResource.class);
		register(FundsTransferResource.class);
		register(LoadMoneyResource.class);
		register(AccountDetailsRetrievalResource.class);
		register(BillDeskLoadMoneyResource.class);
		register(OCHLoginResource.class);
		register(TrackingDetailsResouce.class);
	}
}
