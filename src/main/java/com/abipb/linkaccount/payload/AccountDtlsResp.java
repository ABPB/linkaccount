package com.abipb.linkaccount.payload;

import com.abipb.linkaccount.common.payload.ResponseData;

/**
 * Created by pardhasaradhi.k on 10/26/2018.
 */

public class AccountDtlsResp extends ResponseData {
    private String accountNo;
    private String accountType;
    private String ifscCode;
    private String customerId;
    private String branchId;


    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountDtlsResp that = (AccountDtlsResp) o;

        if (!accountNo.equals(that.accountNo)) return false;
        if (!accountType.equals(that.accountType)) return false;
        if (!ifscCode.equals(that.ifscCode)) return false;
        if (!branchId.equals(that.branchId)) return false;
        return customerId.equals(that.customerId);
    }

    @Override
    public int hashCode() {
        int result = accountNo.hashCode();
        result = 31 * result + accountType.hashCode();
        result = 31 * result + ifscCode.hashCode();
        result = 31 * result + branchId.hashCode();
        result = 31 * result + customerId.hashCode();
        return result;
    }
}
