package com.abipb.linkaccount.payload;

import com.abipb.linkaccount.common.payload.ResponseData;

/**
 * Created by pardhasaradhi.k on 10/16/2018.
 */
public class BalanceEnqryResp extends ResponseData {
    private String status;
    private String amount;
    private String currencyType;

    public BalanceEnqryResp(String status, String amount) {
        this.status = status;
        this.amount = amount;
    }

    public BalanceEnqryResp() {
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(String currencyType) {
        this.currencyType = currencyType;
    }
}
