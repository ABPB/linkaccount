package com.abipb.linkaccount.payload.billdesk.response;

import com.abipb.linkaccount.domain.DATA;

/**
 * Created by Rajasekhar.E-V on 26-10-2018.
 */

    public class BillDeskLoadMoneyResponse {
        private String ERROR;
        DATA DATAObject;
        private String STATUS;


        // Getter Methods

        public String getERROR() {
            return ERROR;
        }

        public DATA getDATA() {
            return DATAObject;
        }

        public String getSTATUS() {
            return STATUS;
        }

        // Setter Methods

        public void setERROR(String ERROR) {
            this.ERROR = ERROR;
        }

        public void setDATA(DATA DATAObject) {
            this.DATAObject = DATAObject;
        }

        public void setSTATUS(String STATUS) {
            this.STATUS = STATUS;
        }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BillDeskLoadMoneyResponse that = (BillDeskLoadMoneyResponse) o;

        if (ERROR != null ? !ERROR.equals(that.ERROR) : that.ERROR != null) return false;
        if (DATAObject != null ? !DATAObject.equals(that.DATAObject) : that.DATAObject != null) return false;
        return STATUS != null ? STATUS.equals(that.STATUS) : that.STATUS == null;
    }

    @Override
    public int hashCode() {
        int result = ERROR != null ? ERROR.hashCode() : 0;
        result = 31 * result + (DATAObject != null ? DATAObject.hashCode() : 0);
        result = 31 * result + (STATUS != null ? STATUS.hashCode() : 0);
        return result;
    }
}

