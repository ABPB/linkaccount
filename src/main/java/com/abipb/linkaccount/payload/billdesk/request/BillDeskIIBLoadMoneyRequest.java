package com.abipb.linkaccount.payload.billdesk.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by Rajasekhar.E-V on 26-10-2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder( {
      /*  "CHANNEL",*/
       /* "processType",*/
        "accountNo",
        "mobileNo",
        "custId",
        /*"SESSION_ID",
        "CORP_PRINCIPAL",
        "USER_PRINCIPAL",*/
        "amount",
        "accountType",
        "returnUrl"
        /*"DEC_CHANNEL_ID",*/

})
public class BillDeskIIBLoadMoneyRequest {
   /* @JsonProperty(value = "channel",required = false,defaultValue = "")
  private String channel;*/
 /* @JsonProperty(value = "processType",required = false,defaultValue = "")
  private String processType;*/
  @JsonProperty(value = "accountNo",required = false,defaultValue = "")
  private String accountNo;
  @JsonProperty(value = "mobileNo",required = false,defaultValue = "")
  private String mobileNo;
    @JsonProperty(value = "custId",required = false,defaultValue = "")
    private String custId;
    /*@JsonProperty(value = "SESSION_ID",required = false,defaultValue = "")
    private String SESSION_ID;
    @JsonProperty(value = "CORP_PRINCIPAL",required = false,defaultValue = "")
    private String CORP_PRINCIPAL;
    @JsonProperty(value = "USER_PRINCIPAL",required = false,defaultValue = "")
    private String USER_PRINCIPAL;*/
    @JsonProperty(value = "amount",required = false,defaultValue = "")
    private String amount;
    @JsonProperty(value = "accountType",required = false,defaultValue = "")
    private String accountType;
    @JsonProperty(value = "returnUrl",required = false,defaultValue = "")
    private String returnUrl;
 /* @JsonProperty(value = "DEC_CHANNEL_ID",required = false,defaultValue = "")
  private String DEC_CHANNEL_ID;*/

    /*public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getProcessType() {
        return processType;
    }

    public void setProcessType(String processType) {
        this.processType = processType;
    }*/

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }
}
