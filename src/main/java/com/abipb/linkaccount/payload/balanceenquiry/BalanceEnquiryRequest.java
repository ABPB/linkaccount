package com.abipb.linkaccount.payload.balanceenquiry;

import com.abipb.linkaccount.common.payload.RequestData;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 10/16/2018.
 */

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder( {
        "token",
        "accountId",
        /*"walletAccountId",*/
        "customerId",
})
public class BalanceEnquiryRequest {
    @JsonProperty(value = "token",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String token;
    /*@JsonProperty(value = "savingsAccountId",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String savingsAccountId;
    @JsonProperty(value = "walletAccountId",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String walletAccountId;*/
    @JsonProperty(value = "accountId",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String accountId;
    @JsonProperty(value = "customerId",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String customerId;


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

   /* public String getSavingsAccountId() {        return savingsAccountId;    }

    public void setSavingsAccountId(String savingsAccountId) {        this.savingsAccountId = savingsAccountId;    }

    public String getWalletAccountId() {        return walletAccountId;    }

    public void setWalletAccountId(String walletAccountId) {        this.walletAccountId = walletAccountId;    }*/

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
