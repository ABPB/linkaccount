
package com.abipb.linkaccount.payload.balanceenquiry.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "RequestData",
    "ADBMsgHdr"
})
public class ADBServiceRequest {

    @JsonProperty(value = "RequestData",required = false,defaultValue = "")
    private RequestData requestData;
    @JsonProperty(value = "ADBMsgHdr",required = false,defaultValue = "")
    private ADBMsgHdr aDBMsgHdr;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ADBServiceRequest() {
    }

    /**
     * 
     * @param aDBMsgHdr
     * @param requestData
     */
    public ADBServiceRequest(RequestData requestData, ADBMsgHdr aDBMsgHdr) {
        super();
        this.requestData = requestData;
        this.aDBMsgHdr = aDBMsgHdr;
    }

    @JsonProperty(value = "RequestData",required = false,defaultValue = "")
    public RequestData getRequestData() {
        return requestData;
    }

    @JsonProperty(value = "RequestData",required = false,defaultValue = "")
    public void setRequestData(RequestData requestData) {
        this.requestData = requestData;
    }

    @JsonProperty(value = "ADBMsgHdr",required = false,defaultValue = "")
    public ADBMsgHdr getADBMsgHdr() {
        return aDBMsgHdr;
    }

    @JsonProperty(value = "ADBMsgHdr",required = false,defaultValue = "")
    public void setADBMsgHdr(ADBMsgHdr aDBMsgHdr) {
        this.aDBMsgHdr = aDBMsgHdr;
    }

}
