
package com.abipb.linkaccount.payload.balanceenquiry.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "accountInfo"
})
public class RequestData {

    @JsonProperty(value = "accountInfo",required = false,defaultValue = "")
    private AccountInfo accountInfo;

    /**
     * No args constructor for use in serialization
     * 
     */
    public RequestData() {
    }

    /**
     * 
     * @param accountInfo
     */
    public RequestData(AccountInfo accountInfo) {
        super();
        this.accountInfo = accountInfo;
    }

    @JsonProperty(value = "accountInfo",required = false,defaultValue = "")
    public AccountInfo getAccountInfo() {
        return accountInfo;
    }

    @JsonProperty(value = "accountInfo",required = false,defaultValue = "")
    public void setAccountInfo(AccountInfo accountInfo) {
        this.accountInfo = accountInfo;
    }


}
