
package com.abipb.linkaccount.payload.balanceenquiry.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "staffId",
    "channelId",
    "deviceId"
})
public class UserAttrGrp {

    @JsonProperty(value = "staffId",required = false,defaultValue = "")
    private String staffId;
    @JsonProperty(value = "channelId",required = false,defaultValue = "")
    private String channelId;
    @JsonProperty(value = "deviceId",required = false,defaultValue = "")
    private String deviceId;

    /**
     * No args constructor for use in serialization
     * 
     */
    public UserAttrGrp() {
    }

    /**
     * 
     * @param staffId
     * @param channelId
     * @param deviceId
     */
    public UserAttrGrp(String staffId, String channelId, String deviceId) {
        super();
        this.staffId = staffId;
        this.channelId = channelId;
        this.deviceId = deviceId;
    }

    @JsonProperty(value = "staffId",required = false,defaultValue = "")
    public String getStaffId() {
        return staffId;
    }

    @JsonProperty(value = "staffId",required = false,defaultValue = "")
    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    @JsonProperty(value = "channelId",required = false,defaultValue = "")
    public String getChannelId() {
        return channelId;
    }

    @JsonProperty(value = "channelId",required = false,defaultValue = "")
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    @JsonProperty(value = "deviceId",required = false,defaultValue = "")
    public String getDeviceId() {
        return deviceId;
    }

    @JsonProperty(value = "deviceId",required = false,defaultValue = "")
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }


}
