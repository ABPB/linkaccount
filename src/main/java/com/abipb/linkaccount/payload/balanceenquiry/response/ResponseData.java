
package com.abipb.linkaccount.payload.balanceenquiry.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "accountDlts"
})
public class ResponseData {

    @JsonProperty(value = "accountDlts",required = false,defaultValue = "")
    private List<AccountDlt> accountDlts = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ResponseData() {
    }

    /**
     * 
     * @param accountDlts
     */
    public ResponseData(List<AccountDlt> accountDlts) {
        super();
        this.accountDlts = accountDlts;
    }

    @JsonProperty(value = "accountDlts",required = false,defaultValue = "")
    public List<AccountDlt> getAccountDlts() {
        return accountDlts;
    }

    @JsonProperty(value = "accountDlts",required = false,defaultValue = "")
    public void setAccountDlts(List<AccountDlt> accountDlts) {
        this.accountDlts = accountDlts;
    }


}
