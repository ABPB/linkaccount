
package com.abipb.linkaccount.payload.balanceenquiry.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "balanceType",
    "balanceVal",
    "currency"
})
public class BalanceInfo {

    @JsonProperty(value = "balanceType",required = false,defaultValue = "")
    private String balanceType;
    @JsonProperty(value = "balanceVal",required = false,defaultValue = "")
    private String balanceVal;
    @JsonProperty(value = "currency",required = false,defaultValue = "")
    private String currency;

    /**
     * No args constructor for use in serialization
     * 
     */
    public BalanceInfo() {
    }

    /**
     * 
     * @param balanceVal
     * @param balanceType
     * @param currency
     */
    public BalanceInfo(String balanceType, String balanceVal, String currency) {
        super();
        this.balanceType = balanceType;
        this.balanceVal = balanceVal;
        this.currency = currency;
    }

    @JsonProperty(value = "balanceType",required = false,defaultValue = "")
    public String getBalanceType() {
        return balanceType;
    }

    @JsonProperty(value = "balanceType",required = false,defaultValue = "")
    public void setBalanceType(String balanceType) {
        this.balanceType = balanceType;
    }

    @JsonProperty(value = "balanceVal",required = false,defaultValue = "")
    public String getBalanceVal() {
        return balanceVal;
    }

    @JsonProperty(value = "balanceVal",required = false,defaultValue = "")
    public void setBalanceVal(String balanceVal) {
        this.balanceVal = balanceVal;
    }

    @JsonProperty(value = "currency",required = false,defaultValue = "")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty(value = "currency",required = false,defaultValue = "")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
