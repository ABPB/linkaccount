
package com.abipb.linkaccount.payload.balanceenquiry.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "accountId",
    "balanceInfo"
})
public class AccountDlt {

    @JsonProperty(value = "accountId",required = false,defaultValue = "")
    private String accountId;
    @JsonProperty(value = "balanceInfo",required = false,defaultValue = "")
    private List<BalanceInfo> balanceInfo = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public AccountDlt() {
    }

    /**
     * 
     * @param accountId
     * @param balanceInfo
     */
    public AccountDlt(String accountId, List<BalanceInfo> balanceInfo) {
        super();
        this.accountId = accountId;
        this.balanceInfo = balanceInfo;
    }

    @JsonProperty(value = "accountId",required = false,defaultValue = "")
    public String getAccountId() {
        return accountId;
    }

    @JsonProperty(value = "accountId",required = false,defaultValue = "")
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    @JsonProperty(value = "balanceInfo",required = false,defaultValue = "")
    public List<BalanceInfo> getBalanceInfo() {
        return balanceInfo;
    }

    @JsonProperty(value = "balanceInfo",required = false,defaultValue = "")
    public void setBalanceInfo(List<BalanceInfo> balanceInfo) {
        this.balanceInfo = balanceInfo;
    }

}
