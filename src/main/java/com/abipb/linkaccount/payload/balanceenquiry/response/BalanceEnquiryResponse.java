
package com.abipb.linkaccount.payload.balanceenquiry.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "ADBServiceResponse"
})
public class BalanceEnquiryResponse {

    @JsonProperty(value = "ADBServiceResponse",required = false,defaultValue = "")
    private ADBServiceResponse aDBServiceResponse;

    /**
     * No args constructor for use in serialization
     * 
     */
    public BalanceEnquiryResponse() {
    }

    /**
     * 
     * @param aDBServiceResponse
     */
    public BalanceEnquiryResponse(ADBServiceResponse aDBServiceResponse) {
        super();
        this.aDBServiceResponse = aDBServiceResponse;
    }

    @JsonProperty(value = "ADBServiceResponse",required = false,defaultValue = "")
    public ADBServiceResponse getADBServiceResponse() {
        return aDBServiceResponse;
    }

    @JsonProperty(value = "ADBServiceResponse",required = false,defaultValue = "")
    public void setADBServiceResponse(ADBServiceResponse aDBServiceResponse) {
        this.aDBServiceResponse = aDBServiceResponse;
    }

}
