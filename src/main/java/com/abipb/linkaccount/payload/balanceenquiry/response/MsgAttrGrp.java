
package com.abipb.linkaccount.payload.balanceenquiry.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "serviceVersion",
    "serviceName"
})
public class MsgAttrGrp {

    @JsonProperty(value = "serviceVersion",required = false,defaultValue = "")
    private String serviceVersion;
    @JsonProperty(value = "serviceName",required = false,defaultValue = "")
    private String serviceName;

    /**
     * No args constructor for use in serialization
     * 
     */
    public MsgAttrGrp() {
    }

    /**
     * 
     * @param serviceVersion
     * @param serviceName
     */
    public MsgAttrGrp(String serviceVersion, String serviceName) {
        super();
        this.serviceVersion = serviceVersion;
        this.serviceName = serviceName;
    }

    @JsonProperty(value = "serviceVersion",required = false,defaultValue = "")
    public String getServiceVersion() {
        return serviceVersion;
    }

    @JsonProperty(value = "serviceVersion",required = false,defaultValue = "")
    public void setServiceVersion(String serviceVersion) {
        this.serviceVersion = serviceVersion;
    }

    @JsonProperty(value = "serviceName",required = false,defaultValue = "")
    public String getServiceName() {
        return serviceName;
    }

    @JsonProperty(value = "serviceName",required = false,defaultValue = "")
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }


}
