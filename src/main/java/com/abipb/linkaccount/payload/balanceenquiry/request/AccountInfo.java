
package com.abipb.linkaccount.payload.balanceenquiry.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "accountId",
    "balanceType"
})
public class AccountInfo {

    @JsonProperty(value = "accountId",required = false,defaultValue = "")
    private List<String> accountId = null;
    @JsonProperty(value = "balanceType",required = false,defaultValue = "")
    private String balanceType;


    /**
     * No args constructor for use in serialization
     * 
     */
    public AccountInfo() {
    }

    /**
     * 
     * @param accountId
     * @param balanceType
     */
    public AccountInfo(List<String> accountId, String balanceType) {
        super();
        this.accountId = accountId;
        this.balanceType = balanceType;
    }

    @JsonProperty(value = "accountId",required = false,defaultValue = "")
    public List<String> getAccountId() {
        return accountId;
    }

    @JsonProperty(value = "accountId",required = false,defaultValue = "")
    public void setAccountId(List<String> accountId) {
        this.accountId = accountId;
    }

    @JsonProperty(value = "balanceType",required = false,defaultValue = "")
    public String getBalanceType() {
        return balanceType;
    }

    @JsonProperty(value = "balanceType",required = false,defaultValue = "")
    public void setBalanceType(String balanceType) {
        this.balanceType = balanceType;
    }



}
