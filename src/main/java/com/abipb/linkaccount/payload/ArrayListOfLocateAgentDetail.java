package com.abipb.linkaccount.payload;

import com.abipb.linkaccount.common.payload.ResponseData;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by abhay.kumar on 9/24/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder( {
        "locateAgentDetils"
})
public class ArrayListOfLocateAgentDetail extends ResponseData {
    @JsonProperty(value = "locateAgentDetils",required = false, index = -1,access = JsonProperty.Access.AUTO)
    List<LocateAgentDetil> locateAgentDetils;

    public ArrayListOfLocateAgentDetail() {
        this.locateAgentDetils=new LinkedList<>();
    }

    public ArrayListOfLocateAgentDetail(List<LocateAgentDetil> locateAgentDetils) {
        this.locateAgentDetils = locateAgentDetils;
    }

    public List<LocateAgentDetil> getLocateAgentDetils() {
        return locateAgentDetils;
    }

    public void setLocateAgentDetils(List<LocateAgentDetil> locateAgentDetils) {
        this.locateAgentDetils = locateAgentDetils;
    }
}
