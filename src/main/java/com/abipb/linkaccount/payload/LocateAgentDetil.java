package com.abipb.linkaccount.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by abhay.kumar on 9/24/2018.
 */

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "agentName",
        "mobileNo",
        "address",
        "district",
        "city",
        "pinCode"
})
public class LocateAgentDetil {
    @JsonProperty(value = "agentName",required = false,defaultValue = "")
    private String agentName;
    @JsonProperty(value = "mobileNo",required = false,defaultValue = "")
    private String mobileNo;
    @JsonProperty(value = "address",required = false,defaultValue = "")
    private String address;
    @JsonProperty(value = "district",required = false,defaultValue = "")
    private String district;
    @JsonProperty(value = "city",required = false,defaultValue = "")
    private String city;
    @JsonProperty(value = "sessionId",required = false,defaultValue = "")
    private String pinCode;

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LocateAgentDetil that = (LocateAgentDetil) o;

        if (agentName != null ? !agentName.equals(that.agentName) : that.agentName != null) return false;
        if (mobileNo != null ? !mobileNo.equals(that.mobileNo) : that.mobileNo != null) return false;
        if (address != null ? !address.equals(that.address) : that.address != null) return false;
        if (district != null ? !district.equals(that.district) : that.district != null) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        return pinCode != null ? pinCode.equals(that.pinCode) : that.pinCode == null;
    }

    @Override
    public int hashCode() {
        int result = agentName != null ? agentName.hashCode() : 0;
        result = 31 * result + (mobileNo != null ? mobileNo.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (district != null ? district.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (pinCode != null ? pinCode.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LocateAgentDetil{" +
                "agentName='" + agentName + '\'' +
                ", mobileNo='" + mobileNo + '\'' +
                ", address='" + address + '\'' +
                ", district='" + district + '\'' +
                ", city='" + city + '\'' +
                ", pinCode='" + pinCode + '\'' +
                '}';
    }
}
