package com.abipb.linkaccount.payload;

import com.abipb.linkaccount.common.payload.ResponseData;

/**
 * Created by pardhasaradhi.k on 10/17/2018.
 */
public class LoadMnyResp extends ResponseData {
    private String reasonCode;
    private String reasonDescription;

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }
}
