package com.abipb.linkaccount.payload.accountDetails.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 10/26/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "reasonCode",
        "reasonDesc"
})
public class Status {

    @JsonProperty(value="reasonCode",required = false,defaultValue = "")
    private String reasonCode;
    @JsonProperty(value="reasonDesc",required = false,defaultValue = "")
    private String reasonDesc;

    public Status(){}


    @JsonProperty(value="reasonCode",required = false,defaultValue = "")
    public String getReasonCode() {        return reasonCode;    }

    @JsonProperty(value="reasonCode",required = false,defaultValue = "")
    public void setReasonCode(String reasonCode) {        this.reasonCode = reasonCode;    }

    @JsonProperty(value="reasonDesc",required = false,defaultValue = "")
    public String getReasonDesc() {        return reasonDesc;    }

    @JsonProperty(value="reasonDesc",required = false,defaultValue = "")
    public void setReasonDesc(String reasonDesc) {        this.reasonDesc = reasonDesc;    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Status status = (Status) o;

        if (!reasonCode.equals(status.reasonCode)) return false;
        return reasonDesc.equals(status.reasonDesc);
    }

    @Override
    public int hashCode() {
        int result = reasonCode.hashCode();
        result = 31 * result + reasonDesc.hashCode();
        return result;
    }
}
