package com.abipb.linkaccount.payload.accountDetails;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 11/1/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder( {
        "token",
        "deviceId",
        "customerNumber",
})
public class AccountRetrievalRequest {
    @JsonProperty(value = "token",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String token;
    @JsonProperty(value = "deviceId",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String deviceId;
    @JsonProperty(value = "customerNumber",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String customerNumber;

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
