package com.abipb.linkaccount.payload.accountDetails.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 10/26/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "accountNumber",
        "cardNumber",
        "accountStatus",
        "accountType",
        "balanceIndicator",
        "avalBalance",
        "lienBalance",
        "bankNumber",
        "branchName",
        "currantBalance",
        "relationship",
        "startDate",
        "accTypeIndicator",
        "ifscCode",
        "branchId",
        "schemeCode"
})
public class AccountDetails {
    @JsonProperty(value = "accountNumber",required = false,defaultValue = "")
    private String accountNumber;
    @JsonProperty(value = "cardNumber",required = false,defaultValue = "")
    private String  cardNumber;
    @JsonProperty(value = "accountStatus",required = false,defaultValue = "")
    private String accountStatus;
    @JsonProperty(value = "accountType",required = false,defaultValue = "")
    private String accountType;
    @JsonProperty(value = "balanceIndicator",required = false,defaultValue = "")
    private String balanceIndicator;
    @JsonProperty(value = "avalBalance",required = false,defaultValue = "")
    private String avalBalance;
    @JsonProperty(value = "lienBalance",required = false,defaultValue = "")
    private String lienBalance;
    @JsonProperty(value = "bankNumber",required = false,defaultValue = "")
    private String bankNumber;
    @JsonProperty(value = "branchName",required = false,defaultValue = "")
    private String branchName;
    @JsonProperty(value = "currantBalance",required = false,defaultValue = "")
    private String currantBalance;
    @JsonProperty(value = "relationship",required = false,defaultValue = "")
    private String relationship;
    @JsonProperty(value = "startDate",required = false,defaultValue = "")
    private String startDate;
    @JsonProperty(value = "accTypeIndicator",required = false,defaultValue = "")
    private String accTypeIndicator;
    @JsonProperty(value = "ifscCode",required = false,defaultValue = "")
    private String ifscCode;
    @JsonProperty(value = "branchId",required = false,defaultValue = "")
    private String branchId;
    @JsonProperty(value = "schemeCode",required = false,defaultValue = "")
    private String schemeCode;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getBalanceIndicator() {
        return balanceIndicator;
    }

    public void setBalanceIndicator(String balanceIndicator) {
        this.balanceIndicator = balanceIndicator;
    }

    public String getAvalBalance() {
        return avalBalance;
    }

    public void setAvalBalance(String avalBalance) {
        this.avalBalance = avalBalance;
    }

    public String getLienBalance() {
        return lienBalance;
    }

    public void setLienBalance(String lienBalance) {
        this.lienBalance = lienBalance;
    }

    public String getBankNumber() {
        return bankNumber;
    }

    public void setBankNumber(String bankNumber) {
        this.bankNumber = bankNumber;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getCurrantBalance() {
        return currantBalance;
    }

    public void setCurrantBalance(String currantBalance) {
        this.currantBalance = currantBalance;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getAccTypeIndicator() {
        return accTypeIndicator;
    }

    public void setAccTypeIndicator(String accTypeIndicator) {
        this.accTypeIndicator = accTypeIndicator;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getSchemeCode() {
        return schemeCode;
    }

    public void setSchemeCode(String schemeCode) {
        this.schemeCode = schemeCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountDetails that = (AccountDetails) o;

        if (!accountNumber.equals(that.accountNumber)) return false;
        if (!cardNumber.equals(that.cardNumber)) return false;
        if (!accountStatus.equals(that.accountStatus)) return false;
        if (!accountType.equals(that.accountType)) return false;
        if (!balanceIndicator.equals(that.balanceIndicator)) return false;
        if (!avalBalance.equals(that.avalBalance)) return false;
        if (!lienBalance.equals(that.lienBalance)) return false;
        if (!bankNumber.equals(that.bankNumber)) return false;
        if (!branchName.equals(that.branchName)) return false;
        if (!currantBalance.equals(that.currantBalance)) return false;
        if (!relationship.equals(that.relationship)) return false;
        if (!startDate.equals(that.startDate)) return false;
        if (!accTypeIndicator.equals(that.accTypeIndicator)) return false;
        if (!ifscCode.equals(that.ifscCode)) return false;
        if (!branchId.equals(that.branchId)) return false;
        return schemeCode.equals(that.schemeCode);
    }

    @Override
    public int hashCode() {
        int result = accountNumber.hashCode();
        result = 31 * result + cardNumber.hashCode();
        result = 31 * result + accountStatus.hashCode();
        result = 31 * result + accountType.hashCode();
        result = 31 * result + balanceIndicator.hashCode();
        result = 31 * result + avalBalance.hashCode();
        result = 31 * result + lienBalance.hashCode();
        result = 31 * result + bankNumber.hashCode();
        result = 31 * result + branchName.hashCode();
        result = 31 * result + currantBalance.hashCode();
        result = 31 * result + relationship.hashCode();
        result = 31 * result + startDate.hashCode();
        result = 31 * result + accTypeIndicator.hashCode();
        result = 31 * result + ifscCode.hashCode();
        result = 31 * result + branchId.hashCode();
        result = 31 * result + schemeCode.hashCode();
        return result;
    }
}
