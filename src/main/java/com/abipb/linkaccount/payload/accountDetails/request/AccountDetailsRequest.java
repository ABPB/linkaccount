package com.abipb.linkaccount.payload.accountDetails.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 10/26/2018.
 */

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "ADBServiceRequest"
})
public class AccountDetailsRequest {
    @JsonProperty(value="ADBServiceRequest",required = false,defaultValue = "")
    private ADBServiceRequest adbServiceRequest;

    public ADBServiceRequest getAdbServiceRequest() {
        return adbServiceRequest;
    }

    public void setAdbServiceRequest(ADBServiceRequest adbServiceRequest) {
        this.adbServiceRequest = adbServiceRequest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountDetailsRequest that = (AccountDetailsRequest) o;

        return adbServiceRequest.equals(that.adbServiceRequest);
    }

    @Override
    public int hashCode() {
        return adbServiceRequest.hashCode();
    }
}
