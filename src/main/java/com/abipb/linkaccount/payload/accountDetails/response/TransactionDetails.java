package com.abipb.linkaccount.payload.accountDetails.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 10/26/2018.
 */

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "transDate",
        "valueDate",
        "transId",
        "reportCode",
        "transType",
        "transSubType",
        "debitCreditIndicator",
        "transValueDate",
        "transAmount",
        "transParticulars",
        "instrumentNumber",
        "deliveryChannelId",
        "beneficiaryName",
        "payeeName",
        "referenceNum",
        "accountNumber",
        "schemeCode"

})
public class TransactionDetails {

    @JsonProperty(value="transDate",required = false,defaultValue = "")
    private String transDate;
    @JsonProperty(value="valueDate",required = false,defaultValue = "")
    private String valueDate;
    @JsonProperty(value="transId",required = false,defaultValue = "")
    private String transId;
    @JsonProperty(value="reportCode",required = false,defaultValue = "")
    private String reportCode;
    @JsonProperty(value="transType",required = false,defaultValue = "")
    private String transType;
    @JsonProperty(value="transSubType",required = false,defaultValue = "")
    private String transSubType;
    @JsonProperty(value="debitCreditIndicator",required = false,defaultValue = "")
    private String debitCreditIndicator;
    @JsonProperty(value="transValueDate",required = false,defaultValue = "")
    private String transValueDate;
    @JsonProperty(value="transAmount",required = false,defaultValue = "")
    private String transAmount;
    @JsonProperty(value="transParticulars",required = false,defaultValue = "")
    private String transParticulars;
    @JsonProperty(value="instrumentNumber",required = false,defaultValue = "")
    private String instrumentNumber;
    @JsonProperty(value="deliveryChannelId",required = false,defaultValue = "")
    private String deliveryChannelId;
    @JsonProperty(value="beneficiaryName",required = false,defaultValue = "")
    private String beneficiaryName;
    @JsonProperty(value="payeeName",required = false,defaultValue = "")
    private String payeeName;
    @JsonProperty(value="referenceNum",required = false,defaultValue = "")
    private String referenceNum;
    @JsonProperty(value="accountNumber",required = false,defaultValue = "")
    private String accountNumber;
    @JsonProperty(value="schemeCode",required = false,defaultValue = "")
    private String schemeCode;

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getValueDate() {
        return valueDate;
    }

    public void setValueDate(String valueDate) {
        this.valueDate = valueDate;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getReportCode() {
        return reportCode;
    }

    public void setReportCode(String reportCode) {
        this.reportCode = reportCode;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getTransSubType() {
        return transSubType;
    }

    public void setTransSubType(String transSubType) {
        this.transSubType = transSubType;
    }

    public String getDebitCreditIndicator() {
        return debitCreditIndicator;
    }

    public void setDebitCreditIndicator(String debitCreditIndicator) {
        this.debitCreditIndicator = debitCreditIndicator;
    }

    public String getTransValueDate() {
        return transValueDate;
    }

    public void setTransValueDate(String transValueDate) {
        this.transValueDate = transValueDate;
    }

    public String getTransAmount() {
        return transAmount;
    }

    public void setTransAmount(String transAmount) {
        this.transAmount = transAmount;
    }

    public String getTransParticulars() {
        return transParticulars;
    }

    public void setTransParticulars(String transParticulars) {
        this.transParticulars = transParticulars;
    }

    public String getInstrumentNumber() {
        return instrumentNumber;
    }

    public void setInstrumentNumber(String instrumentNumber) {
        this.instrumentNumber = instrumentNumber;
    }

    public String getDeliveryChannelId() {
        return deliveryChannelId;
    }

    public void setDeliveryChannelId(String deliveryChannelId) {
        this.deliveryChannelId = deliveryChannelId;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getPayeeName() {
        return payeeName;
    }

    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public String getReferenceNum() {
        return referenceNum;
    }

    public void setReferenceNum(String referenceNum) {
        this.referenceNum = referenceNum;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getSchemeCode() {
        return schemeCode;
    }

    public void setSchemeCode(String schemeCode) {
        this.schemeCode = schemeCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TransactionDetails that = (TransactionDetails) o;

        if (!transDate.equals(that.transDate)) return false;
        if (!valueDate.equals(that.valueDate)) return false;
        if (!transId.equals(that.transId)) return false;
        if (!reportCode.equals(that.reportCode)) return false;
        if (!transType.equals(that.transType)) return false;
        if (!transSubType.equals(that.transSubType)) return false;
        if (!debitCreditIndicator.equals(that.debitCreditIndicator)) return false;
        if (!transValueDate.equals(that.transValueDate)) return false;
        if (!transAmount.equals(that.transAmount)) return false;
        if (!transParticulars.equals(that.transParticulars)) return false;
        if (!instrumentNumber.equals(that.instrumentNumber)) return false;
        if (!deliveryChannelId.equals(that.deliveryChannelId)) return false;
        if (!beneficiaryName.equals(that.beneficiaryName)) return false;
        if (!payeeName.equals(that.payeeName)) return false;
        if (!referenceNum.equals(that.referenceNum)) return false;
        if (!accountNumber.equals(that.accountNumber)) return false;
        return schemeCode.equals(that.schemeCode);
    }

    @Override
    public int hashCode() {
        int result = transDate.hashCode();
        result = 31 * result + valueDate.hashCode();
        result = 31 * result + transId.hashCode();
        result = 31 * result + reportCode.hashCode();
        result = 31 * result + transType.hashCode();
        result = 31 * result + transSubType.hashCode();
        result = 31 * result + debitCreditIndicator.hashCode();
        result = 31 * result + transValueDate.hashCode();
        result = 31 * result + transAmount.hashCode();
        result = 31 * result + transParticulars.hashCode();
        result = 31 * result + instrumentNumber.hashCode();
        result = 31 * result + deliveryChannelId.hashCode();
        result = 31 * result + beneficiaryName.hashCode();
        result = 31 * result + payeeName.hashCode();
        result = 31 * result + referenceNum.hashCode();
        result = 31 * result + accountNumber.hashCode();
        result = 31 * result + schemeCode.hashCode();
        return result;
    }
}
