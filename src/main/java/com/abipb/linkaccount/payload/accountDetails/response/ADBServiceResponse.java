package com.abipb.linkaccount.payload.accountDetails.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 10/26/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "ADBMsgHdr",
        "Status",
        "ResponseData"
})
public class ADBServiceResponse {

    @JsonProperty(value = "ADBMsgHdr",required = false,defaultValue = "")
    private ADBMsgHdr aDBMsgHdr;

    @JsonProperty(value="Status",required = false,defaultValue = "")
    private Status status;

    @JsonProperty(value="ResponseData",required = false,defaultValue = "")
    private ResponseData responseData;

    public ADBServiceResponse(){}

    public ADBServiceResponse(ADBMsgHdr aDBMsgHdr, Status status, ResponseData responseData) {
        this.aDBMsgHdr = aDBMsgHdr;
        this.status = status;
        this.responseData = responseData;
    }

    public ADBMsgHdr getaDBMsgHdr() {
        return aDBMsgHdr;
    }

    public void setaDBMsgHdr(ADBMsgHdr aDBMsgHdr) {
        this.aDBMsgHdr = aDBMsgHdr;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ResponseData getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseData responseData) {
        this.responseData = responseData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ADBServiceResponse that = (ADBServiceResponse) o;

        if (!aDBMsgHdr.equals(that.aDBMsgHdr)) return false;
        if (!status.equals(that.status)) return false;
        return responseData.equals(that.responseData);
    }

    @Override
    public int hashCode() {
        int result = aDBMsgHdr.hashCode();
        result = 31 * result + status.hashCode();
        result = 31 * result + responseData.hashCode();
        return result;
    }
}
