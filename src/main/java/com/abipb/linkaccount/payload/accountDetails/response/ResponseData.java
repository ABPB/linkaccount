package com.abipb.linkaccount.payload.accountDetails.response;

/**
 * Created by pardhasaradhi.k on 10/26/2018.
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Collection;
import java.util.List;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "customerName",
        "FirstName",
        "MiddleName",
        "LastName",
        "customerId",
        "mobileNumber",
        "panNumber",
        "dateOfBirth",
        "emailId",
        "gender",
        "customerRole",
        "State",
        "PartnerBankAccNumber",
        "PartnerBankCIF",
        "PartnerBankName",
        "totalAvalBalance",
        "accountDetails",
        "transRecord"
})
public class ResponseData {
    @JsonProperty(value = "customerName", required = false, defaultValue = "")
    private String customerName;
    @JsonProperty(value = "FirstName", required = false, defaultValue = "")
    private String FirstName;
    @JsonProperty(value = "MiddleName", required = false, defaultValue = "")
    private String MiddleName;
    @JsonProperty(value = "LastName", required = false, defaultValue = "")
    private String LastName;
    @JsonProperty(value = "customerId", required = false, defaultValue = "")
    private String customerId;
    @JsonProperty(value = "mobileNumber", required = false, defaultValue = "")
    private String mobileNumber;
    @JsonProperty(value = "panNumber", required = false, defaultValue = "")
    private String panNumber;
    @JsonProperty(value = "dateOfBirth", required = false, defaultValue = "")
    private String dateOfBirth;
    @JsonProperty(value = "emailId", required = false, defaultValue = "")
    private String emailId;
    @JsonProperty(value = "gender", required = false, defaultValue = "")
    private String gender;
    @JsonProperty(value = "customerRole", required = false, defaultValue = "")
    private String customerRole;
    @JsonProperty(value = "State", required = false, defaultValue = "")
    private String State;
    @JsonProperty(value = "PartnerBankAccNumber", required = false, defaultValue = "")
    private String PartnerBankAccNumber;
    @JsonProperty(value = "PartnerBankCIF", required = false, defaultValue = "")
    private String PartnerBankCIF;
    @JsonProperty(value = "PartnerBankName", required = false, defaultValue = "")
    private String PartnerBankName;
    @JsonProperty(value = "totalAvalBalance", required = false, defaultValue = "")
    private double totalAvalBalance;
    @JsonProperty(value = "accountDetails", required = false)
    private Collection<AccountDetails> accountDetails;
    @JsonProperty(value = "transRecord", required = false)
    private Collection<TransactionRecord> transRecord;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String middleName) {
        MiddleName = middleName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCustomerRole() {
        return customerRole;
    }

    public void setCustomerRole(String customerRole) {
        this.customerRole = customerRole;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getPartnerBankAccNumber() {
        return PartnerBankAccNumber;
    }

    public void setPartnerBankAccNumber(String partnerBankAccNumber) {
        PartnerBankAccNumber = partnerBankAccNumber;
    }

    public String getPartnerBankCIF() {
        return PartnerBankCIF;
    }

    public void setPartnerBankCIF(String partnerBankCIF) {
        PartnerBankCIF = partnerBankCIF;
    }

    public String getPartnerBankName() {
        return PartnerBankName;
    }

    public void setPartnerBankName(String partnerBankName) {
        PartnerBankName = partnerBankName;
    }

    public double getTotalAvalBalance() {
        return totalAvalBalance;
    }

    public void setTotalAvalBalance(double totalAvalBalance) {
        this.totalAvalBalance = totalAvalBalance;
    }

    public Collection<AccountDetails> getAccountDetails() {
        return accountDetails;
    }

    public void setAccountDetails(Collection<AccountDetails> accountDetails) {
        this.accountDetails = accountDetails;
    }

    public Collection<TransactionRecord> getTransRecord() {
        return transRecord;
    }

    public void setTransRecord(Collection<TransactionRecord> transRecord) {
        this.transRecord = transRecord;
    }
}

