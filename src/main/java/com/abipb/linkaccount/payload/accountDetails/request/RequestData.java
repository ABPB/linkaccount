package com.abipb.linkaccount.payload.accountDetails.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 10/26/2018.
 */

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "customerNumber"
})
public class RequestData {

    @JsonProperty(value = "customerNumber",required = false,defaultValue = "")
    private String customerNumber;

    public RequestData(){

    }
    public RequestData(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    @JsonProperty(value = "customerNumber",required = false,defaultValue = "")
    public String getCustomerNumber() {        return customerNumber;    }

    @JsonProperty(value = "customerNumber",required = false,defaultValue = "")
    public void setCustomerNumber(String customerNumber) {        this.customerNumber = customerNumber;    }
}
