
package com.abipb.linkaccount.payload.accountDetails.request;


import com.abipb.linkaccount.payload.accountDetails.request.MsgAttrGrp;
import com.abipb.linkaccount.payload.accountDetails.request.MsgIdentifyGrp;
import com.abipb.linkaccount.payload.accountDetails.request.UserAttrGrp;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "msgIdentifyGrp",
        "sourceChnlInfoGrp",
        "userAttrGrp",
        "msgAttrGrp"
})
public class ADBMsgHdr {

    @JsonProperty(value = "msgIdentifyGrp",required = false,defaultValue = "")
    private MsgIdentifyGrp msgIdentifyGrp;
    @JsonProperty(value = "userAttrGrp",required = false,defaultValue = "")
    private UserAttrGrp userAttrGrp;
    @JsonProperty(value = "msgAttrGrp",required = false,defaultValue = "")
    private MsgAttrGrp msgAttrGrp;
    @JsonProperty(value="sourceChnlInfoGrp",required = false,defaultValue = "")
    private SourceChnlInfoGrp sourceChnlInfoGrp;

    /**
     * No args constructor for use in serialization
     */
    public ADBMsgHdr() {
    }

    /**
     * @param msgIdentifyGrp
     * @param userAttrGrp
     * @param msgAttrGrp
     */
    public ADBMsgHdr(MsgIdentifyGrp msgIdentifyGrp, UserAttrGrp userAttrGrp, MsgAttrGrp msgAttrGrp,SourceChnlInfoGrp sourceChnlInfoGrp) {
        super();
        this.msgIdentifyGrp = msgIdentifyGrp;
        this.userAttrGrp = userAttrGrp;
        this.msgAttrGrp = msgAttrGrp;
        this.sourceChnlInfoGrp=sourceChnlInfoGrp;
    }

    @JsonProperty(value = "msgIdentifyGrp",required = false,defaultValue = "")
    public MsgIdentifyGrp getMsgIdentifyGrp() {
        return msgIdentifyGrp;
    }

    @JsonProperty(value = "msgIdentifyGrp",required = false,defaultValue = "")
    public void setMsgIdentifyGrp(MsgIdentifyGrp msgIdentifyGrp) {
        this.msgIdentifyGrp = msgIdentifyGrp;
    }

    @JsonProperty(value = "userAttrGrp",required = false,defaultValue = "")
    public UserAttrGrp getUserAttrGrp() {
        return userAttrGrp;
    }

    @JsonProperty(value = "userAttrGrp",required = false,defaultValue = "")
    public void setUserAttrGrp(UserAttrGrp userAttrGrp) {
        this.userAttrGrp = userAttrGrp;
    }

    @JsonProperty(value = "msgAttrGrp",required = false,defaultValue = "")
    public MsgAttrGrp getMsgAttrGrp() {
        return msgAttrGrp;
    }

    @JsonProperty(value = "msgAttrGrp",required = false,defaultValue = "")
    public void setMsgAttrGrp(MsgAttrGrp msgAttrGrp) {
        this.msgAttrGrp = msgAttrGrp;
    }

    @JsonProperty(value="sourceChnlInfoGrp",required = false,defaultValue = "")
    public SourceChnlInfoGrp getSourceChnlInfoGrp() {        return sourceChnlInfoGrp;    }

    @JsonProperty(value="sourceChnlInfoGrp",required = false,defaultValue = "")
    public void setSourceChnlInfoGrp(SourceChnlInfoGrp sourceChnlInfoGrp) {        this.sourceChnlInfoGrp = sourceChnlInfoGrp;    }
}
