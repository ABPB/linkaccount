package com.abipb.linkaccount.payload.accountDetails.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

/**
 * Created by pardhasaradhi.k on 10/26/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "moreDataRequired",
        "noOfTransactions",
        "transactionDetails"
})
public class TransactionRecord {

    @JsonProperty(value="moreDataRequired",required = false,defaultValue = "")
    private String moreDataRequired;
    @JsonProperty(value="noOfTransactions",required = false,defaultValue = "")
    private int noOfTransactions;
    @JsonProperty(value="transactionDetails",required = false,defaultValue = "")
    private List<TransactionDetails> transactionDetailsList;

    public int getNoOfTransactions() {        return noOfTransactions;    }

    public void setNoOfTransactions(int noOfTransactions) {        this.noOfTransactions = noOfTransactions;    }

    public List<TransactionDetails> getTransactionDetailsList() {
        return transactionDetailsList;
    }

    public void setTransactionDetailsList(List<TransactionDetails> transactionDetailsList) {
        this.transactionDetailsList = transactionDetailsList;
    }

    public String getMoreDataRequired() {

        return moreDataRequired;
    }

    public void setMoreDataRequired(String moreDataRequired) {
        this.moreDataRequired = moreDataRequired;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TransactionRecord that = (TransactionRecord) o;

        if (noOfTransactions != that.noOfTransactions) return false;
        if (!moreDataRequired.equals(that.moreDataRequired)) return false;
        return transactionDetailsList.equals(that.transactionDetailsList);
    }

    @Override
    public int hashCode() {
        int result = moreDataRequired.hashCode();
        result = 31 * result + noOfTransactions;
        result = 31 * result + transactionDetailsList.hashCode();
        return result;
    }
}

