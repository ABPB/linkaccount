package com.abipb.linkaccount.payload.accountDetails.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 10/26/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
      "ADBServiceResponse"
})
public class AccountDetailsResponse {
    @JsonProperty(value = "ADBServiceResponse",required = false,defaultValue = "")
    private ADBServiceResponse adbServiceResponse;

    public AccountDetailsResponse(){}


    public AccountDetailsResponse(ADBServiceResponse adbServiceResponse) {
        this.adbServiceResponse = adbServiceResponse;
    }

    public ADBServiceResponse getAdbServiceResponse() {
        return adbServiceResponse;
    }

    public void setAdbServiceResponse(ADBServiceResponse adbServiceResponse) {
        this.adbServiceResponse = adbServiceResponse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountDetailsResponse that = (AccountDetailsResponse) o;

        return adbServiceResponse != null ? adbServiceResponse.equals(that.adbServiceResponse) : that.adbServiceResponse == null;
    }

    @Override
    public int hashCode() {
        return adbServiceResponse != null ? adbServiceResponse.hashCode() : 0;
    }
}

