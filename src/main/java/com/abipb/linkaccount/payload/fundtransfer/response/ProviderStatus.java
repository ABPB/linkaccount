package com.abipb.linkaccount.payload.fundtransfer.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 10/17/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "StatusCode",
        "StatusDesc"
})
public class ProviderStatus {
    @JsonProperty(value = "StatusCode",required = false,defaultValue = "")
    private String statusCode;
    @JsonProperty(value = "StatusDesc",required = false,defaultValue = "")
    private String statusDesc;

    @JsonProperty(value = "StatusCode",required = false,defaultValue = "")
    public String getStatusCode() {        return statusCode;    }

    @JsonProperty(value = "StatusCode",required = false,defaultValue = "")
    public void setStatusCode(String statusCode) {        this.statusCode = statusCode;    }

    @JsonProperty(value = "StatusDesc",required = false,defaultValue = "")
    public String getStatusDesc() {        return statusDesc;    }

    @JsonProperty(value = "StatusDesc",required = false,defaultValue = "")
    public void setStatusDesc(String statusDesc) {        this.statusDesc = statusDesc;    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProviderStatus that = (ProviderStatus) o;

        if (statusCode != null ? !statusCode.equals(that.statusCode) : that.statusCode != null) return false;
        return statusDesc != null ? statusDesc.equals(that.statusDesc) : that.statusDesc == null;
    }

    @Override
    public int hashCode() {
        int result = statusCode != null ? statusCode.hashCode() : 0;
        result = 31 * result + (statusDesc != null ? statusDesc.hashCode() : 0);
        return result;
    }
}


