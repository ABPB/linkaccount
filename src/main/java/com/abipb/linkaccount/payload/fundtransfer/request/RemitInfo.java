
package com.abipb.linkaccount.payload.fundtransfer.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "RemitReversal",
    "RemitReferenceNo",
    "RemitAmount",
    "RemitCurrency",
    "RemitRemark",
    "RemitReasonCode"
})
public class RemitInfo {

    @JsonProperty(value = "RemitReversal",required = false,defaultValue = "")
    private String remitReversal;
    @JsonProperty(value = "RemitReferenceNo",required = false,defaultValue = "")
    private RemitReferenceNo remitReferenceNo;
    @JsonProperty(value = "RemitAmount",required = false,defaultValue = "")
    private String remitAmount;
    @JsonProperty(value = "RemitCurrency",required = false,defaultValue = "")
    private String remitCurrency;
    @JsonProperty(value = "RemitRemark",required = false,defaultValue = "")
    private String remitRemark;
    @JsonProperty(value = "RemitReasonCode",required = false,defaultValue = "")
    private RemitReasonCode remitReasonCode;

    /**
     * No args constructor for use in serialization
     * 
     */
    public RemitInfo() {
    }

    /**
     * 
     * @param remitReversal
     * @param remitCurrency
     * @param remitReferenceNo
     * @param remitReasonCode
     * @param remitRemark
     * @param remitAmount
     */
    public RemitInfo(String remitReversal, RemitReferenceNo remitReferenceNo, String remitAmount, String remitCurrency, String remitRemark, RemitReasonCode remitReasonCode) {
        super();
        this.remitReversal = remitReversal;
        this.remitReferenceNo = remitReferenceNo;
        this.remitAmount = remitAmount;
        this.remitCurrency = remitCurrency;
        this.remitRemark = remitRemark;
        this.remitReasonCode = remitReasonCode;
    }

    @JsonProperty(value = "RemitReversal",required = false,defaultValue = "")
    public String getRemitReversal() {
        return remitReversal;
    }

    @JsonProperty(value = "RemitReversal",required = false,defaultValue = "")
    public void setRemitReversal(String remitReversal) {
        this.remitReversal = remitReversal;
    }

    @JsonProperty(value = "RemitReferenceNo",required = false,defaultValue = "")
    public RemitReferenceNo getRemitReferenceNo() {
        return remitReferenceNo;
    }

    @JsonProperty(value = "RemitReferenceNo",required = false,defaultValue = "")
    public void setRemitReferenceNo(RemitReferenceNo remitReferenceNo) {
        this.remitReferenceNo = remitReferenceNo;
    }

    @JsonProperty(value = "RemitAmount",required = false,defaultValue = "")
    public String getRemitAmount() {
        return remitAmount;
    }

    @JsonProperty(value = "RemitAmount",required = false,defaultValue = "")
    public void setRemitAmount(String remitAmount) {
        this.remitAmount = remitAmount;
    }

    @JsonProperty(value = "RemitCurrency",required = false,defaultValue = "")
    public String getRemitCurrency() {
        return remitCurrency;
    }

    @JsonProperty(value = "RemitCurrency",required = false,defaultValue = "")
    public void setRemitCurrency(String remitCurrency) {
        this.remitCurrency = remitCurrency;
    }

    @JsonProperty(value = "RemitRemark",required = false,defaultValue = "")
    public String getRemitRemark() {
        return remitRemark;
    }

    @JsonProperty(value = "RemitRemark",required = false,defaultValue = "")
    public void setRemitRemark(String remitRemark) {
        this.remitRemark = remitRemark;
    }

    @JsonProperty(value = "RemitReasonCode",required = false,defaultValue = "")
    public RemitReasonCode getRemitReasonCode() {
        return remitReasonCode;
    }

    @JsonProperty(value = "RemitReasonCode",required = false,defaultValue = "")
    public void setRemitReasonCode(RemitReasonCode remitReasonCode) {
        this.remitReasonCode = remitReasonCode;
    }


}
