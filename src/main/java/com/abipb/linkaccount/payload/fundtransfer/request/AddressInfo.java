
package com.abipb.linkaccount.payload.fundtransfer.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "AddrLine1",
    "AddrLine2",
    "AddrLine3",
    "City",
    "State",
    "PostCode",
    "Country"
})
public class AddressInfo {

    @JsonProperty(value = "AddrLine1",required = false,defaultValue = "")
    private AddrLine1 addrLine1;
    @JsonProperty(value = "AddrLine2",required = false,defaultValue = "")
    private AddrLine2 addrLine2;
    @JsonProperty(value = "AddrLine3",required = false,defaultValue = "")
    private AddrLine3 addrLine3;
    @JsonProperty(value = "City",required = false,defaultValue = "")
    private City city;
    @JsonProperty(value = "State",required = false,defaultValue = "")
    private State state;
    @JsonProperty(value = "PostCode",required = false,defaultValue = "")
    private PostCode postCode;
    @JsonProperty(value = "Country",required = false,defaultValue = "")
    private Country country;

    /**
     * No args constructor for use in serialization
     * 
     */
    public AddressInfo() {
    }

    /**
     * 
     * @param addrLine1
     * @param addrLine2
     * @param addrLine3
     * @param postCode
     * @param state
     * @param country
     * @param city
     */
    public AddressInfo(AddrLine1 addrLine1, AddrLine2 addrLine2, AddrLine3 addrLine3, City city, State state, PostCode postCode, Country country) {
        super();
        this.addrLine1 = addrLine1;
        this.addrLine2 = addrLine2;
        this.addrLine3 = addrLine3;
        this.city = city;
        this.state = state;
        this.postCode = postCode;
        this.country = country;
    }

    @JsonProperty(value = "AddrLine1",required = false,defaultValue = "")
    public AddrLine1 getAddrLine1() {
        return addrLine1;
    }

    @JsonProperty(value = "AddrLine1",required = false,defaultValue = "")
    public void setAddrLine1(AddrLine1 addrLine1) {
        this.addrLine1 = addrLine1;
    }

    @JsonProperty(value = "AddrLine2",required = false,defaultValue = "")
    public AddrLine2 getAddrLine2() {
        return addrLine2;
    }

    @JsonProperty(value = "AddrLine2",required = false,defaultValue = "")
    public void setAddrLine2(AddrLine2 addrLine2) {
        this.addrLine2 = addrLine2;
    }

    @JsonProperty(value = "AddrLine3",required = false,defaultValue = "")
    public AddrLine3 getAddrLine3() {
        return addrLine3;
    }

    @JsonProperty(value = "AddrLine3",required = false,defaultValue = "")
    public void setAddrLine3(AddrLine3 addrLine3) {
        this.addrLine3 = addrLine3;
    }

    @JsonProperty(value = "City",required = false,defaultValue = "")
    public City getCity() {
        return city;
    }

    @JsonProperty(value = "City",required = false,defaultValue = "")
    public void setCity(City city) {
        this.city = city;
    }

    @JsonProperty(value = "State",required = false,defaultValue = "")
    public State getState() {
        return state;
    }

    @JsonProperty(value = "State",required = false,defaultValue = "")
    public void setState(State state) {
        this.state = state;
    }

    @JsonProperty(value = "PostCode",required = false,defaultValue = "")
    public PostCode getPostCode() {
        return postCode;
    }

    @JsonProperty(value = "PostCode",required = false,defaultValue = "")
    public void setPostCode(PostCode postCode) {
        this.postCode = postCode;
    }

    @JsonProperty(value = "Country",required = false,defaultValue = "")
    public Country getCountry() {
        return country;
    }

    @JsonProperty(value = "Country",required = false,defaultValue = "")
    public void setCountry(Country country) {
        this.country = country;
    }

}
