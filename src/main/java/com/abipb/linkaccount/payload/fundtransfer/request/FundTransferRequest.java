
package com.abipb.linkaccount.payload.fundtransfer.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "ADBServiceRequest"
})
public class FundTransferRequest {

    @JsonProperty(value = "ADBServiceRequest",required = false,defaultValue = "")
    private ADBServiceRequest aDBServiceRequest;
    /**
     * No args constructor for use in serialization
     * 
     */
    public FundTransferRequest() {
    }

    /**
     * 
     * @param aDBServiceRequest
     */
    public FundTransferRequest(ADBServiceRequest aDBServiceRequest) {
        super();
        this.aDBServiceRequest = aDBServiceRequest;
    }

    @JsonProperty(value = "ADBServiceRequest",required = false,defaultValue = "")
    public ADBServiceRequest getADBServiceRequest() {
        return aDBServiceRequest;
    }

    @JsonProperty(value = "ADBServiceRequest",required = false,defaultValue = "")
    public void setADBServiceRequest(ADBServiceRequest aDBServiceRequest) {
        this.aDBServiceRequest = aDBServiceRequest;
    }


}
