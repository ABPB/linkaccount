
package com.abipb.linkaccount.payload.fundtransfer.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "AddrTypeIndicator",
    "Name",
    "AddressInfo",
    "BankDetails",
    "MobileNumber",
    "AadharNumber",
    "MMID",
    "CreditRemark",
    "Txn_remarks"
})
public class CreditorInfo {

    @JsonProperty(value = "AddrTypeIndicator",required = false,defaultValue = "")
    private String addrTypeIndicator;
    @JsonProperty(value = "Name",required = false,defaultValue = "")
    private String name;
    @JsonProperty(value = "AddressInfo",required = false,defaultValue = "")
    private AddressInfo addressInfo;
    @JsonProperty(value = "BankDetails",required = false,defaultValue = "")
    private BankDetails bankDetails;
    @JsonProperty(value = "MobileNumber",required = false,defaultValue = "")
    private String mobileNumber;
    @JsonProperty(value = "AadharNumber",required = false,defaultValue = "")
    private AadharNumber aadharNumber;
    @JsonProperty(value = "MMID",required = false,defaultValue = "")
    private String mMID;
    @JsonProperty(value = "CreditRemark",required = false,defaultValue = "")
    private String creditRemark;
    @JsonProperty(value = "Txn_remarks",required = false,defaultValue = "")
    private String txnRemarks;

    /**
     * No args constructor for use in serialization
     * 
     */
    public CreditorInfo() {
    }

    /**
     * 
     * @param bankDetails
     * @param mMID
     * @param txnRemarks
     * @param addressInfo
     * @param name
     * @param aadharNumber
     * @param creditRemark
     * @param mobileNumber
     * @param addrTypeIndicator
     */
    public CreditorInfo(String addrTypeIndicator, String name, AddressInfo addressInfo, BankDetails bankDetails, String mobileNumber, AadharNumber aadharNumber, String mMID, String creditRemark, String txnRemarks) {
        super();
        this.addrTypeIndicator = addrTypeIndicator;
        this.name = name;
        this.addressInfo = addressInfo;
        this.bankDetails = bankDetails;
        this.mobileNumber = mobileNumber;
        this.aadharNumber = aadharNumber;
        this.mMID = mMID;
        this.creditRemark = creditRemark;
        this.txnRemarks = txnRemarks;
    }

    @JsonProperty(value = "AddrTypeIndicator",required = false,defaultValue = "")
    public String getAddrTypeIndicator() {
        return addrTypeIndicator;
    }

    @JsonProperty(value = "AddrTypeIndicator",required = false,defaultValue = "")
    public void setAddrTypeIndicator(String addrTypeIndicator) {
        this.addrTypeIndicator = addrTypeIndicator;
    }

    @JsonProperty(value = "Name",required = false,defaultValue = "")
    public String getName() {
        return name;
    }

    @JsonProperty(value = "Name",required = false,defaultValue = "")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty(value = "AddressInfo",required = false,defaultValue = "")
    public AddressInfo getAddressInfo() {
        return addressInfo;
    }

    @JsonProperty(value = "AddressInfo",required = false,defaultValue = "")
    public void setAddressInfo(AddressInfo addressInfo) {
        this.addressInfo = addressInfo;
    }

    @JsonProperty(value = "BankDetails",required = false,defaultValue = "")
    public BankDetails getBankDetails() {
        return bankDetails;
    }

    @JsonProperty(value = "BankDetails",required = false,defaultValue = "")
    public void setBankDetails(BankDetails bankDetails) {
        this.bankDetails = bankDetails;
    }

    @JsonProperty(value = "MobileNumber",required = false,defaultValue = "")
    public String getMobileNumber() {
        return mobileNumber;
    }

    @JsonProperty(value = "MobileNumber",required = false,defaultValue = "")
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @JsonProperty(value = "AadharNumber",required = false,defaultValue = "")
    public AadharNumber getAadharNumber() {
        return aadharNumber;
    }

    @JsonProperty(value = "AadharNumber",required = false,defaultValue = "")
    public void setAadharNumber(AadharNumber aadharNumber) {
        this.aadharNumber = aadharNumber;
    }

    @JsonProperty(value = "MMID",required = false,defaultValue = "")
    public String getMMID() {
        return mMID;
    }

    @JsonProperty(value = "MMID",required = false,defaultValue = "")
    public void setMMID(String mMID) {
        this.mMID = mMID;
    }

    @JsonProperty(value = "CreditRemark",required = false,defaultValue = "")
    public String getCreditRemark() {
        return creditRemark;
    }

    @JsonProperty(value = "CreditRemark",required = false,defaultValue = "")
    public void setCreditRemark(String creditRemark) {
        this.creditRemark = creditRemark;
    }

    @JsonProperty(value = "Txn_remarks",required = false,defaultValue = "")
    public String getTxnRemarks() {
        return txnRemarks;
    }

    @JsonProperty(value = "Txn_remarks",required = false,defaultValue = "")
    public void setTxnRemarks(String txnRemarks) {
        this.txnRemarks = txnRemarks;
    }


}
