
package com.abipb.linkaccount.payload.fundtransfer.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "xmlns",
    "msgIdentifyGrp",
    "userAttrGrp"
})
public class ADBMsgHdr {

    @JsonProperty(value = "xmlns",required = false,defaultValue = "")
    private String xmlns;
    @JsonProperty(value = "msgIdentifyGrp",required = false,defaultValue = "")
    private MsgIdentifyGrp msgIdentifyGrp;
    @JsonProperty(value = "userAttrGrp",required = false,defaultValue = "")
    private UserAttrGrp userAttrGrp;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ADBMsgHdr() {
    }

    /**
     * 
     * @param msgIdentifyGrp
     * @param userAttrGrp
     * @param xmlns
     */
    public ADBMsgHdr(String xmlns, MsgIdentifyGrp msgIdentifyGrp, UserAttrGrp userAttrGrp) {
        super();
        this.xmlns = xmlns;
        this.msgIdentifyGrp = msgIdentifyGrp;
        this.userAttrGrp = userAttrGrp;
    }

    @JsonProperty(value = "xmlns",required = false,defaultValue = "")
    public String getXmlns() {
        return xmlns;
    }

    @JsonProperty(value = "xmlns",required = false,defaultValue = "")
    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    @JsonProperty(value = "msgIdentifyGrp",required = false,defaultValue = "")
    public MsgIdentifyGrp getMsgIdentifyGrp() {
        return msgIdentifyGrp;
    }

    @JsonProperty(value = "msgIdentifyGrp",required = false,defaultValue = "")
    public void setMsgIdentifyGrp(MsgIdentifyGrp msgIdentifyGrp) {
        this.msgIdentifyGrp = msgIdentifyGrp;
    }

    @JsonProperty(value = "userAttrGrp",required = false,defaultValue = "")
    public UserAttrGrp getUserAttrGrp() {
        return userAttrGrp;
    }

    @JsonProperty(value = "userAttrGrp",required = false,defaultValue = "")
    public void setUserAttrGrp(UserAttrGrp userAttrGrp) {
        this.userAttrGrp = userAttrGrp;
    }

}
