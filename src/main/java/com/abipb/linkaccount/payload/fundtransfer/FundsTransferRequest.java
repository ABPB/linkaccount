package com.abipb.linkaccount.payload.fundtransfer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 11/1/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder( {
        "token",
        "deviceId",
        "PaymentMode",
        "RemitAmount",
        "RemitCurrency",
        "RemitRemark",
        "creditorAccountId",
        "IFSCCode",
        "CreditRemark",
        "Txn_remarks",
        "debitorAccountId"
})
public class FundsTransferRequest {

    @JsonProperty(value = "token",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String token;
    @JsonProperty(value = "deviceId",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String deviceId;
    @JsonProperty(value = "PaymentMode",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String paymentMode;
    @JsonProperty(value = "RemitAmount",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String remitAmount;
    @JsonProperty(value = "RemitCurrency",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String remitCurrency;
    @JsonProperty(value = "RemitRemark",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String remitRemark;
    @JsonProperty(value = "creditorAccountId",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String creditorAccountId;
    @JsonProperty(value = "IFSCCode",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String iFSCCode;
    @JsonProperty(value = "CreditRemark",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String creditRemark;
    @JsonProperty(value = "Txn_remarks",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String txnRemarks;
    @JsonProperty(value = "debitorAccountId",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String debitorAccountId;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getRemitAmount() {
        return remitAmount;
    }

    public void setRemitAmount(String remitAmount) {
        this.remitAmount = remitAmount;
    }

    public String getRemitCurrency() {
        return remitCurrency;
    }

    public void setRemitCurrency(String remitCurrency) {
        this.remitCurrency = remitCurrency;
    }

    public String getRemitRemark() {
        return remitRemark;
    }

    public void setRemitRemark(String remitRemark) {
        this.remitRemark = remitRemark;
    }

    public String getCreditorAccountId() {
        return creditorAccountId;
    }

    public void setCreditorAccountId(String creditorAccountId) {
        this.creditorAccountId = creditorAccountId;
    }

    public String getiFSCCode() {
        return iFSCCode;
    }

    public void setiFSCCode(String iFSCCode) {
        this.iFSCCode = iFSCCode;
    }

    public String getCreditRemark() {
        return creditRemark;
    }

    public void setCreditRemark(String creditRemark) {
        this.creditRemark = creditRemark;
    }

    public String getTxnRemarks() {
        return txnRemarks;
    }

    public void setTxnRemarks(String txnRemarks) {
        this.txnRemarks = txnRemarks;
    }

    public String getDebitorAccountId() {
        return debitorAccountId;
    }

    public void setDebitorAccountId(String debitorAccountId) {
        this.debitorAccountId = debitorAccountId;
    }
}
