
package com.abipb.linkaccount.payload.fundtransfer.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "channelId",
    "staffId",
    "deviceId"
})
public class UserAttrGrp {

    @JsonProperty(value = "channelId",required = false,defaultValue = "")
    private String channelId;
    @JsonProperty(value = "staffId",required = false,defaultValue = "")
    private Object staffId;
    @JsonProperty(value = "deviceId",required = false,defaultValue = "")
    private String deviceId;

    /**
     * No args constructor for use in serialization
     * 
     */
    public UserAttrGrp() {
    }

    /**
     * 
     * @param staffId
     * @param channelId
     * @param deviceId
     */
    public UserAttrGrp(String channelId, Object staffId, String deviceId) {
        super();
        this.channelId = channelId;
        this.staffId = staffId;
        this.deviceId = deviceId;
    }

    @JsonProperty(value = "channelId",required = false,defaultValue = "")
    public String getChannelId() {
        return channelId;
    }

    @JsonProperty(value = "channelId",required = false,defaultValue = "")
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    @JsonProperty(value = "staffId",required = false,defaultValue = "")
    public Object getStaffId() {
        return staffId;
    }

    @JsonProperty(value = "staffId",required = false,defaultValue = "")
    public void setStaffId(Object staffId) {
        this.staffId = staffId;
    }

    @JsonProperty(value = "deviceId",required = false,defaultValue = "")
    public String getDeviceId() {
        return deviceId;
    }

    @JsonProperty(value = "deviceId",required = false,defaultValue = "")
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }


}
