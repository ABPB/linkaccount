
package com.abipb.linkaccount.payload.fundtransfer.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "CSPIdentifier",
    "SrvcCtgIdentifier",
    "retailerAccountNumber"
})
public class RetailerInfo {

    @JsonProperty(value = "CSPIdentifier",required = false,defaultValue = "")
    private String cSPIdentifier;
    @JsonProperty(value = "SrvcCtgIdentifier",required = false,defaultValue = "")
    private String srvcCtgIdentifier;
    @JsonProperty(value = "retailerAccountNumber",required = false,defaultValue = "")
    private String retailerAccountNumber;

    /**
     * No args constructor for use in serialization
     * 
     */
    public RetailerInfo() {
    }

    /**
     * 
     * @param retailerAccountNumber
     * @param srvcCtgIdentifier
     * @param cSPIdentifier
     */
    public RetailerInfo(String cSPIdentifier, String srvcCtgIdentifier, String retailerAccountNumber) {
        super();
        this.cSPIdentifier = cSPIdentifier;
        this.srvcCtgIdentifier = srvcCtgIdentifier;
        this.retailerAccountNumber = retailerAccountNumber;
    }

    @JsonProperty(value = "CSPIdentifier",required = false,defaultValue = "")
    public String getCSPIdentifier() {
        return cSPIdentifier;
    }

    @JsonProperty(value = "CSPIdentifier",required = false,defaultValue = "")
    public void setCSPIdentifier(String cSPIdentifier) {
        this.cSPIdentifier = cSPIdentifier;
    }

    @JsonProperty(value = "SrvcCtgIdentifier",required = false,defaultValue = "")
    public String getSrvcCtgIdentifier() {
        return srvcCtgIdentifier;
    }

    @JsonProperty(value = "SrvcCtgIdentifier",required = false,defaultValue = "")
    public void setSrvcCtgIdentifier(String srvcCtgIdentifier) {
        this.srvcCtgIdentifier = srvcCtgIdentifier;
    }

    @JsonProperty(value = "retailerAccountNumber",required = false,defaultValue = "")
    public String getRetailerAccountNumber() {
        return retailerAccountNumber;
    }

    @JsonProperty(value = "retailerAccountNumber",required = false,defaultValue = "")
    public void setRetailerAccountNumber(String retailerAccountNumber) {
        this.retailerAccountNumber = retailerAccountNumber;
    }


}
