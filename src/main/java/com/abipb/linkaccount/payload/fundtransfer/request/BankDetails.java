
package com.abipb.linkaccount.payload.fundtransfer.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "BankName",
    "AddressType",
    "AddressInfo",
    "BankId",
    "BranchId",
    "AccountId",
    "IFSCCode"
})
public class BankDetails {

    @JsonProperty(value = "BankName",required = false,defaultValue = "")
    private String bankName;
    @JsonProperty(value = "AddressType",required = false,defaultValue = "")
    private AddressType addressType;
    @JsonProperty(value = "AddressInfo",required = false,defaultValue = "")
    private AddressInfo addressInfo;
    @JsonProperty(value = "BankId",required = false,defaultValue = "")
    private String bankId;
    @JsonProperty(value = "BranchId",required = false,defaultValue = "")
    private String branchId;
    @JsonProperty(value = "AccountId",required = false,defaultValue = "")
    private String accountId;
    @JsonProperty(value = "IFSCCode",required = false,defaultValue = "")
    private String iFSCCode;

    /**
     * No args constructor for use in serialization
     * 
     */
    public BankDetails() {
    }

    /**
     * 
     * @param accountId
     * @param branchId
     * @param addressInfo
     * @param bankName
     * @param addressType
     * @param iFSCCode
     * @param bankId
     */
    public BankDetails(String bankName, AddressType addressType, AddressInfo addressInfo, String bankId, String branchId, String accountId, String iFSCCode) {
        super();
        this.bankName = bankName;
        this.addressType = addressType;
        this.addressInfo = addressInfo;
        this.bankId = bankId;
        this.branchId = branchId;
        this.accountId = accountId;
        this.iFSCCode = iFSCCode;
    }

    @JsonProperty(value = "BankName",required = false,defaultValue = "")
    public String getBankName() {
        return bankName;
    }

    @JsonProperty(value = "BankName",required = false,defaultValue = "")
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @JsonProperty(value = "AddressType",required = false,defaultValue = "")
    public AddressType getAddressType() {
        return addressType;
    }

    @JsonProperty(value = "AddressType",required = false,defaultValue = "")
    public void setAddressType(AddressType addressType) {
        this.addressType = addressType;
    }

    @JsonProperty(value = "AddressInfo",required = false,defaultValue = "")
    public AddressInfo getAddressInfo() {
        return addressInfo;
    }

    @JsonProperty(value = "AddressInfo",required = false,defaultValue = "")
    public void setAddressInfo(AddressInfo addressInfo) {
        this.addressInfo = addressInfo;
    }

    @JsonProperty(value = "BankId",required = false,defaultValue = "")
    public String getBankId() {
        return bankId;
    }

    @JsonProperty(value = "BankId",required = false,defaultValue = "")
    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    @JsonProperty(value = "BranchId",required = false,defaultValue = "")
    public String getBranchId() {
        return branchId;
    }

    @JsonProperty(value = "BranchId",required = false,defaultValue = "")
    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    @JsonProperty(value = "AccountId",required = false,defaultValue = "")
    public String getAccountId() {
        return accountId;
    }

    @JsonProperty(value = "AccountId",required = false,defaultValue = "")
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    @JsonProperty(value = "IFSCCode",required = false,defaultValue = "")
    public String getIFSCCode() {
        return iFSCCode;
    }

    @JsonProperty(value = "IFSCCode",required = false,defaultValue = "")
    public void setIFSCCode(String iFSCCode) {
        this.iFSCCode = iFSCCode;
    }

}
