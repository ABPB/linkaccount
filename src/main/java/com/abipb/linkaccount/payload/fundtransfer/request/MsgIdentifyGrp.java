
package com.abipb.linkaccount.payload.fundtransfer.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "applicationTranId",
    "rqstDateTime",
    "sourceChnlInfoGrp",
    "msgAttrGrp"
})
public class MsgIdentifyGrp {

    @JsonProperty(value = "applicationTranId",required = false,defaultValue = "")
    private String applicationTranId;
    @JsonProperty(value = "rqstDateTime",required = false,defaultValue = "")
    private String rqstDateTime;
    @JsonProperty(value = "sourceChnlInfoGrp",required = false,defaultValue = "")
    private SourceChnlInfoGrp sourceChnlInfoGrp;
    @JsonProperty(value = "msgAttrGrp",required = false,defaultValue = "")
    private MsgAttrGrp msgAttrGrp;

    /**
     * No args constructor for use in serialization
     * 
     */
    public MsgIdentifyGrp() {
    }

    /**
     * 
     * @param applicationTranId
     * @param sourceChnlInfoGrp
     * @param rqstDateTime
     * @param msgAttrGrp
     */
    public MsgIdentifyGrp(String applicationTranId, String rqstDateTime, SourceChnlInfoGrp sourceChnlInfoGrp, MsgAttrGrp msgAttrGrp) {
        super();
        this.applicationTranId = applicationTranId;
        this.rqstDateTime = rqstDateTime;
        this.sourceChnlInfoGrp = sourceChnlInfoGrp;
        this.msgAttrGrp = msgAttrGrp;
    }

    @JsonProperty(value = "applicationTranId",required = false,defaultValue = "")
    public String getApplicationTranId() {
        return applicationTranId;
    }

    @JsonProperty(value = "applicationTranId",required = false,defaultValue = "")
    public void setApplicationTranId(String applicationTranId) {
        this.applicationTranId = applicationTranId;
    }

    @JsonProperty(value = "rqstDateTime",required = false,defaultValue = "")
    public String getRqstDateTime() {
        return rqstDateTime;
    }

    @JsonProperty(value = "rqstDateTime",required = false,defaultValue = "")
    public void setRqstDateTime(String rqstDateTime) {
        this.rqstDateTime = rqstDateTime;
    }

    @JsonProperty(value = "sourceChnlInfoGrp",required = false,defaultValue = "")
    public SourceChnlInfoGrp getSourceChnlInfoGrp() {
        return sourceChnlInfoGrp;
    }

    @JsonProperty(value = "sourceChnlInfoGrp",required = false,defaultValue = "")
    public void setSourceChnlInfoGrp(SourceChnlInfoGrp sourceChnlInfoGrp) {
        this.sourceChnlInfoGrp = sourceChnlInfoGrp;
    }

    @JsonProperty(value = "msgAttrGrp",required = false,defaultValue = "")
    public MsgAttrGrp getMsgAttrGrp() {
        return msgAttrGrp;
    }

    @JsonProperty(value = "msgAttrGrp",required = false,defaultValue = "")
    public void setMsgAttrGrp(MsgAttrGrp msgAttrGrp) {
        this.msgAttrGrp = msgAttrGrp;
    }


}
