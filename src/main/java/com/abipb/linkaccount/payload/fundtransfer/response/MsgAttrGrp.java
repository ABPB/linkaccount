
package com.abipb.linkaccount.payload.fundtransfer.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "serviceName",
    "serviceVersionNbr"
})
public class MsgAttrGrp {

    @JsonProperty(value = "serviceName",required = false,defaultValue = "")
    private String serviceName;
    @JsonProperty(value = "serviceVersionNbr",required = false,defaultValue = "")
    private String serviceVersionNbr;

    /**
     * No args constructor for use in serialization
     * 
     */
    public MsgAttrGrp() {
    }

    /**
     * 
     * @param serviceVersionNbr
     * @param serviceName
     */
    public MsgAttrGrp(String serviceName, String serviceVersionNbr) {
        super();
        this.serviceName = serviceName;
        this.serviceVersionNbr = serviceVersionNbr;
    }

    @JsonProperty(value = "serviceName",required = false,defaultValue = "")
    public String getServiceName() {
        return serviceName;
    }

    @JsonProperty(value = "serviceName",required = false,defaultValue = "")
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    @JsonProperty(value = "serviceVersionNbr",required = false,defaultValue = "")
    public String getServiceVersionNbr() {
        return serviceVersionNbr;
    }

    @JsonProperty(value = "serviceVersionNbr",required = false,defaultValue = "")
    public void setServiceVersionNbr(String serviceVersionNbr) {
        this.serviceVersionNbr = serviceVersionNbr;
    }


}
