
package com.abipb.linkaccount.payload.fundtransfer.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "ADBMsgHdr",
    "Status",
    "ResponseData"
})
public class ADBServiceResponse {

    @JsonProperty(value = "ADBMsgHdr",required = false,defaultValue = "")
    private ADBMsgHdr aDBMsgHdr;
    @JsonProperty(value = "Status",required = false,defaultValue = "")
    private Status status;
    @JsonProperty(value = "ResponseData",required = false,defaultValue = "")
    private ResponseData responseData;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ADBServiceResponse() {
    }

    /**
     * 
     * @param responseData
     * @param status
     * @param aDBMsgHdr
     */
    public ADBServiceResponse(ADBMsgHdr aDBMsgHdr, Status status, ResponseData responseData) {
        super();
        this.aDBMsgHdr = aDBMsgHdr;
        this.status = status;
        this.responseData = responseData;
    }

    @JsonProperty(value = "ADBMsgHdr",required = false,defaultValue = "")
    public ADBMsgHdr getADBMsgHdr() {
        return aDBMsgHdr;
    }

    @JsonProperty(value = "ADBMsgHdr",required = false,defaultValue = "")
    public void setADBMsgHdr(ADBMsgHdr aDBMsgHdr) {
        this.aDBMsgHdr = aDBMsgHdr;
    }

    @JsonProperty(value = "Status",required = false,defaultValue = "")
    public Status getStatus() {
        return status;
    }

    @JsonProperty(value = "Status",required = false,defaultValue = "")
    public void setStatus(Status status) {
        this.status = status;
    }

    @JsonProperty(value = "ResponseData",required = false,defaultValue = "")
    public ResponseData getResponseData() {
        return responseData;
    }

    @JsonProperty(value = "ResponseData",required = false,defaultValue = "")
    public void setResponseData(ResponseData responseData) {
        this.responseData = responseData;
    }

}
