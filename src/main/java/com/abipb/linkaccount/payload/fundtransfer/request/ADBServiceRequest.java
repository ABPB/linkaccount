
package com.abipb.linkaccount.payload.fundtransfer.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "xmlns",
    "ADBMsgHdr",
    "RequestData"
})
public class ADBServiceRequest {

    @JsonProperty(value = "xmlns",required = false,defaultValue = "")
    private String xmlns;
    @JsonProperty(value = "ADBMsgHdr",required = false,defaultValue = "")
    private ADBMsgHdr aDBMsgHdr;
    @JsonProperty(value = "RequestData",required = false,defaultValue = "")
    private RequestData requestData;
   /**
     * No args constructor for use in serialization
     * 
     */
    public ADBServiceRequest() {
    }

    /**
     * 
     * @param xmlns
     * @param aDBMsgHdr
     * @param requestData
     */
    public ADBServiceRequest(String xmlns, ADBMsgHdr aDBMsgHdr, RequestData requestData) {
        super();
        this.xmlns = xmlns;
        this.aDBMsgHdr = aDBMsgHdr;
        this.requestData = requestData;
    }

    @JsonProperty(value = "xmlns",required = false,defaultValue = "")
    public String getXmlns() {
        return xmlns;
    }

    @JsonProperty(value = "xmlns",required = false,defaultValue = "")
    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    @JsonProperty(value = "ADBMsgHdr",required = false,defaultValue = "")
    public ADBMsgHdr getADBMsgHdr() {
        return aDBMsgHdr;
    }

    @JsonProperty(value = "ADBMsgHdr",required = false,defaultValue = "")
    public void setADBMsgHdr(ADBMsgHdr aDBMsgHdr) {
        this.aDBMsgHdr = aDBMsgHdr;
    }

    @JsonProperty(value = "RequestData",required = false,defaultValue = "")
    public RequestData getRequestData() {
        return requestData;
    }

    @JsonProperty(value = "RequestData",required = false,defaultValue = "")
    public void setRequestData(RequestData requestData) {
        this.requestData = requestData;
    }


}
