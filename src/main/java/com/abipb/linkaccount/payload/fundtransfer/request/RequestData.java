
package com.abipb.linkaccount.payload.fundtransfer.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "xmlns",
    "RetailerInfo",
    "PaymentProd",
    "PaymentMode",
    "ProcessingCode",
    "RemitInfo",
    "ChargeAccountId",
    "CreditorInfo",
    "DebitorInfo"
})
public class RequestData {

    @JsonProperty(value = "xmlns",required = false,defaultValue = "")
    private String xmlns;
    @JsonProperty(value = "RetailerInfo",required = false,defaultValue = "")
    private RetailerInfo retailerInfo;
    @JsonProperty(value = "PaymentProd",required = false,defaultValue = "")
    private String paymentProd;
    @JsonProperty(value = "PaymentMode",required = false,defaultValue = "")
    private String paymentMode;
    @JsonProperty(value = "ProcessingCode",required = false,defaultValue = "")
    private ProcessingCode processingCode;
    @JsonProperty(value = "RemitInfo",required = false,defaultValue = "")
    private RemitInfo remitInfo;
    @JsonProperty(value = "ChargeAccountId",required = false,defaultValue = "")
    private ChargeAccountId chargeAccountId;
    @JsonProperty(value = "CreditorInfo",required = false,defaultValue = "")
    private CreditorInfo creditorInfo;
    @JsonProperty(value = "DebitorInfo",required = false,defaultValue = "")
    private DebitorInfo debitorInfo;


    /**
     * No args constructor for use in serialization
     * 
     */
    public RequestData() {
    }

    /**
     * 
     * @param paymentMode
     * @param paymentProd
     * @param remitInfo
     * @param debitorInfo
     * @param chargeAccountId
     * @param xmlns
     * @param creditorInfo
     * @param processingCode
     * @param retailerInfo
     */
    public RequestData(String xmlns, RetailerInfo retailerInfo, String paymentProd, String paymentMode, ProcessingCode processingCode, RemitInfo remitInfo, ChargeAccountId chargeAccountId, CreditorInfo creditorInfo, DebitorInfo debitorInfo) {
        super();
        this.xmlns = xmlns;
        this.retailerInfo = retailerInfo;
        this.paymentProd = paymentProd;
        this.paymentMode = paymentMode;
        this.processingCode = processingCode;
        this.remitInfo = remitInfo;
        this.chargeAccountId = chargeAccountId;
        this.creditorInfo = creditorInfo;
        this.debitorInfo = debitorInfo;
    }

    @JsonProperty(value = "xmlns",required = false,defaultValue = "")
    public String getXmlns() {
        return xmlns;
    }

    @JsonProperty(value = "xmlns",required = false,defaultValue = "")
    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    @JsonProperty(value = "RetailerInfo",required = false,defaultValue = "")
    public RetailerInfo getRetailerInfo() {
        return retailerInfo;
    }

    @JsonProperty(value = "RetailerInfo",required = false,defaultValue = "")
    public void setRetailerInfo(RetailerInfo retailerInfo) {
        this.retailerInfo = retailerInfo;
    }

    @JsonProperty(value = "PaymentProd",required = false,defaultValue = "")
    public String getPaymentProd() {
        return paymentProd;
    }

    @JsonProperty(value = "PaymentProd",required = false,defaultValue = "")
    public void setPaymentProd(String paymentProd) {
        this.paymentProd = paymentProd;
    }

    @JsonProperty(value = "PaymentMode",required = false,defaultValue = "")
    public String getPaymentMode() {
        return paymentMode;
    }

    @JsonProperty(value = "PaymentMode",required = false,defaultValue = "")
    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    @JsonProperty(value = "ProcessingCode",required = false,defaultValue = "")
    public ProcessingCode getProcessingCode() {
        return processingCode;
    }

    @JsonProperty(value = "ProcessingCode",required = false,defaultValue = "")
    public void setProcessingCode(ProcessingCode processingCode) {
        this.processingCode = processingCode;
    }

    @JsonProperty(value = "RemitInfo",required = false,defaultValue = "")
    public RemitInfo getRemitInfo() {
        return remitInfo;
    }

    @JsonProperty(value = "RemitInfo",required = false,defaultValue = "")
    public void setRemitInfo(RemitInfo remitInfo) {
        this.remitInfo = remitInfo;
    }

    @JsonProperty(value = "ChargeAccountId",required = false,defaultValue = "")
    public ChargeAccountId getChargeAccountId() {
        return chargeAccountId;
    }

    @JsonProperty(value = "ChargeAccountId",required = false,defaultValue = "")
    public void setChargeAccountId(ChargeAccountId chargeAccountId) {
        this.chargeAccountId = chargeAccountId;
    }

    @JsonProperty(value = "CreditorInfo",required = false,defaultValue = "")
    public CreditorInfo getCreditorInfo() {
        return creditorInfo;
    }

    @JsonProperty(value = "CreditorInfo",required = false,defaultValue = "")
    public void setCreditorInfo(CreditorInfo creditorInfo) {
        this.creditorInfo = creditorInfo;
    }

    @JsonProperty(value = "DebitorInfo",required = false,defaultValue = "")
    public DebitorInfo getDebitorInfo() {
        return debitorInfo;
    }

    @JsonProperty(value = "DebitorInfo",required = false,defaultValue = "")
    public void setDebitorInfo(DebitorInfo debitorInfo) {
        this.debitorInfo = debitorInfo;
    }

}
