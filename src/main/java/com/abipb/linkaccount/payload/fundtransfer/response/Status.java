
package com.abipb.linkaccount.payload.fundtransfer.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "reasonCode",
    "reasonDesc",
    "ProviderStatus"
})
public class Status {

    @JsonProperty(value = "reasonCode",required = false,defaultValue = "")
    private String reasonCode;
    @JsonProperty(value = "reasonDesc",required = false,defaultValue = "")
    private String reasonDesc;
    @JsonProperty(value = "ProviderStatus",required = false,defaultValue = "")
    private ProviderStatus providerStatus;

    public Status() {
    }

    public Status(String reasonCode, String reasonDesc) {
        super();
        this.reasonCode = reasonCode;
        this.reasonDesc = reasonDesc;
    }

    @JsonProperty(value = "reasonCode",required = false,defaultValue = "")
    public String getReasonCode() {
        return reasonCode;
    }

    @JsonProperty(value = "reasonCode",required = false,defaultValue = "")
    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    @JsonProperty(value = "reasonDesc",required = false,defaultValue = "")
    public String getReasonDesc() {
        return reasonDesc;
    }

    @JsonProperty(value = "reasonDesc",required = false,defaultValue = "")
    public void setReasonDesc(String reasonDesc) {
        this.reasonDesc = reasonDesc;
    }

    @JsonProperty(value = "ProviderStatus",required = false,defaultValue = "")
    public ProviderStatus getProviderStatus() {        return providerStatus;    }

    @JsonProperty(value = "ProviderStatus",required = false,defaultValue = "")
    public void setProviderStatus(ProviderStatus providerStatus) {        this.providerStatus = providerStatus;    }

}
