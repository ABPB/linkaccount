
package com.abipb.linkaccount.payload.fundtransfer.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "TranId",
    "TranDate",
    "ActionCode",
    "ResponseCode",
    "RemitMMID",
    "RemitterName",
    "BenificiaryAccount",
    "BeneficiaryName",
    "PaymentRefNum",
    "PaymentOrderId"
})
public class ResponseData {

    @JsonProperty(value = "TranId",required = false,defaultValue = "")
    private String tranId;
    @JsonProperty(value = "TranDate",required = false,defaultValue = "")
    private String tranDate;
    @JsonProperty(value = "ActionCode",required = false,defaultValue = "")
    private String actionCode;
    @JsonProperty(value = "ResponseCode",required = false,defaultValue = "")
    private String responseCode;
    @JsonProperty(value = "RemitMMID",required = false,defaultValue = "")
    private String remitMMID;
    @JsonProperty(value = "RemitterName",required = false,defaultValue = "")
    private String remitterName;
    @JsonProperty(value = "BenificiaryAccount",required = false,defaultValue = "")
    private String benificiaryAccount;
    @JsonProperty(value = "BeneficiaryName",required = false,defaultValue = "")
    private String beneficiaryName;
    @JsonProperty(value = "PaymentRefNum",required = false,defaultValue = "")
    private String paymentRefNum;
    @JsonProperty(value = "PaymentOrderId",required = false,defaultValue = "")
    private String paymentOrderId;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ResponseData() {
    }

    /**
     * 
     * @param actionCode
     * @param benificiaryAccount
     * @param responseCode
     * @param beneficiaryName
     * @param tranDate
     * @param remitterName
     * @param paymentRefNum
     * @param remitMMID
     * @param paymentOrderId
     * @param tranId
     */
    public ResponseData(String tranId, String tranDate, String actionCode, String responseCode, String remitMMID, String remitterName, String benificiaryAccount, String beneficiaryName, String paymentRefNum, String paymentOrderId) {
        super();
        this.tranId = tranId;
        this.tranDate = tranDate;
        this.actionCode = actionCode;
        this.responseCode = responseCode;
        this.remitMMID = remitMMID;
        this.remitterName = remitterName;
        this.benificiaryAccount = benificiaryAccount;
        this.beneficiaryName = beneficiaryName;
        this.paymentRefNum = paymentRefNum;
        this.paymentOrderId = paymentOrderId;
    }

    @JsonProperty(value = "TranId",required = false,defaultValue = "")
    public String getTranId() {
        return tranId;
    }

    @JsonProperty(value = "TranId",required = false,defaultValue = "")
    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    @JsonProperty(value = "TranDate",required = false,defaultValue = "")
    public String getTranDate() {
        return tranDate;
    }

    @JsonProperty(value = "TranDate",required = false,defaultValue = "")
    public void setTranDate(String tranDate) {
        this.tranDate = tranDate;
    }

    @JsonProperty(value = "ActionCode",required = false,defaultValue = "")
    public String getActionCode() {
        return actionCode;
    }

    @JsonProperty(value = "ActionCode",required = false,defaultValue = "")
    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    @JsonProperty(value = "ResponseCode",required = false,defaultValue = "")
    public String getResponseCode() {
        return responseCode;
    }

    @JsonProperty(value = "ResponseCode",required = false,defaultValue = "")
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @JsonProperty(value = "RemitMMID",required = false,defaultValue = "")
    public String getRemitMMID() {
        return remitMMID;
    }

    @JsonProperty(value = "RemitMMID",required = false,defaultValue = "")
    public void setRemitMMID(String remitMMID) {
        this.remitMMID = remitMMID;
    }

    @JsonProperty(value = "RemitterName",required = false,defaultValue = "")
    public String getRemitterName() {
        return remitterName;
    }

    @JsonProperty(value = "RemitterName",required = false,defaultValue = "")
    public void setRemitterName(String remitterName) {
        this.remitterName = remitterName;
    }

    @JsonProperty(value = "BenificiaryAccount",required = false,defaultValue = "")
    public String getBenificiaryAccount() {
        return benificiaryAccount;
    }

    @JsonProperty(value = "BenificiaryAccount",required = false,defaultValue = "")
    public void setBenificiaryAccount(String benificiaryAccount) {
        this.benificiaryAccount = benificiaryAccount;
    }

    @JsonProperty(value = "BeneficiaryName",required = false,defaultValue = "")
    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    @JsonProperty(value = "BeneficiaryName",required = false,defaultValue = "")
    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    @JsonProperty(value = "PaymentRefNum",required = false,defaultValue = "")
    public String getPaymentRefNum() {
        return paymentRefNum;
    }

    @JsonProperty(value = "PaymentRefNum",required = false,defaultValue = "")
    public void setPaymentRefNum(String paymentRefNum) {
        this.paymentRefNum = paymentRefNum;
    }

    @JsonProperty(value = "PaymentOrderId",required = false,defaultValue = "")
    public String getPaymentOrderId() {
        return paymentOrderId;
    }

    @JsonProperty(value = "PaymentOrderId",required = false,defaultValue = "")
    public void setPaymentOrderId(String paymentOrderId) {
        this.paymentOrderId = paymentOrderId;
    }


}
