
package com.abipb.linkaccount.payload.fundtransfer.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "AccountId"
})
public class DebitorInfo {

    @JsonProperty(value = "AccountId",required = false,defaultValue = "")
    private String accountId;

    /**
     * No args constructor for use in serialization
     * 
     */
    public DebitorInfo() {
    }

    /**
     * 
     * @param accountId
     */
    public DebitorInfo(String accountId) {
        super();
        this.accountId = accountId;
    }

    @JsonProperty(value = "AccountId",required = false,defaultValue = "")
    public String getAccountId() {
        return accountId;
    }

    @JsonProperty(value = "AccountId",required = false,defaultValue = "")
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }


}
