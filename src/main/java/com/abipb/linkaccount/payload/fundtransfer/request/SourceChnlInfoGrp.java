
package com.abipb.linkaccount.payload.fundtransfer.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "sourceChnl",
    "sourceAppl"
})
public class SourceChnlInfoGrp {

    @JsonProperty(value = "sourceChnl",required = false,defaultValue = "")
    private String sourceChnl;
    @JsonProperty(value = "sourceAppl",required = false,defaultValue = "")
    private String sourceAppl;

    /**
     * No args constructor for use in serialization
     * 
     */
    public SourceChnlInfoGrp() {
    }

    /**
     * 
     * @param sourceChnl
     * @param sourceAppl
     */
    public SourceChnlInfoGrp(String sourceChnl, String sourceAppl) {
        super();
        this.sourceChnl = sourceChnl;
        this.sourceAppl = sourceAppl;
    }

    @JsonProperty(value = "sourceChnl",required = false,defaultValue = "")
    public String getSourceChnl() {
        return sourceChnl;
    }

    @JsonProperty(value = "sourceChnl",required = false,defaultValue = "")
    public void setSourceChnl(String sourceChnl) {
        this.sourceChnl = sourceChnl;
    }

    @JsonProperty(value = "sourceAppl",required = false,defaultValue = "")
    public String getSourceAppl() {
        return sourceAppl;
    }

    @JsonProperty(value = "sourceAppl",required = false,defaultValue = "")
    public void setSourceAppl(String sourceAppl) {
        this.sourceAppl = sourceAppl;
    }

}
