package com.abipb.linkaccount.payload;

import com.abipb.linkaccount.common.payload.ResponseData;

/**
 * Created by pardhasaradhi.k on 10/16/2018.
 */
public class FundsTrnsResp extends ResponseData{

    private String reason;

    private String reasonDesc;

    public String getReasonDesc() {               return reasonDesc;    }

    public void setReasonDesc(String reasonDesc) {        this.reasonDesc = reasonDesc;    }

    public String getReason() {        return reason;    }

    public void setReason(String reason) {        this.reason = reason;    }

    }
