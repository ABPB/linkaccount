package com.abipb.linkaccount.payload.CustomerAccountSummary.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 11/8/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "customerNumber",
        "accountrole",
})
public class RequestData {
    @JsonProperty(value = "customerNumber",required = false,defaultValue = "")
    private String customerNumber;
    @JsonProperty(value = "accountrole",required = false,defaultValue = "")
    private String accountrole;

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getAccountrole() {
        return accountrole;
    }

    public void setAccountrole(String accountrole) {
        this.accountrole = accountrole;
    }
}
