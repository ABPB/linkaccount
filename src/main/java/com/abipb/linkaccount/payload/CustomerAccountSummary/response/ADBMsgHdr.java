package com.abipb.linkaccount.payload.CustomerAccountSummary.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 11/8/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "xmlns",
        "msgIdentifyGrp",
        "userAttrGrp"
})
public class ADBMsgHdr {

    @JsonProperty(value = "xmlns",required = false,defaultValue = "")
    private String xmlns;
  @JsonProperty(value = "ADBMsgHdr",required = false,defaultValue = "")
    private MsgIdentifyGrp msgIdentifyGrp;
  @JsonProperty(value = "userAttrGrp",required = false,defaultValue = "")
    private UserAttrGrp userAttrGrp;

    public String getXmlns() {        return xmlns;    }

    public void setXmlns(String xmlns) {        this.xmlns = xmlns;    }

    public MsgIdentifyGrp getMsgIdentifyGrp() {        return msgIdentifyGrp;    }

    public void setMsgIdentifyGrp(MsgIdentifyGrp msgIdentifyGrp) {        this.msgIdentifyGrp = msgIdentifyGrp;    }

    public UserAttrGrp getUserAttrGrp() {        return userAttrGrp;    }

    public void setUserAttrGrp(UserAttrGrp userAttrGrp) {        this.userAttrGrp = userAttrGrp;    }

}
