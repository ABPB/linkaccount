package com.abipb.linkaccount.payload.CustomerAccountSummary.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 11/8/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "sourceChnl",
        "sourceAppl"
})
public class SourceChnlInfoGrp {
    @JsonProperty(value = "sourceChnl",required = false,defaultValue = "")
    private String sourceChnl;
    @JsonProperty(value = "sourceAppl",required = false,defaultValue = "")
    private String sourceAppl;

    public String getSourceChnl() {
        return sourceChnl;
    }

    public void setSourceChnl(String sourceChnl) {
        this.sourceChnl = sourceChnl;
    }

    public String getSourceAppl() {
        return sourceAppl;
    }

    public void setSourceAppl(String sourceAppl) {
        this.sourceAppl = sourceAppl;
    }
}
