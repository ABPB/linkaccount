package com.abipb.linkaccount.payload.CustomerAccountSummary.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 11/8/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "serviceName",
        "serviceVersionNbr"
})
public class MsgAttrGrp {
    @JsonProperty(value = "serviceName",required = false,defaultValue = "")
    private String serviceName;
    @JsonProperty(value = "serviceVersionNbr",required = false,defaultValue = "")
    private String serviceVersionNbr;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceVersionNbr() {
        return serviceVersionNbr;
    }

    public void setServiceVersionNbr(String serviceVersionNbr) {
        this.serviceVersionNbr = serviceVersionNbr;
    }
}
