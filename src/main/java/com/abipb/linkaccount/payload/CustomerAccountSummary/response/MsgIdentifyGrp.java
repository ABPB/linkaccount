package com.abipb.linkaccount.payload.CustomerAccountSummary.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 11/8/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "applicationTranId",
        "rqstDateTime",
        "sourceChnlInfoGrp",
        "msgAttrGrp"
})
public class MsgIdentifyGrp {
    @JsonProperty(value = "applicationTranId",required = false,defaultValue = "")
    private String applicationTranId;
    @JsonProperty(value = "rqstDateTime",required = false,defaultValue = "")
    private String rqstDateTime;
    @JsonProperty(value = "sourceChnlInfoGrp",required = false,defaultValue = "")
    private SourceChnlInfoGrp sourceChnlInfoGrp;
    @JsonProperty(value = "msgAttrGrp",required = false,defaultValue = "")
    private MsgAttrGrp msgAttrGrp;

    public String getApplicationTranId() {
        return applicationTranId;
    }

    public void setApplicationTranId(String applicationTranId) {
        this.applicationTranId = applicationTranId;
    }

    public String getRqstDateTime() {
        return rqstDateTime;
    }

    public void setRqstDateTime(String rqstDateTime) {
        this.rqstDateTime = rqstDateTime;
    }

    public SourceChnlInfoGrp getSourceChnlInfoGrp() {
        return sourceChnlInfoGrp;
    }

    public void setSourceChnlInfoGrp(SourceChnlInfoGrp sourceChnlInfoGrp) {
        this.sourceChnlInfoGrp = sourceChnlInfoGrp;
    }

    public MsgAttrGrp getMsgAttrGrp() {
        return msgAttrGrp;
    }

    public void setMsgAttrGrp(MsgAttrGrp msgAttrGrp) {
        this.msgAttrGrp = msgAttrGrp;
    }
}
