package com.abipb.linkaccount.payload.CustomerAccountSummary.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 11/8/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "accountNumber",
        "accountType",
        "Accountrole",
        "accTypeIndicator",
        "ifscCode",
        "branchName",
        "accountOpenDate",
        "coi",
        "manufacturerName",
        "productType",
        "productSubtype",
        "BranchId",
        "accountRole",
        "schemCode",
        "bankCardId",
        "bankCardExpiry",
        "MerchantUPIHandle",
        "CustomerUPIHandle",
})
public class Custsummary {
    @JsonProperty(value = "accountNumber",required = false,defaultValue = "")
    private String  accountNumber;
    @JsonProperty(value = "Accountrole",required = false,defaultValue = "")
    private String Accountrole ;
    @JsonProperty(value = "accountType",required = false,defaultValue = "")
    private String accountType ;
    @JsonProperty(value = "accTypeIndicator",required = false,defaultValue = "")
    private String  accTypeIndicator;
    @JsonProperty(value = "ifscCode",required = false,defaultValue = "")
    private String  ifscCode;
    @JsonProperty(value = "branchName",required = false,defaultValue = "")
    private String branchName ;
    @JsonProperty(value = "accountOpenDate",required = false,defaultValue = "")
    private String  accountOpenDate;
    @JsonProperty(value = "coi",required = false,defaultValue = "")
    private String  coi;
    @JsonProperty(value = "manufacturerName",required = false,defaultValue = "")
    private String  manufacturerName;
    @JsonProperty(value = "productType",required = false,defaultValue = "")
    private String  productType;
    @JsonProperty(value = "productSubtype",required = false,defaultValue = "")
    private String  productSubtype;
    @JsonProperty(value = "BranchId",required = false,defaultValue = "")
    private String  BranchId;
    @JsonProperty(value = "accountRole",required = false,defaultValue = "")
    private String  accountRole;
    @JsonProperty(value = "schemCode",required = false,defaultValue = "")
    private String  schemCode;
    @JsonProperty(value = "bankCardId",required = false,defaultValue = "")
    private String bankCardId ;
    @JsonProperty(value = "bankCardExpiry",required = false,defaultValue = "")
    private String  bankCardExpiry;
    @JsonProperty(value = "MerchantUPIHandle",required = false,defaultValue = "")
    private String  MerchantUPIHandle;
    @JsonProperty(value = "CustomerUPIHandle",required = false,defaultValue = "")
    private String  CustomerUPIHandle;

    public String getAccountNumber() {        return accountNumber;    }

    public void setAccountNumber(String accountNumber) {        this.accountNumber = accountNumber;    }

    public String getAccountrole() {        return Accountrole;    }

    public void setAccountrole(String accountrole) {        Accountrole = accountrole;    }

    public String getAccountType() {        return accountType;    }

    public void setAccountType(String accountType) {        this.accountType = accountType;    }

    public String getAccTypeIndicator() {        return accTypeIndicator;    }

    public void setAccTypeIndicator(String accTypeIndicator) {
        this.accTypeIndicator = accTypeIndicator;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getAccountOpenDate() {
        return accountOpenDate;
    }

    public void setAccountOpenDate(String accountOpenDate) {
        this.accountOpenDate = accountOpenDate;
    }

    public String getCoi() {
        return coi;
    }

    public void setCoi(String coi) {
        this.coi = coi;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductSubtype() {
        return productSubtype;
    }

    public void setProductSubtype(String productSubtype) {
        this.productSubtype = productSubtype;
    }

    public String getBranchId() {
        return BranchId;
    }

    public void setBranchId(String branchId) {
        BranchId = branchId;
    }

    public String getAccountRole() {
        return accountRole;
    }

    public void setAccountRole(String accountRole) {
        this.accountRole = accountRole;
    }

    public String getSchemCode() {
        return schemCode;
    }

    public void setSchemCode(String schemCode) {
        this.schemCode = schemCode;
    }

    public String getBankCardId() {
        return bankCardId;
    }

    public void setBankCardId(String bankCardId) {
        this.bankCardId = bankCardId;
    }

    public String getBankCardExpiry() {
        return bankCardExpiry;
    }

    public void setBankCardExpiry(String bankCardExpiry) {
        this.bankCardExpiry = bankCardExpiry;
    }

    public String getMerchantUPIHandle() {
        return MerchantUPIHandle;
    }

    public void setMerchantUPIHandle(String merchantUPIHandle) {
        MerchantUPIHandle = merchantUPIHandle;
    }

    public String getCustomerUPIHandle() {
        return CustomerUPIHandle;
    }

    public void setCustomerUPIHandle(String customerUPIHandle) {
        CustomerUPIHandle = customerUPIHandle;
    }
}
