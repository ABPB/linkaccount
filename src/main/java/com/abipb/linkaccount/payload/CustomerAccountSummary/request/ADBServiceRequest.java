package com.abipb.linkaccount.payload.CustomerAccountSummary.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 11/8/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "xmlns",
        "ADBMsgHdr",
        "RequestData"
})
public class ADBServiceRequest {
    @JsonProperty(value = "ADBServiceRequest",required = false,defaultValue = "")
    private String xmlns;
    @JsonProperty(value = "ADBMsgHdr",required = false,defaultValue = "")
    private ADBMsgHdr ADBMsgHdr;
    @JsonProperty(value = "RequestData",required = false,defaultValue = "")
    private RequestData RequestData;
    public ADBServiceRequest(){}

    public String getXmlns() {
        return xmlns;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    public com.abipb.linkaccount.payload.CustomerAccountSummary.request.ADBMsgHdr getADBMsgHdr() {
        return ADBMsgHdr;
    }

    public void setADBMsgHdr(com.abipb.linkaccount.payload.CustomerAccountSummary.request.ADBMsgHdr ADBMsgHdr) {
        this.ADBMsgHdr = ADBMsgHdr;
    }

    public com.abipb.linkaccount.payload.CustomerAccountSummary.request.RequestData getRequestData() {
        return RequestData;
    }

    public void setRequestData(com.abipb.linkaccount.payload.CustomerAccountSummary.request.RequestData requestData) {
        RequestData = requestData;
    }
}
