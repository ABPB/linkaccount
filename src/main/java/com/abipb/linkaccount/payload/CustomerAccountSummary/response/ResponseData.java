package com.abipb.linkaccount.payload.CustomerAccountSummary.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Collection;

/**
 * Created by pardhasaradhi.k on 11/8/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "CustomerId",
        "SourceID",
        "Salutation",
        "customerName",
        "FirstName",
        "MiddleName",
        "LastName",
        "mobileNumber",
        "PanNumber",
        "DateOfBirth",
        "EmailId",
        "Gender",
        "CustomerRole",
        "CustomerType",
        "IsIdeaMigrated",
        "MotherMaidenName",
        "Aadhar",
        "State",
        "CustomerSubType",
        "IfscCode",
        "BranchID",
        "TransactionalStatus",
        "MotherName",
        "AccountOpenDate",
        "ManufacturerName",
        "Product_BranchID",
        "Product_BranchName",
        "AccountStatus",
        "PartnerBankAccNumber",
        "PartnerBankCIF",
        "PartnerBankName",
        "WorkLandLineNum",
        "OfficeEmailID",
        "CorporateMerchantM",
        "Longitude",
        "Latitude",
        "MerchantUPIHandle",
        "CustomerUPIHandle",
        "IFSC",
        "BankName",
        "BankBranchNameSet",
        "MerchantCatName",
        "EntityMerchantType",
        "POIReferenceNumber",
        "POIDocumentCode",
        "Setholdername",
        "SettlementBankName",
        "SettlementBankbranch",
        "SettlementIFSC",
        "AccountopenedDate",
        "SettlementAccountNo",
        "custsummary"
})
public class ResponseData {
    @JsonProperty(value = "CustomerId",required = false,defaultValue = "")
    private String customerId;
    @JsonProperty(value = "SourceID",required = false,defaultValue = "")
    private String sourceID;
    @JsonProperty(value = "Salutation",required = false,defaultValue = "")
    private String salutation;
    @JsonProperty(value = "customerName",required = false,defaultValue = "")
    private String customerName;
    @JsonProperty(value = "FirstName",required = false,defaultValue = "")
    private String firstName;
    @JsonProperty(value = "MiddleName",required = false,defaultValue = "")
    private String middleName;
    @JsonProperty(value = "LastName",required = false,defaultValue = "")
    private String lastName;
    @JsonProperty(value = "mobileNumber",required = false,defaultValue = "")
    private String mobileNumber;
    @JsonProperty(value = "PanNumber",required = false,defaultValue = "")
    private String panNumber;
    @JsonProperty(value = "DateOfBirth",required = false,defaultValue = "")
    private String dateOfBirth;
    @JsonProperty(value = "EmailId",required = false,defaultValue = "")
    private String emailId;
    @JsonProperty(value = "Gender",required = false,defaultValue = "")
    private String gender;
    @JsonProperty(value = "CustomerRole",required = false,defaultValue = "")
    private String customerRole;
    @JsonProperty(value = "CustomerType",required = false,defaultValue = "")
    private String customerType;
    @JsonProperty(value = "IsIdeaMigrated",required = false,defaultValue = "")
    private String isIdeaMigrated;
    @JsonProperty(value = "MotherMaidenName",required = false,defaultValue = "")
    private String motherMaidenName;
    @JsonProperty(value = "Aadhar",required = false,defaultValue = "")
    private String aadhar;
    @JsonProperty(value = "State",required = false,defaultValue = "")
    private String state;
    @JsonProperty(value = "CustomerSubType",required = false,defaultValue = "")
    private String customerSubType;
    @JsonProperty(value = "IfscCode",required = false,defaultValue = "")
    private String ifscCode;
    @JsonProperty(value = "BranchID",required = false,defaultValue = "")
    private String branchID;
    @JsonProperty(value = "TransactionalStatus",required = false,defaultValue = "")
    private String transactionalStatus;
    @JsonProperty(value = "MotherName",required = false,defaultValue = "")
    private String motherName;
    @JsonProperty(value = "AccountOpenDate",required = false,defaultValue = "")
    private String accountOpenDate;
    @JsonProperty(value = "ManufacturerName",required = false,defaultValue = "")
    private String manufacturerName;
    @JsonProperty(value = "Product_BranchID",required = false,defaultValue = "")
    private String productBranchID;
    @JsonProperty(value = "Product_BranchName",required = false,defaultValue = "")
    private String productBranchName;
    @JsonProperty(value = "AccountStatus",required = false,defaultValue = "")
    private String accountStatus;
    @JsonProperty(value = "PartnerBankAccNumber",required = false,defaultValue = "")
    private String partnerBankAccNumber;
    @JsonProperty(value = "PartnerBankCIF",required = false,defaultValue = "")
    private String partnerBankCIF;
    @JsonProperty(value = "PartnerBankName",required = false,defaultValue = "")
    private String partnerBankName;
    @JsonProperty(value = "WorkLandLineNum",required = false,defaultValue = "")
    private String workLandLineNum;
    @JsonProperty(value = "OfficeEmailID",required = false,defaultValue = "")
    private String officeEmailID;
    @JsonProperty(value = "CorporateMerchantM",required = false,defaultValue = "")
    private String corporateMerchantM;
    @JsonProperty(value = "Longitude",required = false,defaultValue = "")
    private String longitude;
    @JsonProperty(value = "Latitude",required = false,defaultValue = "")
    private String latitude;
    @JsonProperty(value = "MerchantUPIHandle",required = false,defaultValue = "")
    private String merchantUPIHandle;
    @JsonProperty(value = "CustomerUPIHandle",required = false,defaultValue = "")
    private String customerUPIHandle;
    @JsonProperty(value = "IFSC",required = false,defaultValue = "")
    private String iFSC;
    @JsonProperty(value = "BankName",required = false,defaultValue = "")
    private String bankName;
    @JsonProperty(value = "BankBranchNameSet",required = false,defaultValue = "")
    private String bankBranchNameSet;
    @JsonProperty(value = "MerchantCatName",required = false,defaultValue = "")
    private String merchantCatName;
    @JsonProperty(value = "EntityMerchantType",required = false,defaultValue = "")
    private String entityMerchantType;
    @JsonProperty(value = "POIReferenceNumber",required = false,defaultValue = "")
    private String pOIReferenceNumber;
    @JsonProperty(value = "POIDocumentCode",required = false,defaultValue = "")
    private String pOIDocumentCode;
    @JsonProperty(value = "Setholdername",required = false,defaultValue = "")
    private String setholdername;
    @JsonProperty(value = "SettlementBankName",required = false,defaultValue = "")
    private String settlementBankName;
    @JsonProperty(value = "SettlementBankbranch",required = false,defaultValue = "")
    private String settlementBankbranch;
    @JsonProperty(value = "SettlementIFSC",required = false,defaultValue = "")
    private String settlementIFSC;
    @JsonProperty(value = "AccountopenedDate",required = false,defaultValue = "")
    private String accountopenedDate;
    @JsonProperty(value = "SettlementAccountNo",required = false,defaultValue = "")
    private String settlementAccountNo;
    @JsonProperty(value = "custsummary",required = false,defaultValue = "")
    private Collection<Custsummary> custsummary;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getSourceID() {
        return sourceID;
    }

    public void setSourceID(String sourceID) {
        this.sourceID = sourceID;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCustomerRole() {
        return customerRole;
    }

    public void setCustomerRole(String customerRole) {
        this.customerRole = customerRole;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getIsIdeaMigrated() {
        return isIdeaMigrated;
    }

    public void setIsIdeaMigrated(String isIdeaMigrated) {
        this.isIdeaMigrated = isIdeaMigrated;
    }

    public String getMotherMaidenName() {
        return motherMaidenName;
    }

    public void setMotherMaidenName(String motherMaidenName) {
        this.motherMaidenName = motherMaidenName;
    }

    public String getAadhar() {
        return aadhar;
    }

    public void setAadhar(String aadhar) {
        this.aadhar = aadhar;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCustomerSubType() {
        return customerSubType;
    }

    public void setCustomerSubType(String customerSubType) {
        this.customerSubType = customerSubType;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getBranchID() {
        return branchID;
    }

    public void setBranchID(String branchID) {
        this.branchID = branchID;
    }

    public String getTransactionalStatus() {
        return transactionalStatus;
    }

    public void setTransactionalStatus(String transactionalStatus) {
        this.transactionalStatus = transactionalStatus;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getAccountOpenDate() {
        return accountOpenDate;
    }

    public void setAccountOpenDate(String accountOpenDate) {
        this.accountOpenDate = accountOpenDate;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getProductBranchID() {
        return productBranchID;
    }

    public void setProductBranchID(String productBranchID) {
        this.productBranchID = productBranchID;
    }

    public String getProductBranchName() {
        return productBranchName;
    }

    public void setProductBranchName(String productBranchName) {
        this.productBranchName = productBranchName;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getPartnerBankAccNumber() {
        return partnerBankAccNumber;
    }

    public void setPartnerBankAccNumber(String partnerBankAccNumber) {
        this.partnerBankAccNumber = partnerBankAccNumber;
    }

    public String getPartnerBankCIF() {
        return partnerBankCIF;
    }

    public void setPartnerBankCIF(String partnerBankCIF) {
        this.partnerBankCIF = partnerBankCIF;
    }

    public String getPartnerBankName() {
        return partnerBankName;
    }

    public void setPartnerBankName(String partnerBankName) {
        this.partnerBankName = partnerBankName;
    }

    public String getWorkLandLineNum() {
        return workLandLineNum;
    }

    public void setWorkLandLineNum(String workLandLineNum) {
        this.workLandLineNum = workLandLineNum;
    }

    public String getOfficeEmailID() {
        return officeEmailID;
    }

    public void setOfficeEmailID(String officeEmailID) {
        this.officeEmailID = officeEmailID;
    }

    public String getCorporateMerchantM() {
        return corporateMerchantM;
    }

    public void setCorporateMerchantM(String corporateMerchantM) {
        this.corporateMerchantM = corporateMerchantM;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getMerchantUPIHandle() {
        return merchantUPIHandle;
    }

    public void setMerchantUPIHandle(String merchantUPIHandle) {
        this.merchantUPIHandle = merchantUPIHandle;
    }

    public String getCustomerUPIHandle() {
        return customerUPIHandle;
    }

    public void setCustomerUPIHandle(String customerUPIHandle) {
        this.customerUPIHandle = customerUPIHandle;
    }

    public String getiFSC() {
        return iFSC;
    }

    public void setiFSC(String iFSC) {
        this.iFSC = iFSC;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankBranchNameSet() {
        return bankBranchNameSet;
    }

    public void setBankBranchNameSet(String bankBranchNameSet) {
        this.bankBranchNameSet = bankBranchNameSet;
    }

    public String getMerchantCatName() {
        return merchantCatName;
    }

    public void setMerchantCatName(String merchantCatName) {
        this.merchantCatName = merchantCatName;
    }

    public String getEntityMerchantType() {
        return entityMerchantType;
    }

    public void setEntityMerchantType(String entityMerchantType) {
        this.entityMerchantType = entityMerchantType;
    }

    public String getpOIReferenceNumber() {
        return pOIReferenceNumber;
    }

    public void setpOIReferenceNumber(String pOIReferenceNumber) {
        this.pOIReferenceNumber = pOIReferenceNumber;
    }

    public String getpOIDocumentCode() {
        return pOIDocumentCode;
    }

    public void setpOIDocumentCode(String pOIDocumentCode) {
        this.pOIDocumentCode = pOIDocumentCode;
    }

    public String getSetholdername() {
        return setholdername;
    }

    public void setSetholdername(String setholdername) {
        this.setholdername = setholdername;
    }

    public String getSettlementBankName() {
        return settlementBankName;
    }

    public void setSettlementBankName(String settlementBankName) {
        this.settlementBankName = settlementBankName;
    }

    public String getSettlementBankbranch() {
        return settlementBankbranch;
    }

    public void setSettlementBankbranch(String settlementBankbranch) {
        this.settlementBankbranch = settlementBankbranch;
    }

    public String getSettlementIFSC() {
        return settlementIFSC;
    }

    public void setSettlementIFSC(String settlementIFSC) {
        this.settlementIFSC = settlementIFSC;
    }

    public String getAccountopenedDate() {
        return accountopenedDate;
    }

    public void setAccountopenedDate(String accountopenedDate) {
        this.accountopenedDate = accountopenedDate;
    }

    public String getSettlementAccountNo() {
        return settlementAccountNo;
    }

    public void setSettlementAccountNo(String settlementAccountNo) {
        this.settlementAccountNo = settlementAccountNo;
    }

    public Collection<Custsummary> getCustsummary() {
        return custsummary;
    }

    public void setCustsummary(Collection<Custsummary> custsummary) {
        this.custsummary = custsummary;
    }
}
