package com.abipb.linkaccount.payload.CustomerAccountSummary.response;

/**
 * Created by pardhasaradhi.k on 11/8/2018.
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "reasonCode",
        "reasonDesc"
})
public class Status {
    @JsonProperty(value = "reasonCode",required = false,defaultValue = "")
    private String reasonCode;
    @JsonProperty(value = "reasonDesc",required = false,defaultValue = "")
    private String reasonDesc;

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonDesc() {
        return reasonDesc;
    }

    public void setReasonDesc(String reasonDesc) {
        this.reasonDesc = reasonDesc;
    }
}
