package com.abipb.linkaccount.payload.CustomerAccountSummary.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 11/8/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "ADBServiceRequest"
})
public class CustomerAccountSummaryRequest {

    @JsonProperty(value = "ADBServiceRequest",required = false,defaultValue = "")
    private ADBServiceRequest aDBServiceRequest;

    @JsonProperty(value = "ADBServiceRequest",required = false,defaultValue = "")
    public ADBServiceRequest getADBServiceRequest() {
        return aDBServiceRequest;
    }

    @JsonProperty(value = "ADBServiceRequest",required = false,defaultValue = "")
    public void setADBServiceRequest(ADBServiceRequest aDBServiceRequest) {
        this.aDBServiceRequest = aDBServiceRequest;
    }
}
