package com.abipb.linkaccount.payload.CustomerAccountSummary.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 11/8/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder( {
        "token",
        "customerNumber",
        "accountrole",
})
public class CustomerAccountSummaryRequestApp {
    @JsonProperty(value = "token",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String token;
    @JsonProperty(value = "customerNumber",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String customerNumber;
    @JsonProperty(value = "accountrole",required = false, index = -1,access = JsonProperty.Access.AUTO)
    private String accountrole;


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAccountrole() {
        return accountrole;
    }

    public void setAccountrole(String accountrole) {
        this.accountrole = accountrole;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }
}
