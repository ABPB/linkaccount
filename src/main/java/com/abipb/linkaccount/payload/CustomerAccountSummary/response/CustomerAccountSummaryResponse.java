package com.abipb.linkaccount.payload.CustomerAccountSummary.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 11/8/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "ADBServiceResponse"
})
public class CustomerAccountSummaryResponse {
    @JsonProperty(value = "ADBServiceResponse",required = false,defaultValue = "")
    private com.abipb.linkaccount.payload.CustomerAccountSummary.response.ADBServiceResponse adbServiceResponse;

    public ADBServiceResponse getAdbServiceResponse() {
        return adbServiceResponse;
    }

    public void setAdbServiceResponse(ADBServiceResponse adbServiceResponse) {
        this.adbServiceResponse = adbServiceResponse;
    }
}
