package com.abipb.linkaccount.payload.CustomerAccountSummary.request;

import com.abipb.linkaccount.payload.CustomerAccountSummary.request.StaffId;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by pardhasaradhi.k on 11/8/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
        "channelId",
        "staffId",
        "deviceId"
})
public class UserAttrGrp {
    @JsonProperty(value = "channelId",required = false,defaultValue = "")
    private String channelId;
    @JsonProperty(value = "staffId",required = false,defaultValue = "")
    private  StaffId staffId;
    @JsonProperty(value = "deviceId",required = false,defaultValue = "")
    private String deviceId;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public StaffId getStaffId() {
        return staffId;
    }

    public void setStaffId(StaffId staffId) {
        this.staffId = staffId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
