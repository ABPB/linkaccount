package com.abipb.linkaccount.payload.loginmpin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by Rajasekhar.E-V on 02-11-2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder( {
        "accountNumber",
        "balance"

})
public class Accounts {
    @JsonProperty(value = "accountNumber",defaultValue="", required = false)
    private String accountNumber;
    @JsonProperty(value = "balanceAmount",defaultValue="", required = false)
    private String balanceAmount;
    @JsonProperty(value = "accountType",defaultValue="", required = false)
    private String accountType;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }
}
