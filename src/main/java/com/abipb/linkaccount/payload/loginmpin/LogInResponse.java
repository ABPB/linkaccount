package com.abipb.linkaccount.payload.loginmpin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by pardhasaradhi.k on 10/31/2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder( {
        "statusCode",
        "status",
        "token",
        "accounts",
        "customerId"
})
public class LogInResponse {
    @JsonProperty(value = "statusCode",defaultValue="", required = false)
    private String statusCode;
    @JsonProperty(value = "status",defaultValue="", required = false)
    private String  status;
    @JsonProperty(value = "balanceAmount",defaultValue="", required = false)
    private String balnceAmount;
    @JsonProperty(value = "token",defaultValue="", required = false)
    private String token;
    /*@JsonProperty(value = "accountId",defaultValue="", required = false)
    private String accountId;
    @JsonProperty(value = "walletId",defaultValue="", required = false)
    private String walletId;*/
    @JsonProperty(value = "customerId",defaultValue="", required = false)
    private String customerId;
    @JsonProperty(value = "accounts",defaultValue="", required = false)
    private Collection<Accounts> accounts;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBalnceAmount() {
        return balnceAmount;
    }

    public void setBalnceAmount(String balnceAmount) {
        this.balnceAmount = balnceAmount;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

  /*  public String getAccountId() {
        return accountId;
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }*/

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Collection<Accounts> getAccounts() {
        return accounts;
    }

    public void setAccounts(Collection<Accounts> accounts) {
        this.accounts = accounts;
    }
}
