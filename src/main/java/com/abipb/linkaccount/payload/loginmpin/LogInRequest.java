package com.abipb.linkaccount.payload.loginmpin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by Rajasekhar.E-V on 31-10-2018.
 */
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder( {
        "userPrinciple",
        "accessCode",
        "ipAddress",
        "token",
        "accountNo"
})
public class LogInRequest {
    @JsonProperty(value = "userPrinciple",defaultValue="", required = false)
    private String userPrinciple;
    /*@JsonProperty(value = "cropPrinciple",defaultValue="", required = false)
    private String cropPrinciple;*/
    @JsonProperty(value = "accessCode",defaultValue="", required = false)
    private String accessCode;
    @JsonProperty(value = "ipAddress",defaultValue="", required = false)
    private String ipAddress;
    @JsonProperty(value = "token",defaultValue="", required = false)
    private String token;
    @JsonProperty(value = "accountNo",defaultValue="", required = false)
    private String accountNo;

    public String getUserPrinciple() {
        return userPrinciple;
    }

    public void setUserPrinciple(String userPrinciple) {
        this.userPrinciple = userPrinciple;
    }

   /* public String getCropPrinciple() {
        return cropPrinciple;
    }

    public void setCropPrinciple(String cropPrinciple) {
        this.cropPrinciple = cropPrinciple;
    }*/

    public String getAccessCode() {
        return accessCode;
    }

    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }
}
