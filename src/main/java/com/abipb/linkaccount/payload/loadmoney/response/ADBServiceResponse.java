
package com.abipb.linkaccount.payload.loadmoney.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "Status"
})
public class ADBServiceResponse {

    @JsonProperty(value = "Status",required = false,defaultValue = "")
    private Status status;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ADBServiceResponse() {
    }

    /**
     * 
     * @param status
     */
    public ADBServiceResponse(Status status) {
        super();
        this.status = status;
    }

    @JsonProperty(value = "Status",required = false,defaultValue = "")
    public Status getStatus() {
        return status;
    }

    @JsonProperty(value = "Status",required = false,defaultValue = "")
    public void setStatus(Status status) {
        this.status = status;
    }


}
