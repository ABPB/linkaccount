
package com.abipb.linkaccount.payload.loadmoney.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "loadOrWithdrawMoney"
})
public class LoadMoneyRequest {

    @JsonProperty(value = "loadOrWithdrawMoney",required = false,defaultValue = "")
    private LoadOrWithdrawMoney loadOrWithdrawMoney;

    /**
     * No args constructor for use in serialization
     * 
     */
    public LoadMoneyRequest() {
    }

    /**
     * 
     * @param loadOrWithdrawMoney
     */
    public LoadMoneyRequest(LoadOrWithdrawMoney loadOrWithdrawMoney) {
        super();
        this.loadOrWithdrawMoney = loadOrWithdrawMoney;
    }

    @JsonProperty(value = "loadOrWithdrawMoney",required = false,defaultValue = "")
    public LoadOrWithdrawMoney getLoadOrWithdrawMoney() {
        return loadOrWithdrawMoney;
    }

    @JsonProperty(value = "loadOrWithdrawMoney",required = false,defaultValue = "")
    public void setLoadOrWithdrawMoney(LoadOrWithdrawMoney loadOrWithdrawMoney) {
        this.loadOrWithdrawMoney = loadOrWithdrawMoney;
    }

}
