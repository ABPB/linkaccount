
package com.abipb.linkaccount.payload.loadmoney.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "channelID",
    "requestId",
    "entityTypeId",
    "entityId",
    "MSISDN"
})
public class CommonServiceData {

    @JsonProperty(value = "channelID",required = false,defaultValue = "")
    private String channelID;
    @JsonProperty(value = "requestId",required = false,defaultValue = "")
    private String requestId;
    @JsonProperty(value = "entityTypeId",required = false,defaultValue = "")
    private String entityTypeId;
    @JsonProperty(value = "entityId",required = false,defaultValue = "")
    private EntityId entityId;
    @JsonProperty(value = "MSISDN",required = false,defaultValue = "")
    private String mSISDN;

    /**
     * No args constructor for use in serialization
     * 
     */
    public CommonServiceData() {
    }

    /**
     * 
     * @param requestId
     * @param mSISDN
     * @param entityTypeId
     * @param channelID
     * @param entityId
     */
    public CommonServiceData(String channelID, String requestId, String entityTypeId, EntityId entityId, String mSISDN) {
        super();
        this.channelID = channelID;
        this.requestId = requestId;
        this.entityTypeId = entityTypeId;
        this.entityId = entityId;
        this.mSISDN = mSISDN;
    }

    @JsonProperty(value = "channelID",required = false,defaultValue = "")
    public String getChannelID() {
        return channelID;
    }

    @JsonProperty(value = "channelID",required = false,defaultValue = "")
    public void setChannelID(String channelID) {
        this.channelID = channelID;
    }

    @JsonProperty(value = "requestId",required = false,defaultValue = "")
    public String getRequestId() {
        return requestId;
    }

    @JsonProperty(value = "requestId",required = false,defaultValue = "")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @JsonProperty(value = "entityTypeId",required = false,defaultValue = "")
    public String getEntityTypeId() {
        return entityTypeId;
    }

    @JsonProperty(value = "entityTypeId",required = false,defaultValue = "")
    public void setEntityTypeId(String entityTypeId) {
        this.entityTypeId = entityTypeId;
    }

    @JsonProperty(value = "entityId",required = false,defaultValue = "")
    public EntityId getEntityId() {
        return entityId;
    }

    @JsonProperty(value = "entityId",required = false,defaultValue = "")
    public void setEntityId(EntityId entityId) {
        this.entityId = entityId;
    }

    @JsonProperty(value = "MSISDN",required = false,defaultValue = "")
    public String getMSISDN() {
        return mSISDN;
    }

    @JsonProperty(value = "MSISDN",required = false,defaultValue = "")
    public void setMSISDN(String mSISDN) {
        this.mSISDN = mSISDN;
    }


}
