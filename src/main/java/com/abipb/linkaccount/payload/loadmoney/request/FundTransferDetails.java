
package com.abipb.linkaccount.payload.loadmoney.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "enterpriseId",
    "customerMSISDN",
    "customermPin",
    "amount",
    "agentMSISDN",
    "agentmPin",
    "transactionType",
    "instance",
    "paymentInstrument",
    "ideamoneyTransactionID"
})
public class FundTransferDetails {

    @JsonProperty(value = "enterpriseId",required = false,defaultValue = "")
    private String enterpriseId;
    @JsonProperty(value = "customerMSISDN",required = false,defaultValue = "")
    private String customerMSISDN;
    @JsonProperty(value = "customermPin",required = false,defaultValue = "")
    private CustomermPin customermPin;
    @JsonProperty(value = "amount",required = false,defaultValue = "")
    private String amount;
    @JsonProperty(value = "agentMSISDN",required = false,defaultValue = "")
    private AgentMSISDN agentMSISDN;
    @JsonProperty(value = "agentmPin",required = false,defaultValue = "")
    private AgentmPin agentmPin;
    @JsonProperty(value = "transactionType",required = false,defaultValue = "")
    private String transactionType;
    @JsonProperty(value = "instance",required = false,defaultValue = "")
    private String instance;
    @JsonProperty(value = "paymentInstrument",required = false,defaultValue = "")
    private String paymentInstrument;
    @JsonProperty(value = "ideamoneyTransactionID",required = false,defaultValue = "")
    private String ideamoneyTransactionID;

    /**
     * No args constructor for use in serialization
     * 
     */
    public FundTransferDetails() {
    }

    /**
     * 
     * @param amount
     * @param transactionType
     * @param agentmPin
     * @param enterpriseId
     * @param paymentInstrument
     * @param instance
     * @param customerMSISDN
     * @param agentMSISDN
     * @param ideamoneyTransactionID
     * @param customermPin
     */
    public FundTransferDetails(String enterpriseId, String customerMSISDN, CustomermPin customermPin, String amount, AgentMSISDN agentMSISDN, AgentmPin agentmPin, String transactionType, String instance, String paymentInstrument, String ideamoneyTransactionID) {
        super();
        this.enterpriseId = enterpriseId;
        this.customerMSISDN = customerMSISDN;
        this.customermPin = customermPin;
        this.amount = amount;
        this.agentMSISDN = agentMSISDN;
        this.agentmPin = agentmPin;
        this.transactionType = transactionType;
        this.instance = instance;
        this.paymentInstrument = paymentInstrument;
        this.ideamoneyTransactionID = ideamoneyTransactionID;
    }

    @JsonProperty(value = "enterpriseId",required = false,defaultValue = "")
    public String getEnterpriseId() {
        return enterpriseId;
    }

    @JsonProperty(value = "enterpriseId",required = false,defaultValue = "")
    public void setEnterpriseId(String enterpriseId) {
        this.enterpriseId = enterpriseId;
    }

    @JsonProperty(value = "customerMSISDN",required = false,defaultValue = "")
    public String getCustomerMSISDN() {
        return customerMSISDN;
    }

    @JsonProperty(value = "customerMSISDN",required = false,defaultValue = "")
    public void setCustomerMSISDN(String customerMSISDN) {
        this.customerMSISDN = customerMSISDN;
    }

    @JsonProperty(value = "customermPin",required = false,defaultValue = "")
    public CustomermPin getCustomermPin() {
        return customermPin;
    }

    @JsonProperty(value = "customermPin",required = false,defaultValue = "")
    public void setCustomermPin(CustomermPin customermPin) {
        this.customermPin = customermPin;
    }

    @JsonProperty(value = "amount",required = false,defaultValue = "")
    public String getAmount() {
        return amount;
    }

    @JsonProperty(value = "amount",required = false,defaultValue = "")
    public void setAmount(String amount) {
        this.amount = amount;
    }

    @JsonProperty(value = "agentMSISDN",required = false,defaultValue = "")
    public AgentMSISDN getAgentMSISDN() {
        return agentMSISDN;
    }

    @JsonProperty(value = "agentMSISDN",required = false,defaultValue = "")
    public void setAgentMSISDN(AgentMSISDN agentMSISDN) {
        this.agentMSISDN = agentMSISDN;
    }

    @JsonProperty(value = "agentmPin",required = false,defaultValue = "")
    public AgentmPin getAgentmPin() {
        return agentmPin;
    }

    @JsonProperty(value = "agentmPin",required = false,defaultValue = "")
    public void setAgentmPin(AgentmPin agentmPin) {
        this.agentmPin = agentmPin;
    }

    @JsonProperty(value = "transactionType",required = false,defaultValue = "")
    public String getTransactionType() {
        return transactionType;
    }

    @JsonProperty(value = "transactionType",required = false,defaultValue = "")
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    @JsonProperty(value = "instance",required = false,defaultValue = "")
    public String getInstance() {
        return instance;
    }

    @JsonProperty(value = "instance",required = false,defaultValue = "")
    public void setInstance(String instance) {
        this.instance = instance;
    }

    @JsonProperty(value = "paymentInstrument",required = false,defaultValue = "")
    public String getPaymentInstrument() {
        return paymentInstrument;
    }

    @JsonProperty(value = "paymentInstrument",required = false,defaultValue = "")
    public void setPaymentInstrument(String paymentInstrument) {
        this.paymentInstrument = paymentInstrument;
    }

    @JsonProperty(value = "ideamoneyTransactionID",required = false,defaultValue = "")
    public String getIdeamoneyTransactionID() {
        return ideamoneyTransactionID;
    }

    @JsonProperty(value = "ideamoneyTransactionID",required = false,defaultValue = "")
    public void setIdeamoneyTransactionID(String ideamoneyTransactionID) {
        this.ideamoneyTransactionID = ideamoneyTransactionID;
    }


}
