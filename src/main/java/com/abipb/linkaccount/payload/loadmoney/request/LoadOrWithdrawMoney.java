
package com.abipb.linkaccount.payload.loadmoney.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "commonServiceData",
    "fundTransferDetails"
})
public class LoadOrWithdrawMoney {

    @JsonProperty(value = "commonServiceData",required = false,defaultValue = "")
    private CommonServiceData commonServiceData;
    @JsonProperty(value = "fundTransferDetails",required = false,defaultValue = "")
    private FundTransferDetails fundTransferDetails;

    /**
     * No args constructor for use in serialization
     * 
     */
    public LoadOrWithdrawMoney() {
    }

    /**
     * 
     * @param commonServiceData
     * @param fundTransferDetails
     */
    public LoadOrWithdrawMoney(CommonServiceData commonServiceData, FundTransferDetails fundTransferDetails) {
        super();
        this.commonServiceData = commonServiceData;
        this.fundTransferDetails = fundTransferDetails;
    }

    @JsonProperty(value = "commonServiceData",required = false,defaultValue = "")
    public CommonServiceData getCommonServiceData() {
        return commonServiceData;
    }

    @JsonProperty(value = "commonServiceData",required = false,defaultValue = "")
    public void setCommonServiceData(CommonServiceData commonServiceData) {
        this.commonServiceData = commonServiceData;
    }

    @JsonProperty(value = "fundTransferDetails",required = false,defaultValue = "")
    public FundTransferDetails getFundTransferDetails() {
        return fundTransferDetails;
    }

    @JsonProperty(value = "fundTransferDetails",required = false,defaultValue = "")
    public void setFundTransferDetails(FundTransferDetails fundTransferDetails) {
        this.fundTransferDetails = fundTransferDetails;
    }

}
