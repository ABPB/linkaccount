
package com.abipb.linkaccount.payload.loadmoney.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "reasonCode",
    "reasonDesc"
})
public class Status {

    @JsonProperty(value = "reasonCode",required = false,defaultValue = "")
    private String reasonCode;
    @JsonProperty(value = "reasonDesc",required = false,defaultValue = "")
    private String reasonDesc;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Status() {
    }

    /**
     * 
     * @param reasonCode
     * @param reasonDesc
     */
    public Status(String reasonCode, String reasonDesc) {
        super();
        this.reasonCode = reasonCode;
        this.reasonDesc = reasonDesc;
    }

    @JsonProperty(value = "reasonCode",required = false,defaultValue = "")
    public String getReasonCode() {
        return reasonCode;
    }

    @JsonProperty(value = "reasonCode",required = false,defaultValue = "")
    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    @JsonProperty(value = "reasonDesc",required = false,defaultValue = "")
    public String getReasonDesc() {
        return reasonDesc;
    }

    @JsonProperty(value = "reasonDesc",required = false,defaultValue = "")
    public void setReasonDesc(String reasonDesc) {
        this.reasonDesc = reasonDesc;
    }


}
