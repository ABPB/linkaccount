
package com.abipb.linkaccount.payload.loadmoney.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@JsonPropertyOrder({
    "ADBServiceResponse"
})
public class LoadMoneyResponse {

    @JsonProperty(value = "ADBServiceResponse",required = false,defaultValue = "")
    private ADBServiceResponse aDBServiceResponse;

    /**
     * No args constructor for use in serialization
     * 
     */
    public LoadMoneyResponse() {
    }

    /**
     * 
     * @param aDBServiceResponse
     */
    public LoadMoneyResponse(ADBServiceResponse aDBServiceResponse) {
        super();
        this.aDBServiceResponse = aDBServiceResponse;
    }

    @JsonProperty(value = "ADBServiceResponse",required = false,defaultValue = "")
    public ADBServiceResponse getADBServiceResponse() {
        return aDBServiceResponse;
    }

    @JsonProperty(value = "ADBServiceResponse",required = false,defaultValue = "")
    public void setADBServiceResponse(ADBServiceResponse aDBServiceResponse) {
        this.aDBServiceResponse = aDBServiceResponse;
    }

}
