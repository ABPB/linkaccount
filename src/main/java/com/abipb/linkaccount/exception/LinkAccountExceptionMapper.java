package com.abipb.linkaccount.exception;

import com.abipb.linkaccount.common.payload.ADBServiceResponse;
import com.abipb.linkaccount.common.payload.Error;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by abhay.kumar on 9/25/2018.
 */
@Provider
public class LinkAccountExceptionMapper implements ExceptionMapper<LinkAccountMiddlewareException> {

    private static final Logger LOGGER= LoggerFactory.getLogger(LinkAccountExceptionMapper.class);
    @Override
    public Response toResponse(LinkAccountMiddlewareException exception) {

        return Response.status(exception.getResponse().getStatus())
                .entity(this.getJerseyError(exception))
                .type(MediaType.APPLICATION_JSON).
                        build();
    }

    public ADBServiceResponse getJerseyError(LinkAccountMiddlewareException middlewareException){
        Error error=new Error();
        ADBServiceResponse adbServiceResponse=StringUtils.isEmpty(middlewareException.getAdbServiceResponseError())?new ADBServiceResponse():middlewareException.getAdbServiceResponseError();


        error.setResonDescription(StringUtils.isEmpty(middlewareException.getCause())?"":middlewareException.getCause().toString());
        error.setResonCode(String.valueOf(middlewareException.getResponse().getStatus()));
        error.setResonDescription(StringUtils.isEmpty(middlewareException.getMessage())?"":middlewareException.getMessage());
        LOGGER.error("MPesa Middleware Error ::"+ error);
        adbServiceResponse.setError(error);
        return adbServiceResponse;
    }
}
