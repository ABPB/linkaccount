package com.abipb.linkaccount.exception;

import com.abipb.linkaccount.common.payload.ADBServiceResponse;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 * {@code LinkAccountMiddlewareException} is the superclass of those
 * exceptions that can be thrown during the normal operation of the
 * Java Virtual Machine.
 *
 * <p>{@code LinkAccountMiddlewareException} and its subclasses are <em>WebApplicationException
 * exceptions</em>.  Unchecked exceptions do <em>not</em> need to be
 * declared in a method or constructor's {@code throws} clause if they
 * can be thrown by the execution of the method or constructor and
 * propagate outside the method or constructor boundary.
 *
 * @author  abhay.kumar
 * @jls 11.2 Compile-Time Checking of Exceptions
 * @since   1.0
 * */

public class LinkAccountMiddlewareException extends WebApplicationException {
    transient ADBServiceResponse adbServiceResponseError=new ADBServiceResponse();

    public LinkAccountMiddlewareException() {
        super();
    }

    public LinkAccountMiddlewareException(String message) {
        super(message);
    }

    public LinkAccountMiddlewareException(Response response) {
        super(response);
    }

    public LinkAccountMiddlewareException(String message, Response response) {
        super(message, response);
    }

    public LinkAccountMiddlewareException(int status) {
        super(status);
    }

    public LinkAccountMiddlewareException(String message, int status) {
        super(message, status);
    }

    public LinkAccountMiddlewareException(Response.Status status) {
        super(status);
    }

    public LinkAccountMiddlewareException(String message, Response.Status status) {
        super(message, status);
    }

    public LinkAccountMiddlewareException(Throwable cause) {
        super(cause);
    }

    public LinkAccountMiddlewareException(String message, Throwable cause) {
        super(message, cause);
    }

    public LinkAccountMiddlewareException(Throwable cause, Response response) {
        super(cause, response);
    }

    public LinkAccountMiddlewareException(String message, Throwable cause, Response response) {
        super(message, cause, response);
    }

    public LinkAccountMiddlewareException(Throwable cause, int status) {
        super(cause, status);
    }

    public LinkAccountMiddlewareException(String message, Throwable cause, int status) {
        super(message, cause, status);
    }

    public LinkAccountMiddlewareException(Throwable cause, Response.Status status) throws IllegalArgumentException {
        super(cause, status);
    }

    public LinkAccountMiddlewareException(String message, Throwable cause, Response.Status status) throws IllegalArgumentException {
        super(message, cause, status);
    }

    @Override
    public Response getResponse() {
        return super.getResponse();
    }

    public ADBServiceResponse getAdbServiceResponseError() {
        return adbServiceResponseError;
    }

    public void setAdbServiceResponseError(ADBServiceResponse adbServiceResponseError) {
        this.adbServiceResponseError = adbServiceResponseError;
    }
}
