package com.abipb.linkaccount.repository.impl;

import com.abipb.linkaccount.exception.LinkAccountDataBaseException;
import com.abipb.linkaccount.payload.LocateAgentDetil;
import com.abipb.linkaccount.repository.LocateAgentRepository;
import com.abipb.linkaccount.utils.MPesaPropertyValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by abhay.kumar on 9/24/2018.
 */
@Repository
public class LocateAgentRepositoryImpl implements LocateAgentRepository {

    private static final Logger LOGGER= LoggerFactory.getLogger(LocateAgentRepositoryImpl.class);

    @Override
    public List<LocateAgentDetil> fetchLocateAgent(LocateAgentDetil locateAgentDetil) throws LinkAccountDataBaseException {
        List<LocateAgentDetil> locateAgentDetils=null;
        try {
            if(StringUtils.isEmpty(locateAgentDetil)){
                LOGGER.debug(MPesaPropertyValue.getValue("resource.locateAgent.MP0001"));
                return Arrays.asList();
            }
            locateAgentDetils=new LinkedList<>();
            locateAgentDetils.add(locateAgentDetil);
            locateAgentDetils.add(locateAgentDetil);
            locateAgentDetils.add(locateAgentDetil);
        }catch (Exception exception){
            return Arrays.asList();
        }

        return locateAgentDetils;
    }
}
