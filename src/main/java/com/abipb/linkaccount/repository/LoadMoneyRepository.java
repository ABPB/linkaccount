package com.abipb.linkaccount.repository;

import com.abipb.linkaccount.domain.AccountDetails;
import com.abipb.linkaccount.domain.LoadMoney;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Rajasekhar.E-V on 26-10-2018.
 */
@Repository
public interface LoadMoneyRepository extends JpaRepository<LoadMoney, String> {
}
