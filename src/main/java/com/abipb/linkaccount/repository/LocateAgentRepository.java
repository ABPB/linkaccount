package com.abipb.linkaccount.repository;

import com.abipb.linkaccount.exception.LinkAccountDataBaseException;
import com.abipb.linkaccount.payload.LocateAgentDetil;

import java.util.List;

/**
 * Created by Abhay.Kumar on 9/24/2018.
 */
public interface LocateAgentRepository {
    public List<LocateAgentDetil> fetchLocateAgent(LocateAgentDetil locateAgentDetil) throws LinkAccountDataBaseException;

}
