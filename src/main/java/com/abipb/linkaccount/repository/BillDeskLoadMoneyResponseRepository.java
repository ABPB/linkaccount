package com.abipb.linkaccount.repository;

import com.abipb.linkaccount.domain.DATA;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Rajasekhar.E-V on 29-10-2018.
 */
@Repository
public interface BillDeskLoadMoneyResponseRepository extends JpaRepository<DATA, String> {
}
