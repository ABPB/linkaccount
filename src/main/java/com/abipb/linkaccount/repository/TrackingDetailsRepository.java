package com.abipb.linkaccount.repository;

import com.abipb.linkaccount.domain.TrackingDetails;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by pardhasaradhi.k on 11/1/2018.
 */
public interface TrackingDetailsRepository extends JpaRepository<TrackingDetails,String > {
 TrackingDetails   findByRequestTrackingId(String trackingId);
}
