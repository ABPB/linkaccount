package com.abipb.linkaccount;

import com.abipb.linkaccount.payload.billdesk.request.BillDeskIIBLoadMoneyRequest;
import com.abipb.linkaccount.repository.UserDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Rajasekhar.E-V on 26-10-2018.
 */
@Controller
public class BillDeskController {

    @Value("${billdesk.return.uat.url.loadMoney}")
    private  String billdeskUrl;

    @Autowired
    UserDetailsRepository userDetailsRepository;

    @RequestMapping(value = "/billDeskLoadMoneyRequest", method = RequestMethod.POST)
    public ModelAndView billDeskLoadMoneyRequest(Model model, HttpServletRequest request,@RequestBody BillDeskIIBLoadMoneyRequest billDeskLoadMoneyRequest){

        // Parameters at server end
        String uniqueCustID = new SimpleDateFormat("yyMMddHHmmss").format(
                new Date()).toString();
        String checkSumKey = "";
        String reqDate = new SimpleDateFormat("yyyyMMddHHmmss").format(
                new Date()).toString();

       /* UserDetails userDetails=userDetailsRepository.findOne(billDeskLoadMoneyRequest.getCUST_ID());
        String tempReq = uniqueCustID + "|" + userDetails.getAccountDetails(). + "|" + amount + "|"
                + channelType + "|" + returnURL + "|" + merchantId + "|" + mobileNumber + "|NA|"
                + reqDate;

        String tempChckSum = HMAC.hmacSHA256(tempReq, checkSumKey);
        // Final request
        String finalReq = tempReq + "|" + tempChckSum;*/
        return  new ModelAndView("redirect:");
    }
}
